#!/bin/bash

file=$1
workload=$2
to_workload=$3


for x in "0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" ; do reference1=`cat $file | grep " $x " | grep "$workload 1 controlplane all" | awk '{print $18/16}'`; reference2=`cat $file | grep " $x " | grep "$workload 2 controlplane all" | awk '{print $18/16}'`; cat $file | awk -v REF1="$reference1" -v REF2="$reference2" -v LOAD="$x" -v WORKLOAD="$workload" '{if($1==WORKLOAD && ($4=="select-1") && $5==LOAD){if($2==1){print $0 " " $18/REF1 " " $19/$7/$16*8/1000000000}else{print $0 " " $18/REF2 " " $19/$7/$16*8/1000000000}}}' ; done > results-$to_workload-select-normalized.txt

for x in  "0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" ; do cat $file | grep " $x " | grep "$workload 1 controlplane all" | awk '{$4="select-1";$18=$18/16;print $0 " 1 " $19/$7/$16*8/1000000000}';  cat $file | grep " $x " | grep "$workload 2 controlplane all" | awk '{$4="select-1";$18=$18/16;print $0 " 1 " $19/$7/$16*8/1000000000}'; done >> results-$to_workload-select-normalized.txt

for x in  "0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" ; do cat $file | grep " $x " | grep "$workload 0 controlplane all" | awk '{$4="select-1";$18=$18/16;print $0 " -1 " $19/$7/$16*8/1000000000}'; done >> results-$to_workload-select-normalized.txt

for x in "0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" ; do reference1=`cat $file | grep " $x " | grep "$workload 1 controlplane all" | awk '{print $18/16}'`; reference2=`cat $file | grep "$x " | grep "$workload 2 controlplane all" | awk '{print $18/16}'`; cat $file | awk -v REF1="$reference1" -v REF2="$reference2" -v LOAD="$x" -v WORKLOAD="$workload" '{if($1==WORKLOAD && ($4=="filter") && $5==LOAD){if($2==1){print $0 " " $18/REF1 " " $19/$7/$16*8/1000000000}else{print $0 " " $18/REF2 " " $19/$7/$16*8/1000000000}}}' ; done > results-$to_workload-select-normalized-filter.txt

for x in  "0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" ; do cat $file | grep " $x " | grep "$workload 1 controlplane all" | awk '{$4="filter";$18=$18/16;print $0 " 1 " $19/$7/$16*8/1000000000}';  cat $file | grep "$x " | grep "$workload 2 controlplane all" | awk '{$4="filter";$18=$18/16;print $0 " 1 " $19/$7/$16*8/1000000000}'; done >> results-$to_workload-select-normalized-filter.txt

for x in  "0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" ; do cat $file | grep " $x " | grep "$workload 0 controlplane all" | awk '{$4="filter";$18=$18/16;print $0 " -1 " $19/$7/$16*8/1000000000}'; done >> results-$to_workload-select-normalized-filter.txt

