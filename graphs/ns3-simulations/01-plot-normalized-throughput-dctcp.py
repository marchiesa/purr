import os, sys, re
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import pylab
from utils import *
from matplotlib.ticker import FuncFormatter
### Roshan, Marco ###
'''
Adapted from the following original code: 
https://github.com/camsas/qjump-nsdi15-plotting/blob/master/figure1b_3b/plot_memcached_latency_cdfs.py
'''

'''
Column numbers from data set file

workload(1) failure(2) technique(3) mode(4) load(5) \
avgFCT(6) largeFCT(7) smallFCT(8) 99-FCT(9) \
99-largeFCT(10) 99-smallFCT(11) 99.9-FCT(12) 99.9-largeFCT(13) \
99.9-smallFCT(14) numberFlows(15) numberLargeFlows(16) numberSmallFlows(17) \
throughput(18) normalizedthroughput(19)

'''



paper_mode = True
subset_mode = False

if len(sys.argv) <= 5:
    print("usage: <input dataset>, <workload (dctcp|websearch)>, <failures (1|2)>, <mode (all|filter-1|select-1|filter-2|select-2)>, <x-axis(5|21)>, <y-axis (column number: 1...21)>\n\n"
    "COLUMN NUMBERS:\n"
    "workload(1) failure(2) technique(3) mode(4) load(5)\n"
    "avgFCT(6) largeFCT(7) smallFCT(8) 99-FCT(9)\n" 
    "99-largeFCT(10) 99-smallFCT(11) 99.9-FCT(12) 99.9-largeFCT(13)\n" 
    "99.9-smallFCT(14) numberFlows(15) numberLargeFlows(16) numberSmallFlows(17)\n" 
    "throughput(18) largethroughput(19) normalizedthroughput(20) perceivedthroughput(21)")
    exit(1)

input_dataset = str(sys.argv[1])
work_load = sys.argv[2]
failure_count = int(sys.argv[3])
mode_type=sys.argv[4]
x_values = int(sys.argv[5]) - 1 # column index, x-values
column_number = int(sys.argv[6]) - 1 # column index, y-values
approach = sys.argv[7].split(" ")
approach_names_override = sys.argv[8].split(" ")
num_cols = int(sys.argv[9])
x_min = float(sys.argv[10].split(":")[0])
x_max = float(sys.argv[10].split(":")[1])
y_min = float(sys.argv[11].split(":")[0])
y_max = float(sys.argv[11].split(":")[1])
y_scale = sys.argv[12]
x_unit = sys.argv[13]
y_unit = sys.argv[14]
y_axis_2 = 20

column_list = ['workload', 'failure', 'technique', 'mode', 'load',
               'avgFCT', 'largeFCT', 'smallFCT', '99-FCT', 
               '99-largeFCT', '99-smallFCT', '99.9-FCT', '99.9-largeFCT', 
               '99.9-smallFCT', 'numberFlows', 'numberLargeFlows', 'numberSmallFlows',
               'throughput',  'large-throughput', 'normalized-throughput', 'perceived-throughput']

column_names_list = ['workload', 'failure', 'technique', 'mode', 'Load',
               'avgFCT', 'largeFCT', 'Small flows FCT', '99-FCT', 
               '99-largeFCT', '99-smallFCT', '99.9-FCT', '99.9-largeFCT', 
               '99.9-smallFCT', 'numberFlows', 'numberLargeFlows', 'numberSmallFlows',
               'Throughput', 'Large Throughput' , 'Norm. Throughput', "Throughput"]

x_axis = column_list[x_values]

outname = "01-" +work_load+"-"+str(failure_count)+"f-"+column_list[column_number].replace(".","-")+"-"+x_axis+ "-filter-"+str(mode_type)
   

if paper_mode:
  fig = plt.figure(figsize=(2.5,1.22))
  set_paper_rcs()
else:
  fig = plt.figure()
  set_rcs()

colours = ['b', 'g', 'r', 'c', 'm', 'y', 'v']

# all other graphs
values_1 = {'controlplane': [], 'circular': [], 'recirculation': [], 'noreconvergence': [], "nofailure" : []}
values_y2 = {'controlplane': [], 'circular': [], 'recirculation': [], 'noreconvergence': [], "nofailure" : []}

#values_1 = {'controlplane': [], 'circular': [], 'recirculation': [], 'noreconvergence': [], "nofailure" : []}
#values_1 = {'0f-controlplane': [], 'f-controlplane': [],'circular': [], 'recirculation': [], 'noreconvergence': []}

approach_names = {'controlplane': "reconv",  'circular': "imm", 'recirculation': "recirc", 'noreconvergence': "obl", 'nofailure' : "no-fail"}
#approach_names = {'0f-controlplane': "zero-failures", 'f-controlplane': "reconvergence",  'circular': "immediate FRR", 'recirculation': "recirculation FRR", 'noreconvergence': "no FRR, no reconvergence"}
for i in range(0,len(approach_names_override)):
    if approach_names_override[i] != "*":
        approach_names[approach[i]] = approach_names_override[i].replace("-"," ")

# only for graph 14 (thrput vs FCT)
values_2 = {'controlplane': {'throughput': [], 'fct': []},
                'circular': {'throughput': [], 'fct': []},
                         'recirculation': {'throughput': [], 'fct': []},
                         'noreconvergence': {'throughput': [], 'fct': []},
                         'nofailure': {'throughput': [], 'fct': []}
         }

# filter/select graphs
values_3 = {'circular': [], 'recirculation': []}


techniques_part_1 = ["controlplane", "circular", "recirculation","noreconvergence","nofailure"]
#techniques_part_1 = ["0f-controlplane", "1f-controlplane", "2f-controlplane", "circular", "recirculation","noreconvergence"]
techniques_part_2 = ["controlplane", "circular","noreconvergence","nofailure"]
techniques_part_2 = ["controlplane", "circular","recirculation"]

print("Analyzing file" + input_dataset + ":")
  # parsing
for line in open(input_dataset).readlines()[0:]:
    fields = [x.strip() for x in line.split()]
    if fields[2] not in ["controlplane", "circular", "recirculation"]:
      print("Skipping line " + (line))
      continue
    workload = fields[0]
    failure = int(fields[1])
    technique = fields[2]
    mode = fields[3]
    field_to_plot = float(fields[column_number])
    extra_field_to_plot = float(fields[y_axis_2])
    #if failure_count == 1 and float(fields[4])>0.6:
        #continue
    #if failure_count == 2 and float(fields[4])>0.5:
        #continue
    if workload == work_load and failure == failure_count and mode == mode_type:
            values_1[technique].append(field_to_plot*1000) # fcts in ms
            values_y2[technique].append(extra_field_to_plot) # fcts in ms

load_arr = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7] # x% network load
if failure_count == 1:
    #load_arr = [13, 27, 40, 53, 67, 80, 93] # x% network load
    load_arr = [10, 20, 30, 40, 50, 60, 70] # x% network load
    #load_arr = [13, 27, 40, 53, 67, 80] # x% network load
elif failure_count == 2:
    #load_arr = [20, 40, 60, 80, 100, 120, 140] # x% network load
    #load_arr = [13, 27, 40, 53, 67, 80, 93] # x% network load
    load_arr = [10, 20, 30, 40, 50, 60, 70] # x% network load
    #load_arr = [18, 36, 54, 72, 90, 108, 126] # x% network load
    #load_arr = [18, 36, 54, 72, 90] # x% network load
   

#approach = {}
#if 5 <= column_number <= 13 and x_axis == "load":
   #approach = techniques_part_2
#elif 5 <= column_number <= 13 and (x_axis == "throughput" or x_axis == "normalized-throughput"):
   #approach = techniques_part_1
#else:
   #approach = techniques_part_1
fig, ax1 = plt.subplots(figsize=(3.5,1.69))
ax2 = ax1.twinx()
color1 = "b"
color2 = "r"
ax1.set_xlim(7,73)
ax1.set_ylim(0.9,3.3)
ax2.set_ylim(0,1.6)
ax1.set_xlabel('Load [\%]',  fontsize=11, y=0.32)
ax1.set_ylabel('Norm. Small FCT', color=color1,  fontsize=11, y=0.32)
ax2.set_ylabel('Norm. Throughput', color=color2,  fontsize=11, y=0.32)
ax1.tick_params(axis='y', labelcolor=color1)
ax2.tick_params(axis='y', labelcolor=color2)
i = 0
for t in approach:
    if x_axis == "load":
       values_y = values_1[t]
       if len(values_y) == 0:
         continue
       print( " load " + str(values_y))
    if x_axis == "throughput"  or x_axis == "normalized-throughput":
       # for graph: throughput vs FCT
       throughput_x = values_2[t]['throughput']
       fct_y = values_2[t]['fct']
       if len(fct_y) == 0:
         continue
       print(" load "+ str(fct_y) + " thr " + str(throughput_x))
    if t == "controlplane":
        i=1
        style = 'o'
    elif t == "circular":
        i=0
        style = 's'
    elif t == "recirculation":
        i=2
        style = 'v'
    elif t == "nofailure":
        i=3
        style = 'x'
    elif t == "no-reconvergence":
        i=4
        style = '+'
    label_name = approach_names[t]
    if x_axis == "throughput" or x_axis == "normalized-throughput":
       plt.plot(throughput_x, fct_y, label=label_name, color=colours[i], lw=1.0, linestyle='-', marker=style, mfc='none', mec=colours[i], ms=3)
       #plt.scatter(throughput_x, fct_y, label=label_name, color=colours[i],  marker=style)
       #plt.annotate("higher load", (throughput_x[6]-0.15, fct_y[6]*1.5))
    else:
       print values_1 
       print values_y2
       if t == "recirculation":
           values_y = [values_y[x]/values_1["circular"][x] for x in range(0,len(values_y))]
           values_y2[t] = [values_y2[t][x]/values_y2["circular"][x] for x in range(0,len(values_y2[t]))]
           print values_y2
           ax1.plot(load_arr, values_y, '-', linewidth=1, color=color1, marker='o', mfc='none', mec=color1, ms=5, label="recirc")
           ax2.plot(load_arr, values_y2[t], '-', linewidth=1, color=color2, marker='s', mfc='none', mec=color2, ms=5,  label="recirc")
           fig.tight_layout()
       #else:
           #continue
       if t == "circular":
           values_y = [1] * 7
           ax1.plot(load_arr, values_y, ':', lw=1.0, color=color1, marker='x', mfc='none', mec=color1, ms=5,  label="imm")
           ax2.plot(load_arr, values_y, ':', lw=1.0, color=color2, marker='v', mfc='none', mec=color2, ms=5,  label="imm")
       print values_y
       #plt.plot(load_arr,values_y , label=label_name, color=colours[i], lw=1.0, linestyle='-', marker=style, mfc='none', mec=colours[i], ms=3)
       plt.show()
    i += 1



# check units

'''
units = ""

if 5 <= column_number <= 13:
   units = "ms"
#elif column_number == 17:
#   units = "Gbps"
else:
   units = ""
'''
#plt.legend(loc='upper left', frameon=False, ncol=num_cols, columnspacing=0.8, handletextpad=0.2)


handles, labels = ax1.get_legend_handles_labels()
ax1.legend(handles, labels, loc='upper left', frameon=False,ncol=2, columnspacing=0.4, handletextpad=0.2)
handles, labels = ax2.get_legend_handles_labels()
ax2.legend(handles, labels, loc='upper right', frameon=False,ncol=2, columnspacing=0.4, handletextpad=0.2)


plt.savefig("%s.pdf" % outname, format="pdf", bbox_inches='tight',
           pad_inches=0.05)

