#!/bin/bash


# plot the websearch load
./normalize-load-to-failure.sh results-websearch.temp websearch websearch

# plot fig 10(a)
python 01-plot-normalized-throughput-dctcp.py results-websearch-select-normalized.txt websearch 1 select-1 5 8 "recirculation circular" "recirc purr" 1 7:73 0:2 lin "\%" ms

# plot fig 10(b)
python 01-plot-normalized-throughput-dctcp.py results-websearch-select-normalized.txt websearch 2 select-1 5 8 "recirculation circular" "recirc purr" 1 7:73 0:2 lin "\%" ms

# plot the datamining load
./normalize-load-to-failure.sh results-datamining.temp datamining datamining

# plot fig 3(a)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 8 "recirculation controlplane" "FRR-recirculation CP-reconvergence" 1 7:73 0:2 lin "\%" ms
# plot fig 3(b)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 8 "recirculation controlplane" "FRR-recirculation CP-reconvergence" 1 7:73 0:2 lin "\%" ms
# plot fig 3(c)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 21 "recirculation controlplane" "FRR-recirculation CP-reconvergence" 1 7:73 0.5:40 log  "\%" Gbps
# plot fig 3(d)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 21 "recirculation controlplane" "FRR-recirculation CP-reconvergence" 1 7:73 0.5:40 log "\%" Gbps

for file in `ls *data*pdf`
do
  mv $file  $file-motivation.pdf
done

# plot fig 9(a)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 8 "recirculation controlplane circular nofailure" "* * * *" 3 7:73 0:2 lin "\%" ms
# plot fig 9(b)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 8 "recirculation controlplane circular nofailure" "* * * *" 3 7:73 0:2 lin "\%" ms
# plot fig 9(c)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 11 "recirculation controlplane circular nofailure" "* * *" 3 7:73 0.5:5 lin "\%" ms
# plot fig 9(d)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 11 "recirculation controlplane circular nofailure" "* * *" 3 7:73 0.5:5 lin "\%" ms
# plot fig 9(e)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 21 "recirculation controlplane circular nofailure" "* * * *" 3 7:73 0.5:40 log "\%" Gbps
# plot fig 9(f)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 21 "recirculation controlplane circular nofailure" "* * * *" 3 7:73 0.5:40 log "\%" Gbps

