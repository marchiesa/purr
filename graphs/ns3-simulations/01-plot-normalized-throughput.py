import os, sys, re
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import pylab
from utils import *
from matplotlib.ticker import FuncFormatter
### Roshan, Marco ###
'''
Adapted from the following original code: 
https://github.com/camsas/qjump-nsdi15-plotting/blob/master/figure1b_3b/plot_memcached_latency_cdfs.py
'''

'''
Column numbers from data set file

workload(1) failure(2) technique(3) mode(4) load(5) \
avgFCT(6) largeFCT(7) smallFCT(8) 99-FCT(9) \
99-largeFCT(10) 99-smallFCT(11) 99.9-FCT(12) 99.9-largeFCT(13) \
99.9-smallFCT(14) numberFlows(15) numberLargeFlows(16) numberSmallFlows(17) \
throughput(18) normalizedthroughput(19)

'''



paper_mode = True
subset_mode = False

if len(sys.argv) <= 5:
    print("usage: <input dataset>, <workload (dctcp|websearch)>, <failures (1|2)>, <mode (all|filter-1|select-1|filter-2|select-2)>, <x-axis(5|21)>, <y-axis (column number: 1...21)>\n\n"
    "COLUMN NUMBERS:\n"
    "workload(1) failure(2) technique(3) mode(4) load(5)\n"
    "avgFCT(6) largeFCT(7) smallFCT(8) 99-FCT(9)\n" 
    "99-largeFCT(10) 99-smallFCT(11) 99.9-FCT(12) 99.9-largeFCT(13)\n" 
    "99.9-smallFCT(14) numberFlows(15) numberLargeFlows(16) numberSmallFlows(17)\n" 
    "throughput(18) largethroughput(19) normalizedthroughput(20) perceivedthroughput(21)")
    exit(1)

input_dataset = str(sys.argv[1])
work_load = sys.argv[2]
failure_count = int(sys.argv[3])
mode_type=sys.argv[4]
x_values = int(sys.argv[5]) - 1 # column index, x-values
column_number = int(sys.argv[6]) - 1 # column index, y-values
approach = sys.argv[7].split(" ")
approach_names_override = sys.argv[8].split(" ")
num_cols = int(sys.argv[9])
x_min = float(sys.argv[10].split(":")[0])
x_max = float(sys.argv[10].split(":")[1])
y_min = float(sys.argv[11].split(":")[0])
y_max = float(sys.argv[11].split(":")[1])
y_scale = sys.argv[12]
x_unit = sys.argv[13]
y_unit = sys.argv[14]
width = sys.argv[15]
height = sys.argv[16]
font_size = sys.argv[17]


column_list = ['workload', 'failure', 'technique', 'mode', 'load',
               'avgFCT', 'largeFCT', 'smallFCT', '99-FCT', 
               '99-largeFCT', '99-smallFCT', '99.9-FCT', '99.9-largeFCT', 
               '99.9-smallFCT', 'numberFlows', 'numberLargeFlows', 'numberSmallFlows',
               'throughput',  'large-throughput', 'normalized-throughput', 'perceived-throughput']

column_names_list = ['workload', 'failure', 'technique', 'mode', 'Load',
               'avgFCT', 'largeFCT', 'Small flows FCT', '99-FCT', 
               '99-largeFCT', '99-smallFCT', '99.9-FCT', '99.9-largeFCT', 
               '99.9-smallFCT', 'numberFlows', 'numberLargeFlows', 'numberSmallFlows',
               'Throughput', 'Large Throughput' , 'Norm. Throughput', "Throughput"]

x_axis = column_list[x_values]

outname = "01-" +work_load+"-"+str(failure_count)+"f-"+column_list[column_number].replace(".","-")+"-"+x_axis+ "-filter-"+str(mode_type)
   

if paper_mode:
  fig = plt.figure(figsize=(float(width),float(height)))
  set_paper_rcs()
else:
  fig = plt.figure()
  set_rcs()

colours = ['b', 'g', 'r', 'c', 'm', 'y', 'v']

# all other graphs
values_1 = {'controlplane': [], 'circular': [], 'recirculation': [], 'noreconvergence': [], "nofailure" : []}
#values_1 = {'controlplane': [], 'circular': [], 'recirculation': [], 'noreconvergence': [], "nofailure" : []}
#values_1 = {'0f-controlplane': [], 'f-controlplane': [],'circular': [], 'recirculation': [], 'noreconvergence': []}

approach_names = {'controlplane': "reconv",  'circular': "purr", 'recirculation': "recirc", 'noreconvergence': "obl", 'nofailure' : "no-fail"}
#approach_names = {'0f-controlplane': "zero-failures", 'f-controlplane': "reconvergence",  'circular': "immediate FRR", 'recirculation': "recirculation FRR", 'noreconvergence': "no FRR, no reconvergence"}
for i in range(0,len(approach_names_override)):
    if approach_names_override[i] != "*":
        approach_names[approach[i]] = approach_names_override[i].replace("-"," ")

# only for graph 14 (thrput vs FCT)
values_2 = {'controlplane': {'throughput': [], 'fct': []},
                'circular': {'throughput': [], 'fct': []},
                         'recirculation': {'throughput': [], 'fct': []},
                         'noreconvergence': {'throughput': [], 'fct': []},
                         'nofailure': {'throughput': [], 'fct': []}
         }

# filter/select graphs
values_3 = {'circular': [], 'recirculation': []}


techniques_part_1 = ["controlplane", "circular", "recirculation","noreconvergence","nofailure"]
#techniques_part_1 = ["0f-controlplane", "1f-controlplane", "2f-controlplane", "circular", "recirculation","noreconvergence"]
techniques_part_2 = ["controlplane", "circular","noreconvergence","nofailure"]
techniques_part_2 = ["controlplane", "circular","recirculation"]

print("Analyzing file" + input_dataset + ":")
  # parsing
for line in open(input_dataset).readlines()[0:]:
    fields = [x.strip() for x in line.split()]
    if fields[2] not in ["controlplane", "circular", "recirculation"]:
      print("Skipping line " + (line))
      continue
    workload = fields[0]
    failure = int(fields[1])
    technique = fields[2]
    mode = fields[3]
    field_to_plot = float(fields[column_number])
    extra_field_to_plot = float(fields[x_values])
    #if failure_count == 1 and float(fields[4])>0.6:
        #continue
    #if failure_count == 2 and float(fields[4])>0.5:
        #continue
    if workload == work_load and failure == failure_count and mode == mode_type:
        if 5 <= column_number <= 14 and x_axis == "load":
            values_1[technique].append(field_to_plot*1000) # fcts in ms
        elif 5 <= column_number <= 14 and (x_axis == "throughput" or x_axis == "normalized-throughput"):
            values_2[technique]['fct'].append(field_to_plot*1000)
            values_2[technique]['throughput'].append(extra_field_to_plot)
        else:
            values_1[technique].append(field_to_plot) 
    elif workload == work_load and failure == 0 and mode == mode_type:
        if mode_type == "select-1":
            extra_field_to_plot = extra_field_to_plot/16
        if 5 <= column_number <= 14 and x_axis == "load":
            values_1["nofailure"].append(field_to_plot*1000) # fcts in ms
        elif 5 <= column_number <= 14 and (x_axis == "throughput" or x_axis == "normalized-throughput"):
            values_2["nofailure"]['fct'].append(field_to_plot*1000)
            values_2["nofailure"]['throughput'].append(extra_field_to_plot)
        else:
            values_1["nofailure"].append(field_to_plot)

load_arr = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7] # x% network load
if failure_count == 1:
    #load_arr = [13, 27, 40, 53, 67, 80, 93] # x% network load
    load_arr = [10, 20, 30, 40, 50, 60, 70] # x% network load
    #load_arr = [13, 27, 40, 53, 67, 80] # x% network load
elif failure_count == 2:
    #load_arr = [20, 40, 60, 80, 100, 120, 140] # x% network load
    #load_arr = [13, 27, 40, 53, 67, 80, 93] # x% network load
    load_arr = [10, 20, 30, 40, 50, 60, 70] # x% network load
    #load_arr = [18, 36, 54, 72, 90, 108, 126] # x% network load
    #load_arr = [18, 36, 54, 72, 90] # x% network load
   

#approach = {}
#if 5 <= column_number <= 13 and x_axis == "load":
   #approach = techniques_part_2
#elif 5 <= column_number <= 13 and (x_axis == "throughput" or x_axis == "normalized-throughput"):
   #approach = techniques_part_1
#else:
   #approach = techniques_part_1

i = 0
for t in approach:
    if x_axis == "load":
       values_y = values_1[t]
       if len(values_y) == 0:
         continue
       print( " load " + str(values_y))
    if x_axis == "throughput"  or x_axis == "normalized-throughput":
       # for graph: throughput vs FCT
       throughput_x = values_2[t]['throughput']
       fct_y = values_2[t]['fct']
       if len(fct_y) == 0:
         continue
       print(" load "+ str(fct_y) + " thr " + str(throughput_x))
    if t == "controlplane":
        i=1
        style = 'o'
    elif t == "circular":
        i=0
        style = 's'
    elif t == "recirculation":
        i=2
        style = 'v'
    elif t == "nofailure":
        i=3
        style = 'x'
    elif t == "no-reconvergence":
        i=4
        style = '+'
    label_name = approach_names[t]
    if x_axis == "throughput" or x_axis == "normalized-throughput":
       plt.plot(throughput_x, fct_y, label=label_name, color=colours[i], lw=1.0, linestyle='-', marker=style, mfc='none', mec=colours[i], ms=3)
       #plt.scatter(throughput_x, fct_y, label=label_name, color=colours[i],  marker=style)
       #plt.annotate("higher load", (throughput_x[6]-0.15, fct_y[6]*1.5))
    else:
       plt.plot(load_arr,values_y , label=label_name, color=colours[i], lw=1.0, linestyle='-', marker=style, mfc='none', mec=colours[i], ms=3)
    i += 1



# check units

'''
units = ""

if 5 <= column_number <= 13:
   units = "ms"
#elif column_number == 17:
#   units = "Gbps"
else:
   units = ""
'''

#plt.xticks(load_arr)
#plt.title(str(work_load)+"/" +str(failure_count)+"f")
#plt.xlabel("Load", fontsize=12)

if 5 <= column_number <= 14 and x_axis == "load":
   plt.ylabel(column_names_list[column_number]+" [ms]", fontsize=font_size,y=0.32)
   plt.xlabel(x_axis, fontsize=11)
   plt.xticks(load_arr)
   #plt.yscale('log')
   if work_load == "websearch":
        if failure_count == 1:
            if mode_type == "select-1":
                plt.ylim(0., 1.8)
                #plt.annotate("2.5x", (55, 0.65))
                #plt.annotate("", (53,1), xytext=(53,0.4), arrowprops=dict(arrowstyle='<->'))
            else:
                plt.ylim(0., 1.8)
        if failure_count == 2:
            if mode_type == "select-1":
                plt.ylim(0., 1.8)
                #plt.annotate("2.75x", (56, 0.7))
                #plt.annotate("", (54,1.1), xytext=(54,0.4), arrowprops=dict(arrowstyle='<->'))
            else:
                plt.ylim(0., 1.8)
   if column_number == 7:
       if not "circular" in  approach:
           if failure_count == 1:
               plt.annotate("2.4x", (42, 0.6))
               plt.annotate("", (40,0.94), xytext=(40,0.4), arrowprops=dict(arrowstyle='<->'))
           else:
               plt.annotate("3.7x", (42, 0.65))
               plt.annotate("", (40,1.1), xytext=(40,0.41), arrowprops=dict(arrowstyle='<->'))
   #if column_number == 7:
       #plt.yscale('log')
elif 5 <= column_number <= 14 and (x_axis == "throughput" or x_axis == "normalized-throughput"):
   plt.ylabel(column_names_list[column_number]+"[ms]", fontsize=font_size, y=0.32)
   plt.xlabel(x_axis+"[Gbps]", fontsize=11)
   #plt.yscale('log')
elif column_number == 17:
   units = "\%"
   plt.ylabel(column_names_list[column_number]+" [Gbps]", fontsize=font_size, y=0.32)
   #plt.text(-1,-1,column_names_list[column_number], fontsize=12, rotation=90)
   plt.xlabel(column_names_list[x_values]+" ["+units+"]", fontsize=font_size)
   plt.xticks(load_arr)
   if work_load == "websearch":
        if failure_count == 1:
            if mode_type == "select-1":
                plt.ylim(0., 3.5)
            else:
                plt.ylim(0., 3.5)
        if failure_count == 2:
            if mode_type == "select-1":
                print "hey"
                plt.ylim(0., 3.5 )
            else:
                plt.ylim(0., 3.5)
        if not "circular" in approach:  
            if failure_count == 1:
                plt.annotate("1.3x", (41, 1.45))
                plt.annotate("", (40,1.85), xytext=(40,1.2), arrowprops=dict(arrowstyle='<->', mutation_scale=7, shrinkA=1, shrinkB=1))
            else:
                plt.annotate("2.5x", (42, 1.1))
                plt.annotate("", (40,1.77), xytext=(40,0.7), arrowprops=dict(arrowstyle='<->'))
elif column_number == 20:
    if not "circular" in approach:  
        if failure_count == 1:
            plt.annotate("2.7x", (42, 2))
            plt.annotate("", (40,4.15), xytext=(40,1.57), arrowprops=dict(arrowstyle='<->'))
        else:
            plt.annotate("3.3x", (42, 1.7))
            plt.annotate("", (40,3.87), xytext=(40,1.19), arrowprops=dict(arrowstyle='<->'))

elif column_number == 19:
   units = "\%"
   plt.ylabel(column_names_list[column_number], fontsize=font_size, y=0.32)
   plt.xlabel("Load ["+units+"]", fontsize=11)
   plt.xticks(load_arr)
   miny = 0
   maxy = 1.4
   plt.ylim(miny, maxy)
   plt.yticks(np.arange(0, maxy+0.001, 0.2), [str(x) for x in np.arange(miny, maxy+0.001, 0.2)])
else:
   plt.ylabel(column_names_list[column_number], fontsize=font_size, y=0.32)
   plt.xlabel("Load", fontsize=11)

if not (5 <= column_number <= 14 and (x_axis == "throughput" or x_axis == "normalized-throughput")):
    if failure_count == 1:
        plt.xlim(10, 96)
        minx=10
        maxx=73
        plt.xticks(np.arange(minx,maxx,10), [str(x) for x in np.arange(minx, maxx, 10)])
    elif failure_count == 2:
        minx=10
        maxx=73
        plt.xlim(minx-3, maxx+1)
        plt.xticks(np.arange(minx,maxx,10), [str(x) for x in np.arange(minx, maxx, 10)])
else:
    if work_load == "dctcp":
        if failure_count == 1:
            if mode_type == "select-1":
                plt.ylim(0.1, 1500)
                plt.xlim(0.01, 4)
                #plt.annotate("higher load", (2.25,100 ))
                #plt.annotate("lower load", (0.2, 0.5))
            else:
                if column_number == 5:
                    plt.ylim(1, 1000)
                    plt.xlim(0.01, 60)
                else:
                    plt.ylim(10, 30000)
                    plt.xlim(0.01, 60)
                #iplt.annotate("higher load", (1.5,100 ))
                #plt.annotate("lower load", (0.15, 0.5))
        else:
            if mode_type == "select-1":
                plt.ylim(0.1, 1500)
                plt.xlim(0.01, 4)
                #plt.annotate("higher load", (2.25,100 ))
                #plt.annotate("lower load", (0.2, 0.5))
            else:
                if column_number == 5:
                    plt.ylim(1, 1000)
                    plt.xlim(0.01, 45)
                else:
                    plt.ylim(10, 30000)
                    plt.xlim(0.01, 45)
                #plt.annotate("higher load", (1.5,100 ))
                #plt.annotate("lower load", (0.15, 0.5))
    else:
        if failure_count == 1:
            if mode_type == "select-1":
                plt.ylim(5, 150)
                plt.xlim(0.01, 3)
                #plt.annotate("higher load", (1.75,50 ))
                #plt.annotate("lower load", (0.25, 7.5))
            else:
                plt.ylim(5, 150)
                plt.xlim(0.01, 2.5)
                #plt.annotate("higher load", (1.4,50 ))
                #plt.annotate("lower load", (0.25, 7.5))
        else:
            if mode_type == "select-1":
                plt.ylim(0.1, 1500)
                plt.xlim(0.01, 4)
                #plt.annotate("higher load", (2.25,100 ))
                #plt.annotate("lower load", (0.2, 0.5))
            else:
                plt.ylim(0.1, 3000)
                plt.xlim(0.01, 3)
                #plt.annotate("higher load", (1.5,100 ))
                #plt.annotate("lower load", (0.15, 0.5))

   
#plt.xticks(load_arr)
#plt.title(str(work_load)+"/" +str(failure_count)+"f")
#plt.ylabel(column_list[column_number]+" ["+units+"]", fontsize=12)
#plt.xlabel("Load", fontsize=12)

#plt.plot(load_arr,avg_recirc, label="recirculation FRR",color='magenta', lw=1.0, linestyle='-',marker= 'o', mfc='none', mec='magenta', ms=3)
#plt.plot(load_arr, avg_immediate, label="immediate FRR",color='blue', lw=1.0, linestyle='-',marker= 's', mfc='none', mec='blue', ms=3)
#plt.plot(load_arr,avg_cp, label="ideal",color='red', lw=1.0, linestyle='-',marker= 'v', mfc='none', mec='red', ms=3)

plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
if x_unit=="*":
    plt.xlabel(column_names_list[x_values], fontsize=font_size,y=0.32)
else:
    plt.xlabel(column_names_list[x_values]+" ["+x_unit+"]", fontsize=font_size,y=0.32)
if y_unit=="*":
    plt.ylabel(column_names_list[column_number], fontsize=font_size,y=0.32)
else:
    plt.ylabel(column_names_list[column_number]+" ["+y_unit+"]", fontsize=font_size,y=0.32)


if y_scale == "log":
    plt.yscale('log')
    if column_number == 20:
        # TODO: disable log notation
        pass

if column_number == 19:
  plt.legend(loc='upper left', frameon=False, ncol=num_cols, columnspacing=0.8, handletextpad=0.2)
  plt.legend(loc='upper left', frameon=False, ncol=num_cols, columnspacing=0.8, handletextpad=0.2)
elif column_number == 17:
  plt.legend(loc='upper left', frameon=False,  ncol=num_cols, columnspacing=0.8, handletextpad=0.2)
#elif column_number == 7:
  #plt.legend(loc='upper left', frameon=False,  ncol=num_cols, columnspacing=0.8, handletextpad=0.2, prop={'size': 11})
  #plt.legend(loc='upper left', frameon=False,  ncol=num_cols, columnspacing=0.8, handletextpad=0.2)
elif x_axis == "throughput" or x_axis == "normalized-throughput":
  plt.legend(loc='upper left', frameon=False, scatterpoints=1, ncol=num_cols, columnspacing=0.8, handletextpad=0.2)
  pass
else:
  plt.legend(loc='upper left', frameon=False, ncol=num_cols, columnspacing=0.8, handletextpad=0.2)


plt.savefig("%s.pdf" % outname, format="pdf", bbox_inches='tight',
           pad_inches=0.05)

