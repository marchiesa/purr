#!/bin/bash


# copy the aggregated raw deta
cp ../../raw-results/ns3-simulations/results-websearch-raw.temp .
cp ../../raw-results/ns3-simulations/results-datamining-raw.temp .

# plot the websearch load
./normalize-load-to-failure.sh results-websearch-raw.temp websearch websearch

# plot fig 10(a)
python 01-plot-normalized-throughput-dctcp.py results-websearch-select-normalized.txt websearch 1 select-1 5 8 "recirculation circular" "recirc purr" 1 7:73 0:2 lin "\%" ms

# plot fig 10(b)
python 01-plot-normalized-throughput-dctcp.py results-websearch-select-normalized.txt websearch 2 select-1 5 8 "recirculation circular" "recirc purr" 1 7:73 0:2 lin "\%" ms

# plot the datamining load
./normalize-load-to-failure.sh results-datamining-raw.temp datamining datamining

rm *motivation*pdf

# plot fig 3(a)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 8 "recirculation controlplane" "FRR-recirculation CP-reconvergence" 1 7:73 0:2 lin "\%" ms 2.8 1.22 11
# plot fig 3(b)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 8 "recirculation controlplane" "FRR-recirculation CP-reconvergence" 1 7:73 0:2 lin "\%" ms 2.8 1.22 11
# plot fig 3(c)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 21 "recirculation controlplane" "FRR-recirculation CP-reconvergence" 1 7:73 0.5:40 log  "\%" Gbps 2.8 1.22 11
# plot fig 3(d)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 21 "recirculation controlplane" "FRR-recirculation CP-reconvergence" 1 7:73 0.5:40 log "\%" Gbps 2.8 1.22 11

for file in `ls *data*pdf`
do
  final_file=`echo $file | sed "s/\.pdf//"` 
  mv $file  $final_file-motivation.pdf
done

# plot fig 9(a)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 8 "recirculation controlplane circular nofailure" "* * * *" 3 7:73 0:2 lin "\%" ms 2.5 1.22 11
# plot fig 9(b)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 8 "recirculation controlplane circular nofailure" "* * * *" 3 7:73 0:2 lin "\%" ms 2.5 1.22 11
# plot fig 9(c)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 11 "recirculation controlplane circular nofailure" "* * *" 3 7:73 0.5:5 lin "\%" ms 2.5 1.22 11
# plot fig 9(d)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 11 "recirculation controlplane circular nofailure" "* * *" 3 7:73 0.5:5 lin "\%" ms 2.5 1.22 11
# plot fig 9(e)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 1 select-1 5 21 "recirculation controlplane circular nofailure" "* * * *" 3 7:73 0.5:40 log "\%" Gbps 2.5 1.22 11
# plot fig 9(f)
python 01-plot-normalized-throughput.py results-datamining-select-normalized.txt datamining 2 select-1 5 21 "recirculation controlplane circular nofailure" "* * * *" 3 7:73 0.5:40 log "\%" Gbps 2.5 1.22 11

