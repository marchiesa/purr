# Copyright (c) 2015, Malte Schwarzkopf
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of qjump-nsdi15-plotting nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


### Roshan ###
'''
Adapted from the following original code: 
https://github.com/camsas/qjump-nsdi15-plotting/blob/master/figure1b_3b/plot_memcached_latency_cdfs.py

'''

import os, sys, re
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import pylab
from utils import *
from matplotlib.ticker import FuncFormatter


#x-axis: number-of sequences (fix size_of_sequences to 16)
#y-axis: average(scs-length) over the 6 seeds

seqsize=int(sys.argv[1])
costfilter=sys.argv[2]

paper_mode = True
subset_mode = False

outname = "05-random-vs-jelly-fish-seq-size-" + str(seqsize) + "-" + str(costfilter)
fnames = []


for i in range(0, len(sys.argv) - 1, 1):
  #mode = sys.argv[2 + i]   
  fnames.append(sys.argv[1 + i])

if paper_mode:
  fig = plt.figure(figsize=(3.33,1.22))
  set_paper_rcs()
else:
  fig = plt.figure()
  set_rcs()

colours = ['b', 'g', 'r', 'c', 'm', 'y', 'v']

random = []
circular = []
jellyfish = []


ran = []
jelly = []
circ = []

randommap = {}
jellymap = {}
circularmap = {}

fname = "results.txt"

filenames = []
filenames.append(fname)

dict_list = {"random": randommap, "jellyfish": jellymap , "circular": circularmap }

for f in filenames:
  print "Analyzing file %s: " % (f)
  for line in open(f).readlines()[1:]:
    fields = [x.strip() for x in line.split()]
    algo = fields[0] # algo
    generator = fields[1] # random generator
    seed = fields[2] # random seed
    nof_sq = int(fields[3]) # no of sequences
    seq_size = int(fields[4]) # seq size
    cost = fields[5] # bitcost/entrycost
    scs_length = int(fields[6]) # scs length
    timems = float(fields[7]) # time in millisecond
    bitcost = (scs_length + seq_size) * scs_length
    if costfilter == "bitcost":
      bitcost = (scs_length + seq_size) * scs_length
    else:
      bitcost = scs_length
    if algo == "fastgreedy" and generator == "jellyfish" and seq_size == seqsize:
      jellymap.setdefault(nof_sq*seq_size, [])
      jellymap[nof_sq*seq_size].append(bitcost)
      #fastgreedy[seq_size].append(scs_length) #entrycost
    elif algo == "fastgreedy" and generator == "random" and seq_size == seqsize:
      randommap.setdefault(nof_sq, [])
      randommap[nof_sq].append(bitcost)
      #fastgreedy[seq_size].append(scs_length) #entrycost
    elif algo == "fastgreedy" and generator == "circular" and seq_size == seqsize:
      circularmap.setdefault(nof_sq, [])
      circularmap[nof_sq].append(bitcost)
      #fastgreedy[seq_size].append(scs_length) #entrycost


for item in dict_list:
  dict_ = dict_list[item]
  for key in sorted(dict_.keys()):
    if len(dict_[key]) < 6:
      continue
    row = np.average(dict_[key])
    if item == "random":
      random.append(row)
      ran.append(key)
    elif (item == "jellyfish"):
      jellyfish.append(row)
      jelly.append(key)
    elif (item == "circular"):
      circular.append(row)
      circ.append(key)

plt.xlabel("number of sequences", fontsize=10)
if costfilter == "bitcost":
  plt.ylabel("Memory cost [bit]")
else:
  plt.ylabel("\#TCAM entries")
plt.xscale('log')
#plt.title('sizeofsequence=16')
#plt.ylim(0,2000)
plt.xlim(100,200000)
#plt.xticks(range(0, 10000, 10), [str(x) for x in range(0, 10000, 10)])

if seqsize == 8:
  if costfilter == "bitcost":
    plt.xlim(40, 200000)
    miny=1800
    maxy=3400
    plt.ylim(miny, maxy)
    plt.yticks(range(miny+200, maxy+1, 500), [str(x) for x in range(miny+200, maxy+1, 500)])
  else:
    plt.xlim(2, 2000)
    miny=0
    maxy=80
    plt.ylim(miny, maxy)
    plt.yticks(range(miny, maxy+1, 20), [str(x) for x in range(miny, maxy+1, 20)])

elif seqsize == 16:
  if costfilter == "bitcost":
     #plt.xlim(2, 2000)
     miny=20000
     maxy=50000
     plt.ylim(miny, maxy)
     plt.yticks(range(miny, maxy+1, 10000), [str(x) for x in range(miny, maxy+1, 10000)])
  else:
    miny=140
    maxy=200
    plt.ylim(miny, maxy)
    plt.yticks(range(miny, maxy+1, 20), [str(x) for x in range(miny, maxy+1, 20)])
elif seqsize == 32:
  if costfilter == "bitcost":
    plt.xlim(200, 200000)
    miny=300000
    maxy=600000
    plt.ylim(miny, maxy)
    plt.yticks(range(miny, maxy+1, 100000), [str(x) for x in range(miny, maxy+1, 100000)])
  else:
    plt.xlim(2, 2000)
    miny=0
    maxy=1000
    plt.ylim(miny, maxy)
    plt.yticks(range(miny, maxy+1, 200), [str(x) for x in range(miny, maxy+1, 200)])

print ran
print random
print jelly
print jellyfish
#print circ
#print circular

plt.plot(ran, random, label="random",color='red', lw=1.0, linestyle='-',marker= '>', mfc='none', mec='red', ms=3)
plt.plot(jelly, jellyfish, label="tree",color='blue', lw=1.0, linestyle='-',marker= 's', mfc='none', mec='blue', ms=3)
#plt.plot(circ, circular, label="basic",color='blue', lw=1.0, linestyle='-',marker= 's', mfc='none', mec='green', ms=3)


plt.legend(loc='lower right', frameon=False)
plt.savefig("%s.pdf" % outname, format="pdf", bbox_inches='tight', pad_inches=0.05)

