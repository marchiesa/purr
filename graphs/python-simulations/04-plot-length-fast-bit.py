# Copyright (c) 2015, Malte Schwarzkopf
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of qjump-nsdi15-plotting nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


### Marco, Roshan ###
'''
Adapted from the following original code: 
https://github.com/camsas/qjump-nsdi15-plotting/blob/master/figure1b_3b/plot_memcached_latency_cdfs.py

'''

import os, sys, re
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import pylab
from utils import *
from matplotlib.ticker import FuncFormatter

costfilter=sys.argv[1]

#x-axis: number-of sequences (fix size_of_sequences to 16)
#y-axis: average(scs-length) over the 6 seeds


paper_mode = True
subset_mode = False

outname = "04-fastgreedy-" + costfilter
fnames = []


for i in range(0, len(sys.argv) - 1, 1):
  #mode = sys.argv[2 + i]   
  fnames.append(sys.argv[1 + i])

if paper_mode:
  fig = plt.figure(figsize=(3.33,1.2))
  set_paper_rcs()
else:
  fig = plt.figure()
  set_rcs()

colours = ['b', 'g', 'r', 'c', 'm', 'y', 'v']

dpscs = []
greedy = []
hierarchical = []
fastgreedy8 = []
fastgreedy16 = []
fastgreedy32 = []


dp = []
gr = []
hier = []
fast8 = []
fast16 = []
fast32 = []

dpscsmap = {}
greedymap = {}
hierarchicalmap = {}
fastgreedymap8 = {}
fastgreedymap16 = {}
fastgreedymap32 = {}

fname = "results.txt"

filenames = []
filenames.append(fname)

dict_list = { "fastgreedy8": fastgreedymap8, "fastgreedy16": fastgreedymap16, "fastgreedy32": fastgreedymap32}

for f in filenames:
  print "Analyzing file %s: " % (f)
  for line in open(f).readlines()[1:]:
    fields = [x.strip() for x in line.split()]
    algo = fields[0] # algo
    generator = fields[1] # random generator
    seed = fields[2] # random seed
    nof_sq = int(fields[3]) # no of sequences
    seq_size = int(fields[4]) # seq size
    cost = fields[5] # bitcost/entrycost
    scs_length = int(fields[6]) # scs length
    timems = float(fields[7]) # time in millisecond
    if costfilter == "bitcost":
      bitcost = (scs_length + seq_size) * scs_length
    else:
      bitcost = scs_length
    if algo == "fastgreedy" and seq_size == 8 and generator == "random":
      fastgreedymap8.setdefault(nof_sq, [])
      fastgreedymap8[nof_sq].append(bitcost)
    elif algo == "fastgreedy" and seq_size == 16 and generator == "random":
      fastgreedymap16.setdefault(nof_sq, [])
      fastgreedymap16[nof_sq].append(bitcost)
    elif algo == "fastgreedy" and seq_size == 32 and generator == "random":
      fastgreedymap32.setdefault(nof_sq, [])
      fastgreedymap32[nof_sq].append(bitcost)


for item in dict_list:
  dict_ = dict_list[item]
  for key in sorted(dict_.keys()):
    if len(dict_[key]) < 6:
      continue
    row = np.average(dict_[key])
    if row <1:
      continue
    if item == "fastgreedy8":
      fastgreedy8.append(row)
      fast8.append(key)
    elif (item == "fastgreedy16"):
      fastgreedy16.append(row)
      fast16.append(key)
    elif (item == "fastgreedy32"):
      fastgreedy32.append(row)
      fast32.append(key)

plt.xlabel("number of sequences", fontsize=10)
if costfilter == "bitcost":
  plt.ylabel("Memory cost [bit]")
else:
  plt.ylabel("\#TCAM entries")
plt.yscale('log')
plt.xscale('log')
#plt.title('sizeofsequence=16')
#plt.ylim(0,2000)
if costfilter == "bitcost":
  plt.xlim(2, 200000)
  miny=100
  maxy=10000000
  plt.ylim(miny, maxy)
  #plt.yticks(range(miny, maxy+1, 100000), [str(x) for x in range(miny, maxy+1, 100000)])
else:
  plt.xlim(2, 200000)
  miny=10
  maxy=2500
  plt.ylim(miny, maxy)
  #plt.yticks(range(miny, maxy+1, 200), [str(x) for x in range(miny, maxy+1, 200)])

plt.plot(fast8, fastgreedy8, label="k=8",color='red', lw=1.0, linestyle='-',marker= '>', mfc='none', mec='red', ms=3)
plt.plot(fast16, fastgreedy16, label="k=16",color='blue', lw=1.0, linestyle='-',marker= 's', mfc='none', mec='blue', ms=3)
plt.plot(fast32, fastgreedy32, label="k=32",color='green', lw=1.0, linestyle='-',marker= 'v', mfc='none', mec='green', ms=3)

plt.legend(loc='upper left', frameon=False, ncol=3)
plt.savefig("%s.pdf" % outname, format="pdf", bbox_inches='tight', pad_inches=0.05)

