#!/bin/bash

echo "usage: ./generate-all-plots.sh results-file"

# get the file with the results as input
input=$1
#input=../../code/python-simulations/results

is_dir=`echo $input | grep txt | wc -l`

if [ $is_dir -eq 0 ]
then
  # get the output of the runned simulation
  for x in `ls $input/*txt`; do cat $x; done | sort | uniq | sed "s/michael-scs/fastgreedy/g" > results.txt
else
  # use the provided raw data
  cp $input .
fi

# generate the plot comparing DCSCS, GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.5(a)) - bit-cost
python 01-plot-length-optimal-bit.py 

# generate the plot comparing DCSCS, GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.5(b)) - entry-cost
python 02-plot-length-optimal-entry.py 

# generate the plot comparing DCSCS, GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.5(c)) - time
python 06-plot-optimal-time.py 

# generate the plot comparing GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.6(a)) - sequence size = 8 - bit-cost
python 03-plot-length-large-bit.py 8 bitcost

# generate the plot comparing GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.6(b)) - sequence size = 16 - bit-cost
python 03-plot-length-large-bit.py 16 bitcost

# generate the plot comparing GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.6(c)) - sequence size = 32 - bit-cost
python 03-plot-length-large-bit.py 32 bitcost

# generate the plot comparing GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.6(d)) - sequence size = 8  - time
python 07-plot-large-time.py 8

# generate the plot comparing GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.6(e)) - sequence size = 16  - time
python 07-plot-large-time.py 16

# generate the plot comparing GREEDY, HIERARCHICAL, and FAST-GREEDY (Fig.6(f)) - sequence size = 32  - time
python 07-plot-large-time.py 32

# generate the plot focusing on FAST-GREEDY (Fig.7(a)) - bit-cost
python 04-plot-length-fast-bit.py bitcost

# generate the plot focusing on FAST-GREEDY (Fig.7(b)) - entry-cost
python 04-plot-length-fast-bit.py entrycost

# generate the plot comparing random sequences with tree-based sequences (Fig.7(c))
python 05-plot-jelly-vs-random.py 16 bitcost
