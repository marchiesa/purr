# Copyright (c) 2015, Malte Schwarzkopf
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of qjump-nsdi15-plotting nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


### Marco, Roshan ###
'''
Adapted from the following original code: 
https://github.com/camsas/qjump-nsdi15-plotting/blob/master/figure1b_3b/plot_memcached_latency_cdfs.py

'''

import os, sys, re
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import pylab
from utils import *
from matplotlib.ticker import FuncFormatter


#x-axis: number-of sequences (fix size_of_sequences to 16)
#y-axis: average(scs-length) over the 6 seeds

paper_mode = True
subset_mode = False

outname = "08-memory-short-sequences"
fnames = []


for i in range(0, len(sys.argv) - 1, 1):
  #mode = sys.argv[2 + i]   
  fnames.append(sys.argv[1 + i])

if paper_mode:
  fig = plt.figure(figsize=(3.33,1.2))
  #fig = plt.figure(figsize=(3.33,12))
  set_paper_rcs()
else:
  fig = plt.figure()
  set_rcs()

colours = ['b', 'g', 'r', 'c', 'm', 'y', 'v']

a64ports = []
a32ports = []
a16ports = []
a8ports = []

a64p = []
a32p = []
a16p = []
a8p = []

a64portsmap = {}
a32portsmap = {}
a16portsmap = {}
a8portsmap = {}

fname = "results-short-sequences.txt"

filenames = []
filenames.append(fname)

dict_list = {"64": a64portsmap, "32": a32portsmap, "16": a16portsmap, "8": a8portsmap}

for f in filenames:
  print("Analyzing file " + str(f) + ":")
  for line in open(f).readlines()[1:]:
    fields = [x.strip() for x in line.split()]
    ports = fields[0] # algo
    nof_sq = int(fields[1]) # random generator
    duplication = int(fields[2]) # random seed
    recirculation = int(fields[3]) # no of sequences
    purr = float(fields[4]) # seq size
    #gain = int(fields[5]) # bitcost/entrycost
    gain = 100*purr/duplication # bitcost/entrycost
    if ports == "64":
      a64portsmap.setdefault(nof_sq, [])
      a64portsmap[nof_sq].append(gain)

    elif ports == "32":
      a32portsmap.setdefault(nof_sq, [])
      a32portsmap[nof_sq].append(gain)

    elif ports == "16":
      a16portsmap.setdefault(nof_sq, [])
      a16portsmap[nof_sq].append(gain)

    elif ports == "8":
      a8portsmap.setdefault(nof_sq, [])
      a8portsmap[nof_sq].append(gain)


for item in dict_list:
  dict_ = dict_list[item]
  for key in sorted(dict_.keys()):
    row = np.average(dict_[key])
    if item == "64":
      a64ports.append(row)
      a64p.append(key)
    elif item == "32":
      a32ports.append(row)
      a32p.append(key)
    elif item == "16":
      a16ports.append(row)
      a16p.append(key)
    elif item == "8":
      a8ports.append(row)
      a8p.append(key)

#iax.set_xscale("log")
plt.xlabel("Length of sequences", fontsize=10)
plt.ylabel("Memory savings [\%]")
plt.gca().set_yscale("log")
locmaj = matplotlib.ticker.LogLocator(base=10,numticks=10)
plt.gca().yaxis.set_major_locator(locmaj)
locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=10)
plt.gca().yaxis.set_minor_locator(locmin)
plt.gca().yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
#plt.yscale('log')
#plt.xscale('log')
#plt.title('sizeofsequence=16')
#plt.ylim(0,2000)
plt.xlim(2, 8)
miny=0.0001
plt.ylim(miny, 100)
#plt.yticks(range(miny, 11000001, 200), [str(x) for x in range(miny, 11000001, 2000000)])

plt.plot(a8p, a8ports, label="8",color='green', lw=1.0, linestyle='-',marker= 'v', mfc='none', mec='green', ms=3)
plt.plot(a32p, a32ports, label="32",color='magenta', lw=1.0, linestyle='-',marker= 's', mfc='none', mec='magenta', ms=3)
#plt.plot(a32p, a32ports, label="32",color='magenta', lw=1.0, linestyle='-',marker= 'x', mfc='none', mec='magenta', ms=3)
plt.plot(a16p, a16ports, label="16",color='red', lw=1.0, linestyle='-',marker= '>', mfc='none', mec='red', ms=3)
plt.plot(a64p, a64ports, label="64",color='blue', lw=1.0, linestyle='-',marker= 'x', mfc='none', mec='blue', ms=3)

plt.legend(loc='upper right', frameon=False, ncol=2, columnspacing=0.8, handletextpad=0.2)
plt.savefig("%s.pdf" % outname, format="pdf", bbox_inches='tight', pad_inches=0.05)

