from ipaddress import ip_address
import sys
import csv

parse_ipv4 = False
parse_port_sets = False
parse_port_seq = False
parse_ports = False
port_set_bit_size = 0
ipv4_lpm_table_size = 2^8
frr_group_table_table_size = 2^8
sequence = []

ipv4_2_frr_id = {}
frr_id_2_seq = {}

port_2_id = {}

p4 = bfrt.purr_pipeline.pipe

# This function can clear all the tables and later on other fixed objects
# once bfrt support is added.
def clear_all(verbose=True, batching=True):
    global p4
    global bfrt
    
    def _clear(table, verbose=False, batching=False):
        if verbose:
            print("Clearing table {:<40} ... ".
                  format(table['full_name']), end='', flush=True)
        try:    
            entries = table['node'].get(regex=True, print_ents=False)
            try:
                if batching:
                    bfrt.batch_begin()
                for entry in entries:
                    entry.remove()
            except Exception as e:
                print("Problem clearing table {}: {}".format(
                    table['name'], e.sts))
            finally:
                if batching:
                    bfrt.batch_end()
        except Exception as e:
            if e.sts == 6:
                if verbose:
                    print('(Empty) ', end='')
        finally:
            if verbose:
                print('Done')

        # Optionally reset the default action, but not all tables
        # have that
        try:
            table['node'].reset_default()
        except:
            pass
    
    # The order is important. We do want to clear from the top, i.e.
    # delete objects that use other objects, e.g. table entries use
    # selector groups and selector groups use action profile members
    

    # Clear Match Tables
    for table in p4.info(return_info=True, print_info=False):
        if table['type'] in ['MATCH_DIRECT', 'MATCH_INDIRECT_SELECTOR']:
            _clear(table, verbose=verbose, batching=batching)

    # Clear Selectors
    for table in p4.info(return_info=True, print_info=False):
        if table['type'] in ['SELECTOR']:
            _clear(table, verbose=verbose, batching=batching)
            
    # Clear Action Profiles
    for table in p4.info(return_info=True, print_info=False):
        if table['type'] in ['ACTION_PROFILE']:
            _clear(table, verbose=verbose, batching=batching)
    
clear_all()

#with open("labs/60-purr/purr.conf", newline='') as csvfile:
with open("labs/60-purr/purr.conf") as csvfile:
    lines = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for line in lines:
        if len(line) == 0:
            continue
        if "SET" in line:
            parse_ipv4 = False
            parse_port_sets = True
            parse_port_seq = False
            parse_ports = False
        elif "IPV4" in line:
        	parse_ipv4 = True
        	parse_port_sets = False
        	parse_port_seq = False
        	parse_ports = False
        elif "SEQUENCE" in line:
        	parse_ipv4 = False
        	parse_port_sets = False
        	parse_port_seq = True
        	parse_ports = False
        elif "PORTS" in line:
        	parse_ipv4 = False
        	parse_port_sets = False
        	parse_port_seq = False
        	parse_ports = True
        	port_set_bit_size = int(line[1].rstrip())
        elif parse_ipv4:
        	ipv4_lpm = line[0].rstrip()
        	ipv4_lpm_mask = line[1].rstrip()
        	frr_id = line[2].rstrip()
        	ipv4_2_frr_id[(ipv4_lpm,ipv4_lpm_mask)] = frr_id
        elif parse_port_seq:
            sequence = line
            parse_port_seq = False
        elif parse_port_sets:
        	frr_id = line[0].rstrip()
        	port_set = line[1].rstrip()
        	frr_id_2_seq[frr_id] = port_set.split("-")
        elif parse_ports:
        	j=0
        	for element in line:
        		port_2_id[element] = j
        		j += 1


ipv4_lpm = p4.Ingress.ipv4_lpm
frr_group_table = p4.Ingress.frr_group_table
fwd_port_table = p4.Ingress.fwd_port_table
#fwd_port_table_table_size = 2^port_set_bit_size
fwd_port_table_table_size = len(sequence)

# populate the all and active servers tables. 
# repeat some servers to implemented weighted load balancing

for (ip,mask) in ipv4_2_frr_id:
	ipv4_lpm.add_with_set_frr_id(dst_addr=ip_address(ip), dst_addr_p_length=mask, frr_id=ipv4_2_frr_id[(ip,mask)])

'''ipv4_lpm.add_with_set_frr_id(
    dst_addr=ip_address('192.168.1.0'), dst_addr_p_length=24, frr_id=1)
ipv4_lpm.add_with_set_frr_id(
    dst_addr=ip_address('192.168.2.0'), dst_addr_p_length=24, frr_id=2)
ipv4_lpm.add_with_set_frr_id(
    dst_addr=ip_address('192.168.3.0'), dst_addr_p_length=24, frr_id=3)
ipv4_lpm.add_with_set_frr_id(
    dst_addr=ip_address('192.168.4.0'), dst_addr_p_length=24, frr_id=4)'''


for id in frr_id_2_seq:
	j=0
	s=""
	done=False
	for i in range(0,len(sequence)):
		if done:
			s += "0"
		elif sequence[i] == frr_id_2_seq[id][j]:
			j += 1
			s += "1"
		else:
			s += "0"
		if j == len(frr_id_2_seq[id]):
			done = True
	port_set_value = int(hex(int(s, 2)), 16)
	frr_group_table.add_with_set_port_set(frr_group_id=id, port_set=port_set_value)

'''frr_group_table.add_with_set_port_set(frr_group_id=1, port_set=0x0078)
frr_group_table.add_with_set_port_set(frr_group_id=2, port_set=0x003C)
frr_group_table.add_with_set_port_set(frr_group_id=3, port_set=0x001E)
frr_group_table.add_with_set_port_set(frr_group_id=4, port_set=0x000F)'''


i = len(sequence)-1
for el in sequence:
	port_set_bit = 2**i
	port_status_bit = 2**(port_2_id[el])
	fwd_port_table.add_with_forward(port_set=port_set_bit, port_set_mask=port_set_bit, port_status=port_status_bit, port_status_mask=port_status_bit, egress_port=int(el))
	i -= 1

'''fwd_port_table.add_with_forward(port_set=value, port_set_mask=value, port_status=0x08, port_status_mask=0x08, egress_port=144)
fwd_port_table.add_with_forward(port_set=0x0020, port_set_mask=0x0020, port_status=0x04, port_status_mask=0x04, egress_port=60)
fwd_port_table.add_with_forward(port_set=0x0010, port_set_mask=0x0010, port_status=0x02, port_status_mask=0x02, egress_port=52)
fwd_port_table.add_with_forward(port_set=0x0008, port_set_mask=0x0008, port_status=0x01, port_status_mask=0x01, egress_port=17)
fwd_port_table.add_with_forward(port_set=0x0004, port_set_mask=0x0004, port_status=0x08, port_status_mask=0x08, egress_port=144)
fwd_port_table.add_with_forward(port_set=0x0002, port_set_mask=0x0002, port_status=0x04, port_status_mask=0x04, egress_port=60)
fwd_port_table.add_with_forward(port_set=0x0001, port_set_mask=0x0001, port_status=0x02, port_status_mask=0x02, egress_port=52)'''

port_status_reg = p4.Ingress.port_status_reg

# set the size of the table
j = port_set_bit_size
s = ""
for x in range(0,port_set_bit_size):
	s += "1"
port_status_reg.mod(register_index=0,f1=s) 

# clean the counters
def clear_counters(table_node):
    for e in table_node.get(regex=True):
        e.data[b'$COUNTER_SPEC_BYTES'] = 0
        e.data[b'$COUNTER_SPEC_PKTS'] = 0
        e.push()

# dump everything
ipv4_lpm.dump(table=True)
frr_group_table.dump(table=True)
fwd_port_table.dump(table=True)
port_status_reg.dump(table=True,from_hw=1)