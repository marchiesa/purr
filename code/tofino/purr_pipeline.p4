/* -*- P4_16 -*- */

#include <core.p4>
#include <tna.p4>

/*************************************************************************
 ************* C O N S T A N T S    A N D   T Y P E S  *******************
**************************************************************************/

const int IPV4_LPM_BIT_SIZE = 8;
const int FRR_GROUP_BIT_SIZE = 8;
const int FWD_PORT_BIT_SIZE = 16;

enum bit<16> ether_type_t {
    TPID = 0x8100,
    IPV4 = 0x0800,
    IPV6 = 0x86DD
}

typedef bit<48>   mac_addr_t;
typedef bit<32>   ipv4_addr_t;

/*************************************************************************
 ***********************  H E A D E R S  *********************************
 *************************************************************************/

/*  Define all the headers the program will recognize             */
/*  The actual sets of headers processed by each gress can differ */

/* Standard ethernet header */
header ethernet_h {
    mac_addr_t    dst_addr;
    mac_addr_t    src_addr;
    ether_type_t  ether_type;
}

header ipv4_h {
    bit<4>       version;
    bit<4>       ihl;
    bit<8>       diffserv;
    bit<16>      total_len;
    bit<16>      identification;
    bit<3>       flags;
    bit<13>      frag_offset;
    bit<8>       ttl;
    bit<8>   	 protocol;
    bit<16>      hdr_checksum;
    ipv4_addr_t  src_addr;
    ipv4_addr_t  dst_addr;
}


/*************************************************************************
 **************  I N G R E S S   P R O C E S S I N G   *******************
 *************************************************************************/
 
    /***********************  H E A D E R S  ************************/


struct my_ingress_headers_t {
    ethernet_h          ethernet;
    ipv4_h              ipv4;
}

    /******  G L O B A L   I N G R E S S   M E T A D A T A  *********/

struct my_ingress_metadata_t {
    bit<16>       port_set;
    bit<8>        frr_group_id;
}

    /***********************  P A R S E R  **************************/
parser IngressParser(packet_in        pkt,
    /* User */    
    out my_ingress_headers_t          hdr,
    out my_ingress_metadata_t         meta,
    /* Intrinsic */
    out ingress_intrinsic_metadata_t  ig_intr_md)
{

    /* This is a mandatory state, required by Tofino Architecture */
     state start {
        pkt.extract(ig_intr_md);
        pkt.advance(PORT_METADATA_SIZE);
        transition meta_init;
    }

    /* User Metadata Initialization */
    state meta_init {
        transition parse_ethernet;
    }

    state parse_ethernet {
        pkt.extract(hdr.ethernet);
        transition select(hdr.ethernet.ether_type) {
            ether_type_t.IPV4 :  parse_ipv4;
            default :  accept;
        }
    }

    state parse_ipv4 {
        pkt.extract(hdr.ipv4);
        transition accept;    
    }

}

    /***************** M A T C H - A C T I O N  *********************/


control Ingress(
    /* User */
    inout my_ingress_headers_t                       hdr,
    inout my_ingress_metadata_t                      meta,
    /* Intrinsic */
    in    ingress_intrinsic_metadata_t               ig_intr_md,
    in    ingress_intrinsic_metadata_from_parser_t   ig_prsr_md,
    inout ingress_intrinsic_metadata_for_deparser_t  ig_dprsr_md,
    inout ingress_intrinsic_metadata_for_tm_t        ig_tm_md)
{
    /* The template type reflects the total width of the counter pair */
    bit<16> port_status;

    Register<bit<16>, bit<32>>(32w1) port_status_reg;
    RegisterAction<bit<16>, bit<32>, bit<16>>(port_status_reg) port_status_reg_read = {
        void apply(inout bit<16> value, out bit<16> read_value){
            read_value = value;
        }
    };

    Register<bit<16>, bit<32>>(32w1) port_status_reg_debug;
    RegisterAction<bit<16>, bit<32>, bit<16>>(port_status_reg_debug) port_status_reg_write = {
        void apply(inout bit<16> value, out bit<16> read_value){
            value = (bit <16>)ig_tm_md.ucast_egress_port;
            //value = 2;
            read_value = 0;
        }
    };

    action forward(bit<9> egress_port) {
        ig_tm_md.ucast_egress_port = egress_port;
        //ig_tm_md.bypass_egress = (bit<1>)true;
    }

    action set_frr_id(bit<8> frr_id) {
        meta.frr_group_id = frr_id;
        //hdr.ipv4.dst_addr = (bit<32)frr_id;
    }

    action set_port_set(bit<16> port_set) {
        meta.port_set = port_set;
    }

    action drop() {
        ig_dprsr_md.drop_ctl = 1;
    }

    table ipv4_lpm {
        key = { hdr.ipv4.dst_addr : lpm; }
        actions = {
            set_frr_id;
            //@defaultonly NoAction;
        }
        const default_action = set_frr_id(3);
        size = 1 << IPV4_LPM_BIT_SIZE;
    }

    table frr_group_table {
        key = { meta.frr_group_id : exact; }
        actions = {
            set_port_set;
            @defaultonly NoAction;
        }
        const default_action = NoAction();
        size = 1 << FRR_GROUP_BIT_SIZE;
    }

    table fwd_port_table {
        key = { meta.port_set : ternary;
                port_status : ternary; }
        actions = {
            forward;
            @defaultonly NoAction;
        }
        const default_action = NoAction();
        size = 1 << FWD_PORT_BIT_SIZE;
    }
   

    apply {
        if(hdr.ipv4.isValid()){
            ipv4_lpm.apply();
            port_status = port_status_reg_read.execute(0);  
            frr_group_table.apply();
            fwd_port_table.apply();
            port_status_reg_write.execute(0); 
            //ig_tm_md.ucast_egress_port = 60;
        }else{
            ig_tm_md.ucast_egress_port = 60;
        }
    }
}

    /*********************  D E P A R S E R  ************************/

control IngressDeparser(packet_out pkt,
    /* User */
    inout my_ingress_headers_t                       hdr,
    in    my_ingress_metadata_t                      meta,
    /* Intrinsic */
    in    ingress_intrinsic_metadata_for_deparser_t  ig_dprsr_md)
{

    apply {
   
        pkt.emit(hdr);
    }
}


/*************************************************************************
 ****************  E G R E S S   P R O C E S S I N G   *******************
 *************************************************************************/

    /***********************  H E A D E R S  ************************/

struct my_egress_headers_t {
}

    /********  G L O B A L   E G R E S S   M E T A D A T A  *********/

struct my_egress_metadata_t {
}

    /***********************  P A R S E R  **************************/

parser EgressParser(packet_in        pkt,
    /* User */
    out my_egress_headers_t          hdr,
    out my_egress_metadata_t         meta,
    /* Intrinsic */
    out egress_intrinsic_metadata_t  eg_intr_md)
{
    /* This is a mandatory state, required by Tofino Architecture */
    state start {
        pkt.extract(eg_intr_md);
        transition accept;
    }
}

    /***************** M A T C H - A C T I O N  *********************/

control Egress(
    /* User */
    inout my_egress_headers_t                          hdr,
    inout my_egress_metadata_t                         meta,
    /* Intrinsic */    
    in    egress_intrinsic_metadata_t                  eg_intr_md,
    in    egress_intrinsic_metadata_from_parser_t      eg_prsr_md,
    inout egress_intrinsic_metadata_for_deparser_t     eg_dprsr_md,
    inout egress_intrinsic_metadata_for_output_port_t  eg_oport_md)
{
    apply {
    }
}

    /*********************  D E P A R S E R  ************************/

control EgressDeparser(packet_out pkt,
    /* User */
    inout my_egress_headers_t                       hdr,
    in    my_egress_metadata_t                      meta,
    /* Intrinsic */
    in    egress_intrinsic_metadata_for_deparser_t  eg_dprsr_md)
{
    apply {
        pkt.emit(hdr);
    }
}


/************ F I N A L   P A C K A G E ******************************/
Pipeline(
    IngressParser(),
    Ingress(),
    IngressDeparser(),
    EgressParser(),
    Egress(),
    EgressDeparser()
) pipe;

Switch(pipe) main;
