#!/bin/bash

cd ns3-simulations-purr
# compile and build the  PURR working directory
CXXFLAGS="--std=c++11 -Wall" ./waf --enable-examples --enable-tests configure
./waf build
cd ..
