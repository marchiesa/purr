#!/bin/bash

# create three working directories for recirculation, control plane with 1 failure, and control plane with two failures
# TODO: we should have one single working directory without code replication
mkdir ns3-simulations-recirculation
mkdir ns3-simulations-reconvergence-one-failure
mkdir ns3-simulations-reconvergence-two-failures

# copy the files from the PURR working directory into these three directories
cp -r ns3-simulations-purr/* ns3-simulations-recirculation/
cp -r ns3-simulations-purr/* ns3-simulations-reconvergence-one-failure/
cp -r ns3-simulations-purr/* ns3-simulations-reconvergence-two-failures/

# import the diff_files into the new working directories
cp -r diff_files/recirculation/* ns3-simulations-recirculation/
cp -r diff_files/reconvergence-one-failure/* ns3-simulations-reconvergence-one-failure/
cp -r diff_files/reconvergence-two-failures/* ns3-simulations-reconvergence-two-failures/

# build all the working directories
for dir in `ls -d ns3-simu*`;
do
    cd $dir
    CXXFLAGS="--std=c++11 -Wall" ./waf --enable-examples --enable-tests configure
    ./waf build
    cd ..
done

