// ------------------------------------------------------------------------
// The simulation script created for an evaluation of the selected
// fast-failover mechanisms in a DCTCP-enabled datacenter network
// Created: October 2018
//
// Scenarios: CIRCULAR
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// The original code to load the network topology was written by Roshan Sedar
// Additional sources:
//   https://github.com/shravya-ks/ns-3-tcp-prague (the implementation of DCTCP)
// ------------------------------------------------------------------------

#include <algorithm>		// Needed for std::string trimming and std::max
#include <cctype>			// Needed for std::string trimming
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>			// strchr()
#include <cassert>
#include <stdint.h>			// uint8_t, uint32_t
#include <stdio.h>			// FILE, fopen(), fread(), fclose()
#include <map>
#include <time.h>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/node-list.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/sender-application.h"		// AK
#include "ns3/receiver-application.h"	// AK
#include "ns3/fast-failover-stats.h"	// AK

// ------------------------------------------------------------------------

using namespace ns3;

NS_LOG_COMPONENT_DEFINE( "DctcpFastFailoverSimulator" );


#define FILE_IO_BUFFER_SIZE_BYTES	16384

#define TRAFFIC_SINKS_START_SEC		  0.0
#define TRAFFIC_GEN_START_SEC		  0.5
#define FAILURE_EVENTS_AT_SEC		( TRAFFIC_GEN_START_SEC + 2.5 )
//#define TRAFFIC_GEN_STOP_SEC		( FAILURE_EVENTS_AT_SEC + 3.5 )
//#define TRAFFIC_GEN_STOP_SEC		( TRAFFIC_GEN_START_SEC + 2.5 )
#define TRAFFIC_GEN_STOP_SEC		( TRAFFIC_GEN_START_SEC + 5.0 )
#define TRAFFIC_SINKS_STOP_SEC		( TRAFFIC_GEN_STOP_SEC + 0.0 )
#define SIMULATION_TIME_SEC		( TRAFFIC_SINKS_STOP_SEC + 0.5 )
#define THROUGHPUT_MONITORING_INTERVAL_SEC	0.001

std::map< std::string, uint32_t > link_utilization;

// ------------------------------------------------------------------------
// Source: https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring

inline std::string trim( const std::string &s )
{
	auto wsfront = std::find_if_not( s.begin(), s.end(), []( int c ){ return std::isspace( c ); } );
	auto wsback = std::find_if_not( s.rbegin(), s.rend(), []( int c ){ return std::isspace( c ); } ).base();
	return ( wsback <= wsfront ? std::string() : std::string( wsfront, wsback ) );
}

// ------------------------------------------------------------------------
// Enable/disable a network interface/port

void set_link_status( Ptr<NetDevice> device, bool enabled )
{
	if ( device == NULL )
	{
		return;
	}

	Ptr<Node> node = device -> GetNode();
	Ptr<Ipv4L3Protocol> ipv4_l3_protocol = node -> GetObject<Ipv4L3Protocol>();

	if ( ipv4_l3_protocol == NULL )
	{
		return;
	}

	int32_t ip_interface = ipv4_l3_protocol -> GetInterfaceForDevice( device );

	if ( ip_interface < 0 )
	{
		NS_LOG_UNCOND( "--> IPv4 interface not found for device " << device -> GetIfIndex() << " on node " << node -> GetId() );

		return;
	}

	if ( enabled )
	{
		ipv4_l3_protocol -> SetUp( ( uint32_t )ip_interface );
		NS_LOG_UNCOND( "--> Changed status of interface " << device -> GetIfIndex() << " on node " << node -> GetId() << " to UP" );
	}
	else
	{
		ipv4_l3_protocol -> SetDown( ( uint32_t )ip_interface );
		NS_LOG_UNCOND( "--> Changed status of interface " << device -> GetIfIndex() << " on node " << node -> GetId() << " to DOWN" );
	}
}

// ------------------------------------------------------------------------
// Print the current simulation time to reflect the progress

void simulation_status_message()
{
	static uint32_t time = 0;
	uint32_t progress_percentage = ( ++time ) * 100.0 / SIMULATION_TIME_SEC;

	NS_LOG_UNCOND( "[ " << progress_percentage << "% ] Simulation time: " << time << " s" );

	Simulator::Schedule( Seconds( 1.0 ), &simulation_status_message );
}

// ------------------------------------------------------------------------
// Check the size of the TX NetDevice queue and write the value to a file

void check_tx_dev_queue_size( Ptr<OutputStreamWrapper> stream, uint32_t old_val, uint32_t new_val )
{
	*stream -> GetStream() << Simulator::Now().GetSeconds() << " " << new_val << std::endl;
}

// ------------------------------------------------------------------------
// Check the size of the NetDevice queue and write the value to a file

void check_dev_queue_size( Ptr< Queue > queue, std::string file_plot_queue )
{
	uint32_t queue_size = queue -> GetNPackets();

	// Check the queue size every 10 ms

	if ( Simulator::Now().GetSeconds() < SIMULATION_TIME_SEC )
	{
		Simulator::Schedule( Seconds( 0.01 ), &check_dev_queue_size, queue, file_plot_queue );
	}
	
	// Write the result to a file

	FILE *output_fd = NULL;
	std::string data_line = std::to_string( Simulator::Now().GetSeconds() ) + " " + std::to_string( queue_size ) + "\r\n";

	if ( ( output_fd = fopen( file_plot_queue.c_str(), "a" ) ) != NULL )
	{
		fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
		fclose( output_fd );
		output_fd = NULL;
	}
}

// ------------------------------------------------------------------------
// Check the size of the RED queue and write the value to a file

//void check_red_queue_size( Ptr<QueueDisc> queue, std::string file_plot_queue )
void check_red_queue_size( std::string file_name, uint32_t old_size, uint32_t new_size )
{
	//uint32_t queue_size = StaticCast<RedQueueDisc>( queue ) -> GetQueueSize();
	/*uint32_t queue_size = StaticCast<RedQueueDisc>( queue ) -> GetNPackets();

	// Check the queue size every 1 ms

	if ( Simulator::Now().GetSeconds() < SIMULATION_TIME_SEC )
	{
		Simulator::Schedule( Seconds( 0.001 ), &check_red_queue_size, queue, file_plot_queue );
	}*/
	
	// Write the result to a file

	FILE *output_fd = NULL;
	std::string data_line = std::to_string( Simulator::Now().GetSeconds() ) + " " + std::to_string( new_size ) + "\r\n";

	if ( ( output_fd = fopen( file_name.c_str(), "a" ) ) != NULL )
	{
		fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
		fclose( output_fd );
		output_fd = NULL;
	}
}

// ------------------------------------------------------------------------

//static void packet_dropped( std::string file_name, Ptr<Packet const> packet )
static void packet_dropped( std::string file_name, Ptr<const QueueItem> item )
{
	FILE *output_fd = fopen( file_name.c_str(), "a" );
	
	if ( output_fd != NULL )
	{
		std::string data_line = std::to_string( Simulator::Now().GetSeconds() ) + " " + std::to_string( 1 ) + "\r\n";
		fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
		fclose( output_fd );
		output_fd = NULL;
	}
}

// ------------------------------------------------------------------------

static void link_utilization_log( std::string file_name, Ptr<Packet const> packet )
{
	if ( link_utilization.find( file_name ) == link_utilization.end() )
	{
		link_utilization[ file_name ] = packet -> GetSize();
	}
	else
	{
		link_utilization[ file_name ] += packet -> GetSize();
	}
}

// ------------------------------------------------------------------------

void link_utilization_snapshot( std::string file_name )
{
	if ( Simulator::Now().GetSeconds() < SIMULATION_TIME_SEC )
	{
		Simulator::Schedule( Seconds( THROUGHPUT_MONITORING_INTERVAL_SEC ), &link_utilization_snapshot, file_name );
	}

	FILE *output_fd = fopen( file_name.c_str(), "a" );

	if ( output_fd != NULL )
	{
		std::string data_line = std::to_string( Simulator::Now().GetSeconds() ) + " " + std::to_string( link_utilization[ file_name ] ) + "\r\n";
		fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
		fclose( output_fd );
		output_fd = NULL;

		link_utilization[ file_name ] = 0;
	}
}

// ------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
	time_t processing_start = time( NULL );

	// ------------------------------------------------------------------------
	// Parse the command line arguments
	// ------------------------------------------------------------------------

	NS_LOG_UNCOND( "Parsing the command line arguments.." );

	uint32_t simulation_run = 1;
	uint32_t data_segment_size_bytes = 1348;
	uint32_t core_link_capacity_mbps = 40000;
	uint32_t access_link_capacity_mbps = 10000;
	double access_link_utilization_factor = 0.9;
	double core_link_delay_ms = 0.01;
	double access_link_delay_ms = 0.01;
	uint32_t persistent_tcp_connections_count = 1;
	uint32_t schedule_multiple_subflows_per_connection = 1;
	uint32_t failures_count = 0;
	std::string file_network_topology;
	std::string file_packet_count_distribution_A;
	std::string file_packet_count_distribution_B;
	
	CommandLine cmd;
	cmd.AddValue( "simRun", "ID of the current simulation run within a single scenario", simulation_run );
	cmd.AddValue( "dataSegmentSize", "Size of the data segment (without headers, in bytes)", data_segment_size_bytes );
	cmd.AddValue( "coreLinkCapacity", "The capacity of core links (in Mbit/s)", core_link_capacity_mbps );
	cmd.AddValue( "accessLinkCapacity", "The capacity of access links (in Mbit/s)", access_link_capacity_mbps );
	cmd.AddValue( "accessLinkUtilizationFactor", "The fraction of capacity of access links that is considered when computing the mean flow inter-arrival time", access_link_utilization_factor );
	cmd.AddValue( "coreLinkDelay", "The delay of core links (in ms)", core_link_delay_ms );
	cmd.AddValue( "accessLinkDelay", "The delay of access links (in ms)", access_link_delay_ms );
	cmd.AddValue( "persistentTCPConnectionsCount", "Number of persistent TCP connections established by each client node", persistent_tcp_connections_count );
	cmd.AddValue( "scheduleMultipleSubflowsPerConnection", "If set to 0, only one subflow will be transmitted within each persistent TCP connection", schedule_multiple_subflows_per_connection );
	cmd.AddValue( "failuresCount", "If > 0, fail one selected link at the specified time", failures_count );
	cmd.AddValue( "networkTopology", "Path to a text file contatining the definition of the network topology", file_network_topology );
	cmd.AddValue( "firstPacketCountDistribution", "Path to a text file containing the first distribution of packet count values per flow (one value per line)", file_packet_count_distribution_A );
	cmd.AddValue( "secondPacketCountDistribution", "[NOT USED] Path to a text file containing the second distribution of packet count values per flow (one value per line)", file_packet_count_distribution_B );
	
	cmd.Parse( argc, argv );

	// ------------------------------------------------------------------------
	// Default settings
	// ------------------------------------------------------------------------
	//LogComponentEnable ("TcpSocketBase", LOG_LEVEL_INFO);
        LogComponentEnable ("TcpDCTCP", LOG_LEVEL_DEBUG);

	//Config::SetDefault( "ns3::TcpL4Protocol::SocketType", StringValue( "ns3::TcpDctcp" ) );
        Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TcpDCTCP::GetTypeId ()));
        Config::SetDefault( "ns3::TcpSocketBase::MinRto", TimeValue( MicroSeconds( 200 ) ) );     // Minimum retransmit timeout value (ns-3 default: 1000 ms)
        //Config::SetDefault( "ns3::TcpSocketBase::MaxRto", TimeValue( MilliSeconds( 10 ) ) );     // Minimum retransmit timeout value (ns-3 default: 1000 ms)
	Config::SetDefault( "ns3::TcpSocket::SndBufSize", UintegerValue( 1 << 21 ) );		// Set to 4 MB
	Config::SetDefault( "ns3::TcpSocket::RcvBufSize", UintegerValue( 1 << 21 ) );		// Set to 4 MB
	Config::SetDefault( "ns3::TcpSocket::SegmentSize", UintegerValue( data_segment_size_bytes ) );		// As in queue examples
	Config::SetDefault( "ns3::TcpSocket::InitialCwnd", UintegerValue( 10 ) );			// The default value on Linux systems equals 10 segments
	Config::SetDefault( "ns3::TcpSocket::DelAckCount", UintegerValue( 0 ) );
        // HERMES parameters follow
	//Config::SetDefault ("ns3::TcpSocket::ConnTimeout", TimeValue (MilliSeconds (5)));
	//Config::SetDefault ("ns3::TcpSocketBase::ClockGranularity", TimeValue (MicroSeconds (100)));
	//Config::SetDefault ("ns3::RttEstimator::InitialEstimation", TimeValue (MicroSeconds (80)));
	//Config::SetDefault ("ns3::TcpSocket::SndBufSize", UintegerValue (160000000));
	//Config::SetDefault ("ns3::TcpSocket::RcvBufSize", UintegerValue (160000000));
	//Config::SetDefault ("ns3::RedQueueDisc::Gentle", BooleanValue (false));
	//if (resequenceBuffer)
        //{
        //    NS_LOG_INFO ("Enabling Resequence Buffer");
	//    Config::SetDefault ("ns3::TcpSocketBase::ResequenceBuffer", BooleanValue (true));
        //    Config::SetDefault ("ns3::TcpResequenceBuffer::InOrderQueueTimerLimit", TimeValue (MicroSeconds (15)));
        //    Config::SetDefault ("ns3::TcpResequenceBuffer::SizeLimit", UintegerValue (100));
        //    Config::SetDefault ("ns3::TcpResequenceBuffer::OutOrderQueueTimerLimit", TimeValue (MicroSeconds (250)));
        //}

	
	GlobalValue::Bind( "ChecksumEnabled", BooleanValue( false ) );

	Config::SetDefault( "ns3::Queue::Mode", StringValue( "QUEUE_MODE_PACKETS" ) );
	Config::SetDefault( "ns3::Queue::MaxPackets", UintegerValue( 1 ) );		// Max Packets allowed in the device queue (ns-3 default is 100)
	
	Config::SetDefault( "ns3::RedQueueDisc::Mode", StringValue( "QUEUE_MODE_PACKETS" ) );
	Config::SetDefault( "ns3::RedQueueDisc::MeanPktSize", UintegerValue( data_segment_size_bytes ) );		// As in queue examples
	Config::SetDefault ("ns3::RedQueueDisc::Gentle", BooleanValue (false));

	// DCTCP tracks instantaneous queue length only; so set QW = 1
	Config::SetDefault( "ns3::RedQueueDisc::QW", DoubleValue( 1 ) );

	// Triumph and Scorpion switches used in the DCTCP Paper have 4 MB of buffer
	// If every IP datagram carries 1400 bytes, 2857 datagrams can be stored in 4 MB
	Config::SetDefault( "ns3::RedQueueDisc::QueueLimit", UintegerValue( 100 ) );

	// MinTh = MaxTh = 17% of the bandwidth-delay product, as recommended in the ACM SIGMETRICS 2011 DCTCP Paper
	// (65 was used in the original SIGCOMM paper and we decided to use the same value)
	Config::SetDefault( "ns3::RedQueueDisc::MinTh", DoubleValue( 15 ) );
	Config::SetDefault( "ns3::RedQueueDisc::MaxTh", DoubleValue( 15 ) );

	// Setting ECN is mandatory for DCTCP
	//Config::SetDefault( "ns3::RedQueueDisc::UseEcn", BooleanValue( true ) );

	// ECMP routing for traffic flows
	Config::SetDefault( "ns3::Ipv4GlobalRouting::PerflowEcmpRouting", BooleanValue( true ) );

	// Let ns-3 update the FIBs immediately after failures
	// (use it in the "ideal"/"control plane" scenario; note that no convergence period is simulated)
	//Config::SetDefault( "ns3::Ipv4GlobalRouting::RespondToInterfaceEvents", BooleanValue( true ) );

	// ------------------------------------------------------------------------
	// Set up the random number generator
	// ------------------------------------------------------------------------

	RngSeedManager::SetSeed( 1 );						// Use a fixed seed
	RngSeedManager::SetRun( simulation_run );			// Change the run id to obtain randomness in a reliable way

	// ------------------------------------------------------------------------
	// Set up the network
	// ------------------------------------------------------------------------

	// -------------------------------------------------------
	// Roshan's code (with necessary modifications)
	// Required by the hardcoded values in the modified src/internet/model/ipv4-l3-protocol.cc file

	NS_LOG_UNCOND( "Loading the network topology.." );

	uint32_t NODES = 42;
	uint32_t LINKS = 48;
	
	NodeContainer c;
	c.Create( NODES );

	InternetStackHelper internet;
	internet.Install( c );

	/*
	 * HULA Figure 4 topology
	 * keep port numbering anti-clockwise for Spines {a1s1=> port 1 for s1}
	 * and clockwise for aggregates {l1a1=> port 1 for a1}
	 */

	FILE *fp = fopen( file_network_topology.c_str(), "r" );

	if ( fp == NULL )
	{
		NS_LOG_UNCOND( "E: Unable to open file " << file_network_topology );

		return -1;
	}

	uint32_t n1, n2;
	NodeContainer p2p_links[ LINKS ];

	for ( uint32_t i = 0; i < LINKS; i++ )
	{
		fscanf( fp, "%u", &n1 );
		fscanf( fp, "%u", &n2 );

		p2p_links[ i ].Add( c.Get( n1 ) );
		p2p_links[ i ].Add( c.Get( n2 ) );
	}

	fclose( fp );
	fp = NULL;

	PointToPointHelper p2p_SA_AL;
	p2p_SA_AL.SetDeviceAttribute( "DataRate", StringValue( ( std::to_string( core_link_capacity_mbps ) + std::string( "Mb/s" ) ).c_str() ) );
	p2p_SA_AL.SetChannelAttribute( "Delay", StringValue( ( std::to_string( core_link_delay_ms ) + std::string( "ms" ) ).c_str() ) );

	PointToPointHelper p2p_LC_LS;
	p2p_LC_LS.SetDeviceAttribute( "DataRate", StringValue( ( std::to_string( access_link_capacity_mbps ) + std::string( "Mb/s" ) ).c_str() ) );
	p2p_LC_LS.SetChannelAttribute( "Delay", StringValue( ( std::to_string( access_link_delay_ms ) + std::string( "ms" ) ).c_str() ) );

	TrafficControlHelper tch_red;
	tch_red.SetRootQueueDisc( "ns3::RedQueueDisc", "LinkBandwidth", StringValue( ( std::to_string( core_link_capacity_mbps ) + std::string( "Mb/s" ) ).c_str() ), "LinkDelay", StringValue( ( std::to_string( core_link_delay_ms ) + std::string( "ms" ) ).c_str() ) );

	NetDeviceContainer devices[ LINKS ];
	NodeContainer S_nodes;
	NodeContainer A_nodes;
	NodeContainer L_nodes;
	NodeContainer all_backbone_nodes;
	NodeContainer client_nodes;
	NodeContainer server_nodes;

	std::string queue_size_file;
	//AsciiTraceHelper ascii_A;
	//AsciiTraceHelper ascii_B;

	//char str[ 100 ];
	//linkutil = new uint32_t[ LINKS ];

	for ( uint32_t i = 0; i < LINKS; i++ )
	{
		if ( i < 16 ) //TODO: link
		{
			// Installed links:  TOR <--> AGGREGATE NODE <--> SPINE NODE

			devices[ i ] = p2p_SA_AL.Install( p2p_links[ i ] );
			QueueDiscContainer red_queues = tch_red.Install( devices[ i ] );

			/*Ptr<PointToPointNetDevice> ptp_port_A = DynamicCast<PointToPointNetDevice>( devices[ i ].Get( 0 ) );
			Ptr<PointToPointNetDevice> ptp_port_B = DynamicCast<PointToPointNetDevice>( devices[ i ].Get( 1 ) );
			
			Ptr< Queue<Packet> > queue_A = ptp_port_A -> GetQueue();
			Ptr< Queue<Packet> > queue_B = ptp_port_B -> GetQueue();

			queue_size_file = "dev_queue_size_node_" + std::to_string( ptp_port_A -> GetNode() -> GetId() ) + "_port_" + std::to_string( ptp_port_A -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".csv";
			Simulator::Schedule( Seconds( TRAFFIC_GEN_START_SEC ), &check_dev_queue_size, queue_A, queue_size_file );
			
			queue_size_file = "dev_queue_size_node_" + std::to_string( ptp_port_B -> GetNode() -> GetId() ) + "_port_" + std::to_string( ptp_port_B -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".csv";
			Simulator::Schedule( Seconds( TRAFFIC_GEN_START_SEC ), &check_dev_queue_size, queue_B, queue_size_file );

			// -------- START experimental

			Ptr<NetDeviceQueueInterface> queue_interface_A = devices[ i ].Get( 0 ) -> GetObject<NetDeviceQueueInterface>();
			Ptr<NetDeviceQueueInterface> queue_interface_B = devices[ i ].Get( 1 ) -> GetObject<NetDeviceQueueInterface>();

			Ptr<NetDeviceQueue> tx_queue_interface_A = queue_interface_A -> GetTxQueue( 0 );
			Ptr<NetDeviceQueue> tx_queue_interface_B = queue_interface_B -> GetTxQueue( 0 );

			Ptr<DynamicQueueLimits> tx_queue_limits_A = StaticCast<DynamicQueueLimits>( tx_queue_interface_A -> GetQueueLimits() );
			Ptr<DynamicQueueLimits> tx_queue_limits_B = StaticCast<DynamicQueueLimits>( tx_queue_interface_B -> GetQueueLimits() );

			queue_size_file = "tx_dev_queue_size_node_" + std::to_string( ptp_port_A -> GetNode() -> GetId() ) + "_port_" + std::to_string( ptp_port_A -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".txt";
			Ptr<OutputStreamWrapper> stream_limits_A = ascii_A.CreateFileStream( queue_size_file );

			queue_size_file = "tx_dev_queue_size_node_" + std::to_string( ptp_port_B -> GetNode() -> GetId() ) + "_port_" + std::to_string( ptp_port_B -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".txt";
			Ptr<OutputStreamWrapper> stream_limits_B = ascii_B.CreateFileStream( queue_size_file );
			
			tx_queue_limits_A -> TraceConnectWithoutContext( "Limit", MakeBoundCallback( &check_tx_dev_queue_size, stream_limits_A ) );
			tx_queue_limits_B -> TraceConnectWithoutContext( "Limit", MakeBoundCallback( &check_tx_dev_queue_size, stream_limits_B ) );*/

			// -------- END experimental
			
			Ptr<QueueDisc> queue_A = red_queues.Get( 0 );
			Ptr<QueueDisc> queue_B = red_queues.Get( 1 );

			Ptr<NetDevice> port_A = devices[ i ].Get( 0 );
			Ptr<NetDevice> port_B = devices[ i ].Get( 1 );
			
			queue_size_file = "red_queue_size_node_" + std::to_string( port_A -> GetNode() -> GetId() ) + "_port_" + std::to_string( port_A -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".csv";
			queue_A -> TraceConnect( "PacketsInQueue", queue_size_file, MakeCallback( &check_red_queue_size ) );
			//Simulator::Schedule( Seconds( TRAFFIC_GEN_START_SEC ), &check_red_queue_size, queue_A, queue_size_file );
			
			queue_size_file = "red_queue_size_node_" + std::to_string( port_B -> GetNode() -> GetId() ) + "_port_" + std::to_string( port_B -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".csv";
			queue_B -> TraceConnect( "PacketsInQueue", queue_size_file, MakeCallback( &check_red_queue_size ) );
			//Simulator::Schedule( Seconds( TRAFFIC_GEN_START_SEC ), &check_red_queue_size, queue_B, queue_size_file );

			queue_size_file = "red_queue_drops_node_" + std::to_string( port_A -> GetNode() -> GetId() ) + "_port_" + std::to_string( port_A -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".csv";
			queue_A -> TraceConnect( "Drop", queue_size_file, MakeCallback( &packet_dropped ) );
			
			queue_size_file = "red_queue_drops_node_" + std::to_string( port_B -> GetNode() -> GetId() ) + "_port_" + std::to_string( port_B -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".csv";
			queue_B -> TraceConnect( "Drop", queue_size_file, MakeCallback( &packet_dropped ) );

			queue_size_file = "link_utilization_node_" + std::to_string( port_A -> GetNode() -> GetId() ) + "_port_" + std::to_string( port_A -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".csv";
			port_A -> GetObject< PointToPointNetDevice >() -> TraceConnect( "MacTx", queue_size_file, MakeCallback( &link_utilization_log ) );
			Simulator::Schedule( Seconds( TRAFFIC_GEN_START_SEC ), &link_utilization_snapshot, queue_size_file );

			queue_size_file = "link_utilization_node_" + std::to_string( port_B -> GetNode() -> GetId() ) + "_port_" + std::to_string( port_B -> GetIfIndex() ) + "_run_" + std::to_string( simulation_run ) + ".csv";
			port_B -> GetObject< PointToPointNetDevice >() -> TraceConnect( "MacTx", queue_size_file, MakeCallback( &link_utilization_log ) );
			Simulator::Schedule( Seconds( TRAFFIC_GEN_START_SEC ), &link_utilization_snapshot, queue_size_file );

		}
		else
		{
			// Installed links:  TOR <--> CLIENT/SERVER

			devices[ i ] = p2p_LC_LS.Install( p2p_links[ i ] );
		}

		/*linkutil[ i ] = 0;
		sprintf(str, "%d", i);
		devices[i].Get(0)->GetObject<PointToPointNetDevice>() -> TraceConnect("MacTx", str, MakeCallback(&LinkUtilLog));
		sprintf(str, "%d", i);
		devices[i].Get(1)->GetObject<PointToPointNetDevice>() -> TraceConnect("MacTx", str, MakeCallback(&LinkUtilLog));*/
	}

	// Node indices based on the 'topo_general.txt' file
    
	S_nodes.Add( c.Get( 8 ) );
	S_nodes.Add( c.Get( 9 ) );

	A_nodes.Add( c.Get( 4 ) );
	A_nodes.Add( c.Get( 5 ) );
	A_nodes.Add( c.Get( 6 ) );
	A_nodes.Add( c.Get( 7 ) );

	L_nodes.Add( c.Get( 0 ) );
	L_nodes.Add( c.Get( 1 ) );
	L_nodes.Add( c.Get( 2 ) );
	L_nodes.Add( c.Get( 3 ) );

	all_backbone_nodes.Add( S_nodes );
	all_backbone_nodes.Add( A_nodes );
	all_backbone_nodes.Add( L_nodes );

	client_nodes.Add( c.Get( 10 ) );
	client_nodes.Add( c.Get( 11 ) );
	client_nodes.Add( c.Get( 12 ) );
	client_nodes.Add( c.Get( 13 ) );
	client_nodes.Add( c.Get( 14 ) );
	client_nodes.Add( c.Get( 15 ) );
	client_nodes.Add( c.Get( 16 ) );
	client_nodes.Add( c.Get( 17 ) );

	client_nodes.Add( c.Get( 18 ) );
	client_nodes.Add( c.Get( 19 ) );
	client_nodes.Add( c.Get( 20 ) );
	client_nodes.Add( c.Get( 21 ) );
	client_nodes.Add( c.Get( 22 ) );
	client_nodes.Add( c.Get( 23 ) );
	client_nodes.Add( c.Get( 24 ) );
	client_nodes.Add( c.Get( 25 ) );

	server_nodes.Add( c.Get( 26 ) );
	server_nodes.Add( c.Get( 27 ) );
	server_nodes.Add( c.Get( 28 ) );
	server_nodes.Add( c.Get( 29 ) );
	server_nodes.Add( c.Get( 30 ) );
	server_nodes.Add( c.Get( 31 ) );
	server_nodes.Add( c.Get( 32 ) );
	server_nodes.Add( c.Get( 33 ) );

	server_nodes.Add( c.Get( 34 ) );
	server_nodes.Add( c.Get( 35 ) );
	server_nodes.Add( c.Get( 36 ) );
	server_nodes.Add( c.Get( 37 ) );
	server_nodes.Add( c.Get( 38 ) );
	server_nodes.Add( c.Get( 39 ) );
	server_nodes.Add( c.Get( 40 ) );
	server_nodes.Add( c.Get( 41 ) );

	// Recirculation scenario:
	// 10, 11, 12 ,13 -- additional virtual nodes for recirculation, they are placed between A1-S1, A1-S2, A2-S1, A2-S2
	// 8-10, 9-13 -- LOOP LINKS

	//
	// Assign IPv4 addresses to network interfaces
	//

	NS_LOG_UNCOND( "Assigning network addresses.." );

	Ipv4AddressHelper address[ LINKS ];
	Ipv4Address baseAddress1( "10.1.1.0" );
	uint32_t baseAddressNo1 = baseAddress1.Get();
	uint32_t addressNo1;

	for ( uint32_t i = 0; i < LINKS; i++ )
	{
		addressNo1 = baseAddressNo1 + ( i * 256 );
		baseAddress1.Set( addressNo1 );
		address[ i ].SetBase( baseAddress1, "255.255.255.0" );
	}

	Ipv4InterfaceContainer interfaces[ LINKS ];

	for ( uint32_t i = 0; i < LINKS; i++ )
	{
		interfaces[ i ] = address[ i ].Assign( devices[ i ] );
		//NS_LOG_UNCOND(" link >"<<i<<" ip address "<<interfaces[i].GetAddress(1)<<"\n");
	}

	Ptr<NetDevice> link_s1_a3 = devices[ 9 ].Get( 1 );
	Ptr<NetDevice> link_a3_s1 = devices[ 9 ].Get( 0 );

	Ptr<NetDevice> link_s2_a4 = devices[ 14 ].Get( 1 );
	Ptr<NetDevice> link_a4_s2 = devices[ 14 ].Get( 0 );

	// End of Roshan's code (with modifications)
	// -------------------------------------------------------

	//
	// Turn on static IPv4 routing
	//

	NS_LOG_UNCOND( "Setting up the static global routing scheme.." );
	Ipv4GlobalRoutingHelper::PopulateRoutingTables();

	//
	// Install one server application on each of the server nodes
	//

	NS_LOG_UNCOND( "Installing packet sinks on server nodes.." );

	uint16_t port = 50000;
	Address sink_address( InetSocketAddress( Ipv4Address::GetAny(), port ) );
	ApplicationContainer server_applications;
	Ptr<ReceiverApp> server_app_ptr = NULL;
	
	for ( NodeContainer::Iterator it_node = server_nodes.Begin(); it_node != server_nodes.End(); ++it_node )
	{
		server_app_ptr = CreateObject<ReceiverApp>();

		if ( server_app_ptr == NULL )
		{
			NS_LOG_UNCOND( "E: Unable to create a ReceiverApp object." );

			return -1;
		}

		server_app_ptr -> Setup( sink_address );
		server_app_ptr -> SetFCTCollectionStartTime( 0.0 );

		( *it_node ) -> AddApplication( server_app_ptr );
		server_applications.Add( server_app_ptr );
	}

	server_applications.Start( Seconds( TRAFFIC_SINKS_START_SEC ) );
	server_applications.Stop( Seconds( TRAFFIC_SINKS_STOP_SEC ) );

	//
	// Each client node establishes three persistent TCP connections with the randomly-selected server node
	//
	
	// Load the packet count distribution from an external file

	NS_LOG_UNCOND( "Loading the first packet count distribution.." );
	std::vector<uint32_t> packet_count_distribution;

	if ( !file_packet_count_distribution_A.empty() )
	{
		std::string data_line;
		std::ifstream input_file( file_packet_count_distribution_A.c_str() );
		
		if ( input_file.is_open() )
		{
			while ( std::getline( input_file, data_line ) ) 
			{
				data_line = trim( data_line );

				if ( data_line.empty() )
				{
					continue;
				}

				packet_count_distribution.push_back( ( uint32_t )std::stoul( data_line ) );
			}
		}
		else
		{
			NS_LOG_UNCOND( "E: Unable to open the input file " << file_packet_count_distribution_A );

			return -1;
		}

		NS_LOG_UNCOND( "--> The distribution contains " << packet_count_distribution.size() << " values" );
	}

	// Configure persistent TCP connections

	NS_LOG_UNCOND( "Configuring persistent TCP connections.." );

	Ptr<UniformRandomVariable> dst_node_index = CreateObject<UniformRandomVariable>();
	dst_node_index -> SetAttribute( "Min", DoubleValue( 0 ) );
	dst_node_index -> SetAttribute( "Max", DoubleValue( server_nodes.GetN() - 1 ) );

	ApplicationContainer client_applications;
	Ptr<SenderApp> client_app_ptr = NULL;
	Ptr<Node> dst_node_ptr = NULL;
	Ptr<Ipv4> ipv4 = NULL;

	Ipv4InterfaceAddress ipv4_int_addr;
	Ipv4Address src_ip;
	Ipv4Address dst_ip;

	uint32_t client_application_id = 0;
	
	// DEBUG: establish only one connection
	//for ( NodeContainer::Iterator it_node = client_nodes.Begin(); it_node != client_nodes.End(); ++it_node )
	{
		NodeContainer::Iterator it_node = client_nodes.Begin();

		// Select the destination node
		
		dst_node_ptr = server_nodes.Get( dst_node_index -> GetInteger() );

		// Configure three persistent TCP connections (each handled by one client application)

		ipv4 = ( *it_node ) -> GetObject<Ipv4>();
		ipv4_int_addr = ipv4 -> GetAddress( 1, 0 );
		src_ip = ipv4_int_addr.GetLocal();

		ipv4 = dst_node_ptr -> GetObject<Ipv4>();
		ipv4_int_addr = ipv4 -> GetAddress( 1, 0 );
		dst_ip = ipv4_int_addr.GetLocal();

		Address server_address( InetSocketAddress( dst_ip, port ) );
		
		for ( uint32_t i = 0; i < persistent_tcp_connections_count; i++ )
		{
			client_app_ptr = CreateObject<SenderApp>();

			if ( client_app_ptr == NULL )
			{
				NS_LOG_UNCOND( "E: Unable to create a SenderApp object." );

				return -1;
			}

			( *it_node ) -> AddApplication( client_app_ptr );	// Needed before Setup(): the socket must be associated with a network node
			client_applications.Add( client_app_ptr );

			client_app_ptr -> Setup( client_application_id, server_address, data_segment_size_bytes, &packet_count_distribution, access_link_capacity_mbps, access_link_utilization_factor, persistent_tcp_connections_count, SIMULATION_TIME_SEC, THROUGHPUT_MONITORING_INTERVAL_SEC );
			client_app_ptr -> ScheduleMultipleSubflowsPerConnection( ( bool )( schedule_multiple_subflows_per_connection != 0 ) );
			
			client_application_id++;
			NS_LOG_UNCOND( "--> Added a new persistent TCP connection [Client Application ID: " << client_application_id << "]: " << src_ip << " --> " << dst_ip );
		}
	}

	client_applications.Start( Seconds( TRAFFIC_GEN_START_SEC ) );
	client_applications.Stop( Seconds( TRAFFIC_GEN_STOP_SEC ) );

	// ------------------------------------------------------------------------
	// Install one instance of the FastFailoverStats object on each backbone node incident with a failed link
	// See the 'Schedule link failures' section below for details
	// ------------------------------------------------------------------------

	NS_LOG_UNCOND( "Installing FastFailoverStats modules on the backbone nodes incident with failed links.." );

	NodeContainer affected_nodes;
	affected_nodes.Add( link_s1_a3 -> GetNode() );
	affected_nodes.Add( link_a3_s1 -> GetNode() );

	Ptr<FastFailoverStats> fast_failover_stats = NULL;

	for ( NodeContainer::Iterator it_node = affected_nodes.Begin(); it_node != affected_nodes.End(); ++it_node )
	{
		// Check if the object has already been installed on the node
		// Do not install it multiple times

		dst_node_ptr = *it_node;
		fast_failover_stats = dst_node_ptr -> GetObject<FastFailoverStats>();

		if ( fast_failover_stats != NULL )
		{
			NS_LOG_UNCOND( "--> The FastFailoverStats module has already been installed on node " << dst_node_ptr -> GetId() );

			continue;
		}

		// No predefined object - create a new one

		ObjectFactory factory;
		factory.SetTypeId( "ns3::FastFailoverStats" );
		Ptr<Object> protocol = factory.Create<Object>();
		dst_node_ptr -> AggregateObject( protocol );

		fast_failover_stats = dst_node_ptr -> GetObject<FastFailoverStats>();

		if ( fast_failover_stats == NULL )
		{
			NS_LOG_UNCOND( "--> Unable to create a new FastFailoverStats module on node " << dst_node_ptr -> GetId() );

			continue;
		}

		fast_failover_stats -> SetNode( dst_node_ptr );
		fast_failover_stats -> SetIpv4L3Protocol( dst_node_ptr -> GetObject<Ipv4L3Protocol>() );

		// Register all flows forwarded through a specific network device
		// Use only in the 'control plane with no failures' scenario

		if ( link_s1_a3 -> GetNode() == dst_node_ptr )
		{
			fast_failover_stats -> SetOutputDevice( link_s1_a3 );
		}
		else if ( link_a3_s1 -> GetNode() == dst_node_ptr )
		{
			fast_failover_stats -> SetOutputDevice( link_a3_s1 );
		}
	}

	// ------------------------------------------------------------------------
	// Schedule link failures
	// ------------------------------------------------------------------------

	NetDeviceContainer all_failed_ports;

	if ( failures_count > 0 )
	{
		NS_LOG_UNCOND( "Scheduling link failure events.." );

		// Failure of link S1-A3

		Simulator::Schedule( Seconds( FAILURE_EVENTS_AT_SEC ), &set_link_status, link_s1_a3, false );
		Simulator::Schedule( Seconds( FAILURE_EVENTS_AT_SEC ), &set_link_status, link_a3_s1, false );
		NS_LOG_UNCOND( "--> Scheduled a link failure event at " << FAILURE_EVENTS_AT_SEC << " s (" << link_s1_a3 -> GetNode() -> GetId() << " <--> " << link_a3_s1 -> GetNode() -> GetId() << ")" );

		all_failed_ports.Add( link_s1_a3 );
		all_failed_ports.Add( link_a3_s1 );

		// Failure of link S2-A4

		//Simulator::Schedule( Seconds( FAILURE_EVENTS_AT_SEC ), &set_link_status, link_s2_a4, false );
		//Simulator::Schedule( Seconds( FAILURE_EVENTS_AT_SEC ), &set_link_status, link_a4_s2, false );
		//NS_LOG_UNCOND( "--> Scheduled a link failure event at " << FAILURE_EVENTS_AT_SEC << " s (" << link_s2_a4 -> GetNode() -> GetId() << " <--> " << link_a4_s2 -> GetNode() -> GetId() << ")" );

		//all_failed_ports.Add( link_s2_a4 );
		//all_failed_ports.Add( link_a4_s2 );
	}

	// ------------------------------------------------------------------------
	// Set up the flow monitor
	// ------------------------------------------------------------------------

	/*NS_LOG_UNCOND( "Configuring the flow monitor.." );

	Ptr<FlowMonitor> flow_monitor;
	FlowMonitorHelper flow_monitor_helper;

	flow_monitor = flow_monitor_helper.InstallAll();
	flow_monitor -> Start( Seconds( TRAFFIC_SINKS_START_SEC ) );
	flow_monitor -> Stop( Seconds( SIMULATION_TIME_SEC ) );*/

	//p2p_SA_AL.EnablePcapAll ("pcap/recirc");
	
	// ------------------------------------------------------------------------
	// Start the simulation
	// ------------------------------------------------------------------------
	
	NS_LOG_UNCOND( "Starting the simulation.." );

	Simulator::Schedule( Seconds( 1.0 ), &simulation_status_message );
	Simulator::Stop( Seconds( SIMULATION_TIME_SEC ) );
	Simulator::Run();

	// ------------------------------------------------------------------------
	// Collect the results and compute metrics
	// ------------------------------------------------------------------------

	NS_LOG_UNCOND( "Collecting the results, computing metrics, and preparing output data sets.." );

	/*flow_monitor -> CheckForLostPackets();
	FlowMonitor::FlowStatsContainer flow_stats = flow_monitor -> GetFlowStats();
	//flow_monitor -> SerializeToXmlFile( "results.xml", true, true );

	std::list<uint32_t> list_total_lost_packets;
	std::list<double> list_avg_throughput_mbps;
	double temp;
	FlowMonitor::FlowStatsContainer::iterator it_flow;

	for ( it_flow = flow_stats.begin(); it_flow != flow_stats.end(); it_flow++ )
	{
		list_total_lost_packets.push_front( it_flow -> second.lostPackets );

		temp = ( double )it_flow -> second.rxBytes * 8.0 / ( double )( it_flow -> second.timeLastRxPacket.GetSeconds() - it_flow -> second.timeFirstRxPacket.GetSeconds() ) / 1000000.0;
		list_avg_throughput_mbps.push_front( temp );
	}

	//
	// Sort the values on the lists
	//

	list_total_lost_packets.sort();
	list_avg_throughput_mbps.sort();

	//
	// Append the values to the corresponding files for further processing
	//

	std::list<uint32_t>::iterator it_total_lost_packets;
	std::list<double>::iterator it_avg_throughput_mbps;
	
	// Total number of lost packets

	std::string data_line;
	std::string output_file( "all_trials_total_lost_packets.csv" );
	FILE *output_fd = fopen( output_file.c_str(), "a" );

	if ( output_fd != NULL )
	{
		for ( it_total_lost_packets = list_total_lost_packets.begin(); it_total_lost_packets != list_total_lost_packets.end(); it_total_lost_packets++ )
		{
			data_line += std::to_string( *it_total_lost_packets ) + " ";
		}

		data_line = trim( data_line ) + "\r\n";
		fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
		fclose( output_fd );
	}

	// Average throughput

	output_file = "all_trials_avg_throughput_mbps.csv";
	data_line.clear();

	if ( ( output_fd = fopen( output_file.c_str(), "a" ) ) != NULL )
	{
		for ( it_avg_throughput_mbps = list_avg_throughput_mbps.begin(); it_avg_throughput_mbps != list_avg_throughput_mbps.end(); it_avg_throughput_mbps++ )
		{
			data_line += std::to_string( *it_avg_throughput_mbps ) + " ";
		}

		data_line = trim( data_line ) + "\r\n";
		fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
		fclose( output_fd );
	}

	output_fd = NULL;

	//
	// Save information about the minimum observed TTL values
	//
	
	{ // To avoid variable redefinition conflicts
		std::map< std::string, uint8_t > *flow_min_ttl_ptr = NULL;
		std::map< std::string, uint8_t >::iterator it_flow_min_ttl;
		Ptr<ReceiverApp> app_ptr = NULL;

		output_file = "flow_min_observed_ttl_run_" + std::to_string( simulation_run ) + ".csv";
		data_line.clear();

		if ( ( output_fd = fopen( output_file.c_str(), "w" ) ) != NULL )
		{
			data_line = "#<FlowID> <MinTTL>\r\n";
			fwrite( data_line.c_str(), 1, data_line.size(), output_fd );

			for ( ApplicationContainer::Iterator it_app = server_applications.Begin(); it_app != server_applications.End(); ++it_app )
			{
				app_ptr = DynamicCast<ReceiverApp>( *it_app );
				flow_min_ttl_ptr = app_ptr -> GetFlowMinObservedTTLs();

				for ( it_flow_min_ttl = flow_min_ttl_ptr -> begin(); it_flow_min_ttl != flow_min_ttl_ptr -> end(); it_flow_min_ttl++ )
				{
					data_line = it_flow_min_ttl -> first + std::string( " " ) + std::to_string( it_flow_min_ttl -> second ) + "\r\n";
					fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
				}
			}

			fclose( output_fd );
			output_fd = NULL;
		}
	}*/

	//
	// Save information about the flow start time, flow end time, and Flow Completion Time (FCT)
	//

	{ // To avoid variable redefinition conflicts
		std::map< std::string, double > *flow_start_times_ptr = NULL;
		std::map< std::string, double > *flow_stop_times_ptr = NULL;
		std::map< std::string, double > *fct_ptr = NULL;
		std::map< std::string, double >::iterator it_flow_id;
		Ptr<ReceiverApp> app_ptr = NULL;
		FILE *output_fd = NULL;

		std::string output_file = "flow_completion_time_run_" + std::to_string( simulation_run ) + ".csv";
		std::string data_line;

		if ( ( output_fd = fopen( output_file.c_str(), "w" ) ) != NULL )
		{
			data_line = "#<FlowID> <FlowScheduledAtTimeSec> <FlowEndTimeSec> <FlowCompletionTimeSec>\r\n";
			fwrite( data_line.c_str(), 1, data_line.size(), output_fd );

			for ( ApplicationContainer::Iterator it_app = server_applications.Begin(); it_app != server_applications.End(); ++it_app )
			{
				app_ptr = DynamicCast<ReceiverApp>( *it_app );
				flow_start_times_ptr = app_ptr -> GetFlowStartTimes();
				flow_stop_times_ptr = app_ptr -> GetFlowStopTimes();
				fct_ptr = app_ptr -> GetFlowCompletionTimes();

				for ( it_flow_id = fct_ptr -> begin(); it_flow_id != fct_ptr -> end(); it_flow_id++ )
				{
					data_line = it_flow_id -> first + std::string( " " ) + 
								std::to_string( ( *flow_start_times_ptr )[ it_flow_id -> first ] ) + std::string( " " ) + 
								std::to_string( ( *flow_stop_times_ptr )[ it_flow_id -> first ] ) + std::string( " " ) + 
								std::to_string( it_flow_id -> second ) + "\r\n";
					fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
				}
			}

			fclose( output_fd );
			output_fd = NULL;
		}
	}

	//
	// Save information about the size of flows
	//

	{ // To avoid variable redefinition conflicts
		std::map< std::string, uint64_t > *flow_size_map_ptr = NULL;
		std::map< std::string, uint64_t >::iterator it_flow_id;
		Ptr<SenderApp> app_ptr = NULL;
		FILE *output_fd = NULL;

		std::string output_file = "flow_size_run_" + std::to_string( simulation_run ) + ".csv";
		std::string data_line;

		if ( ( output_fd = fopen( output_file.c_str(), "w" ) ) != NULL )
		{
			data_line = "#<FlowID> <FlowSizeBytes>\r\n";
			fwrite( data_line.c_str(), 1, data_line.size(), output_fd );

			for ( ApplicationContainer::Iterator it_app = client_applications.Begin(); it_app != client_applications.End(); ++it_app )
			{
				app_ptr = DynamicCast<SenderApp>( *it_app );
				flow_size_map_ptr = app_ptr -> GetFlowSizeMap();

				for ( it_flow_id = flow_size_map_ptr -> begin(); it_flow_id != flow_size_map_ptr -> end(); it_flow_id++ )
				{
					data_line = it_flow_id -> first + std::string( " " ) + std::to_string( it_flow_id -> second ) + "\r\n";
					fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
				}
			}

			fclose( output_fd );
			output_fd = NULL;
		}
	}

	//
	// Save information about flows affected by link failures
	//

	//if ( failures_count > 0 )
	{
		std::map< std::string, uint32_t > *forwarded_flows_ptr = NULL;
		std::map< std::string, uint32_t >::iterator it_flow_id;
		FILE *output_fd = NULL;
		
		std::string output_file = "flows_affected_by_failures_run_" + std::to_string( simulation_run ) + ".csv";
		std::string data_line;

		if ( ( output_fd = fopen( output_file.c_str(), "w" ) ) != NULL )
		{
			data_line = "#<FlowID> <ForwardedByNode> <FailedOutPort>\r\n";
			fwrite( data_line.c_str(), 1, data_line.size(), output_fd );

			for ( NetDeviceContainer::Iterator it_port = all_failed_ports.Begin(); it_port != all_failed_ports.End(); ++it_port )
			{
				dst_node_ptr = ( *it_port ) -> GetNode();
				fast_failover_stats = dst_node_ptr -> GetObject<FastFailoverStats>();

				if ( fast_failover_stats == NULL )
				{
					NS_LOG_UNCOND( "--> Unable to access the FastFailoverStats module installed on node " << dst_node_ptr -> GetId() );

					continue;
				}

				forwarded_flows_ptr = fast_failover_stats -> GetForwardedFlows();

				for ( it_flow_id = forwarded_flows_ptr -> begin(); it_flow_id != forwarded_flows_ptr -> end(); it_flow_id++ )
				{
					if ( it_flow_id -> second != ( *it_port ) -> GetIfIndex() )
					{
						continue;
					}

					data_line = it_flow_id -> first + std::string( " " ) + 
								std::to_string( dst_node_ptr -> GetId() ) + std::string( " " ) + 
								std::to_string( it_flow_id -> second ) + "\r\n";
					fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
				}
			}

			fclose( output_fd );
			output_fd = NULL;
		}
	}

	// ------------------------------------------------------------------------
	// Done
	// ------------------------------------------------------------------------

	Simulator::Destroy();
	time_t processing_stop = time( NULL );

	NS_LOG_UNCOND( "Done (processing time: " << processing_stop - processing_start << " s)" );
}

// ------------------------------------------------------------------------
