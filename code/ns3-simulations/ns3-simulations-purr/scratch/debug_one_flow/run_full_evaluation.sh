#!/bin/bash
# ------------------------------------------------------------------------
# The simulation script created for an evaluation of the selected
# fast-failover mechanisms in a DCTCP-enabled datacenter network
#
# Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
#
# To run the script:
#   cd /home/(...)/dctcp_fast_failover
#   ./scratch/sigmetrics_dctcp/run_full_evaluation.sh
#
# Remember to adjust the constants below to match your environment.

# -----------------------------------------------------------------------------
# CONSTANTS

APP_NAME="debug_one_big"
PROC_GREP="debug_one_b"
DATASET_DIR="/home/mchiesa/dctcp_experiments/dctcp_fast_failover_node_163/scratch/${APP_NAME}/input_data"
WORK_DIR="scratch/${APP_NAME}/simout"
MAX_PROC=1

SIMULATION_RUNS=1
SIMULATION_COUNTER=0

#ALGORITHM="mode_circular"
#ALGORITHM="mode_control_plane_with_failure"
ALGORITHM="mode_control_plane_no_failures"
#ALGORITHM="mode_recirculation"
DATA_SEGMENT_SIZE_BYTES=1348		# Headers/overhead: 12 + 20 (TCP) + 20 (IPv4) bytes
CORE_LINK_CAPACITY_MBPS=40000
ACCESS_LINK_CAPACITY_MBPS=10000
ACCESS_LINK_UTILIZATION_FACTOR="0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9"	# Modify to change the network load
CORE_LINK_DELAY_MS=0.001
ACCESS_LINK_DELAY_MS=0.01
PERSISTENT_TCP_CONNECTIONS_COUNT=1
SCHEDULE_MULTIPLE_SUBFLOWS_PER_CONNECTION=0
FAILURES_COUNT=0
NETWORK_TOPOLOGY="${DATASET_DIR}/topo_general.txt"
#NETWORK_TOPOLOGY="${DATASET_DIR}/topo_recirculation.txt"
#TRAFFIC_WORKLOAD_LIST="cdf_dctcp_websearch cdf_vl2_datamining"
#TRAFFIC_WORKLOAD_LIST="cdf_dctcp_websearch"
#TRAFFIC_WORKLOAD_LIST="cdf_vl2_datamining"
TRAFFIC_WORKLOAD_LIST="cdf_one_large_flow.txt"
SECOND_PACKET_COUNT_DISTRIBUTION="not_used"

# -----------------------------------------------------------------------------
# SIMULATION STEPS

for sim_run in `seq 1 ${SIMULATION_RUNS}`
do
	for sim_traffic_workload in ${TRAFFIC_WORKLOAD_LIST}
	do
		for sim_link_utilization_factor in ${ACCESS_LINK_UTILIZATION_FACTOR}
		do
			sleep 5
			
			while [[ `pgrep ${PROC_GREP} | wc -l` -ge ${MAX_PROC} ]]
			do
				sleep 5
			done

			SIMULATION_COUNTER=$[ ${SIMULATION_COUNTER} + 1 ]
			CURRENT_DATE_TIME=`date`
			echo "[${CURRENT_DATE_TIME}] Simulation ${SIMULATION_COUNTER}, ${ALGORITHM}, access_link utilization factor ${sim_link_utilization_factor}, traffic workload ${sim_traffic_workload}, run ${sim_run} out of ${SIMULATION_RUNS}"

			output_dir="${WORK_DIR}/${sim_traffic_workload}/${ALGORITHM}/access_link_utilization_factor_${sim_link_utilization_factor}"
			mkdir -p ${output_dir}
			mkdir -p "${WORK_DIR}/${sim_traffic_workload}/plots"

			SIMULATION_ARGS="--simRun=$[ ${SIMULATION_COUNTER} ] --dataSegmentSize=${DATA_SEGMENT_SIZE_BYTES} --coreLinkCapacity=${CORE_LINK_CAPACITY_MBPS} --accessLinkCapacity=${ACCESS_LINK_CAPACITY_MBPS} --accessLinkUtilizationFactor=${sim_link_utilization_factor} --coreLinkDelay=${CORE_LINK_DELAY_MS} --accessLinkDelay=${ACCESS_LINK_DELAY_MS} --persistentTCPConnectionsCount=${PERSISTENT_TCP_CONNECTIONS_COUNT} --scheduleMultipleSubflowsPerConnection=${SCHEDULE_MULTIPLE_SUBFLOWS_PER_CONNECTION} --failuresCount=${FAILURES_COUNT} --networkTopology=${NETWORK_TOPOLOGY} --firstPacketCountDistribution=${DATASET_DIR}/${sim_traffic_workload}.txt --secondPacketCountDistribution=${SECOND_PACKET_COUNT_DISTRIBUTION}"
			./waf --run "${APP_NAME} ${SIMULATION_ARGS}" --cwd="${output_dir}" > ${output_dir}/sim_log_run_${SIMULATION_COUNTER}.txt 2>&1 &

			# DEBUG RUN
			#./waf --run "${APP_NAME}" --command-template="gdb --args %s ${SIMULATION_ARGS}" --cwd="${output_dir}"
		done
	done
done

while [[ `pgrep ${PROC_GREP} | wc -l` -gt 0 ]]
do
	sleep 1
done

# -----------------------------------------------------------------------------
# Simulation completed

echo "Done."
tput bel
