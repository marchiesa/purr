// ------------------------------------------------------------------------
// A custom application sending multiple TCP flows
// Created: October 2018
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// Based on the original BulkSendApplication class
// ------------------------------------------------------------------------

#include "ns3/log.h"
#include "ns3/address.h"
#include "ns3/inet-socket-address.h"
#include "ns3/packet-socket-address.h"
#include "ns3/tcp-socket-factory.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/nstime.h"
#include "ns3/data-rate.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include "ns3/random-variable-stream.h"
#include "ns3/double.h"
#include "ns3/trace-helper.h"

#include "sender-application.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE( "SenderApp" );

NS_OBJECT_ENSURE_REGISTERED( SenderApp );

// ------------------------------------------------------------------------
// AK

TypeId SenderApp::GetTypeId( void )
{
	static TypeId tid = TypeId( "ns3::SenderApp" )
		.SetParent<Application>()
		.SetGroupName( "Applications" )
		.AddConstructor<SenderApp>()
		.AddAttribute( "Protocol", "The type of protocol to use.",
				TypeIdValue( TcpSocketFactory::GetTypeId() ),
				MakeTypeIdAccessor( &SenderApp::m_tid ),
				MakeTypeIdChecker() )
		.AddTraceSource( "Tx", "A new packet is created and is sent",
				MakeTraceSourceAccessor( &SenderApp::m_tx_trace ),
				"ns3::Packet::TracedCallback" )
		;

	return tid;
}

// ------------------------------------------------------------------------

SenderApp::SenderApp()
  : m_id( 0 ),
  	m_node_id( 0 ),
  	m_socket( 0 ),
	m_peer(),
	m_running( false ),
	m_connected( false ),
	m_transmitting( false ),
	m_schedule_multiple_subflows( true ),
	m_flow_counter( 0 ),
	m_packet_payload_size( 0 ),
	m_flow_max_bytes( 0 ),
	m_flow_total_bytes_sent( 0 ),
	m_flow_packets_sent( 0 ),
	m_link_capacity_mbps( 0 ),
	m_link_utilization_factor( 0.9 ),
	m_avgPacketCount( 0.0 ),
	m_flowStartEvent(),
	m_distributionIndex( NULL ),
	m_firstCwnd( true ),
	m_firstSshThr( true ),
	m_firstRtt( true ),
	m_firstRto( true ),
	m_cWndStream( NULL ),
	m_ssThreshStream( NULL ),
	m_rttStream( NULL ),
	m_rtoStream( NULL ),
	m_cWndValue( 0 ),
	m_ssThreshValue( 0 ),
	m_previous_total_bytes_sent( 0 )
{
	NS_LOG_FUNCTION( this );
}

// ------------------------------------------------------------------------

SenderApp::~SenderApp()
{
	NS_LOG_FUNCTION( this );

	m_socket = 0;
}

// ------------------------------------------------------------------------

void SenderApp::Setup( uint32_t application_id, Address dst_address, uint32_t data_segment_size, std::vector<uint32_t> *packet_count_distribution, uint32_t link_capacity_mbps, double link_utilization_factor, uint32_t persistent_tcp_connections_count, double simulation_time_sec, double throughput_monitoring_interval_sec )
{
	NS_LOG_FUNCTION( this );

	m_socket = Socket::CreateSocket( GetNode(), m_tid );
	m_id = application_id;
	m_peer = dst_address;
	m_packet_payload_size = data_segment_size;
	m_link_capacity_mbps = link_capacity_mbps;
	m_link_utilization_factor = link_utilization_factor;
	m_persistent_tcp_connections_count = persistent_tcp_connections_count;
	m_simulation_time_sec = simulation_time_sec;
	m_throughput_monitoring_interval_sec = throughput_monitoring_interval_sec;
	m_avgPacketCount = 0.0;

	if ( packet_count_distribution != NULL )
	{
		m_packetCountDistribution = *packet_count_distribution;
		std::vector<uint32_t>::iterator it_value;

		for ( it_value = m_packetCountDistribution.begin(); it_value != m_packetCountDistribution.end(); it_value++ )
		{
			m_avgPacketCount += ( double )( *it_value );
		}
		
		// We use the following relations to compute the necessary parameters:
		//   1) MeanFlowArrivalRate = [ link capacity ] / ( [ mean flow size ] * [ persistent tcp connections count ] )
		//   2) MeanFlowInterArrivalTime = ( [ mean flow size ] * [ persistent tcp connections count ] ) / [ link capacity ]
		// Note that we increase the packet size by (32 + 20) B to take into account the overhead (including TCP/IP headers).

		m_avgPacketCount /= ( double )( m_packetCountDistribution.size() );
		m_meanFlowInterArrivalTime = ( m_avgPacketCount * ( double )( m_packet_payload_size + 32 + 20 ) * ( double )m_persistent_tcp_connections_count / 1000000.0 ) / ( m_link_utilization_factor * ( double )m_link_capacity_mbps / 8.0 );
	}

	m_distributionIndex = CreateObject<UniformRandomVariable>();
	m_distributionIndex -> SetAttribute( "Min", DoubleValue( 0 ) );
	m_distributionIndex -> SetAttribute( "Max", DoubleValue( m_packetCountDistribution.size() - 1 ) );

	m_flowInterArrivalTime = CreateObject<ExponentialRandomVariable>();
	m_flowInterArrivalTime -> SetAttribute( "Mean", DoubleValue( m_meanFlowInterArrivalTime ) );
	//m_flowInterArrivalTime -> SetAttribute( "Bound", DoubleValue( bound ) );
}

// ------------------------------------------------------------------------

void SenderApp::ScheduleMultipleSubflowsPerConnection( bool value )
{
	m_schedule_multiple_subflows = value;
}

// ------------------------------------------------------------------------

Ptr<Socket> SenderApp::GetSocket( void ) const
{
	NS_LOG_FUNCTION( this );

	return m_socket;
}

// ------------------------------------------------------------------------

std::map< std::string, uint64_t > *SenderApp::GetFlowSizeMap()
{
	return &m_flow_size_map;
}

// ------------------------------------------------------------------------

uint64_t SenderApp::GetTotalBytesSentForCurrentFlow( void )
{
	return m_flow_total_bytes_sent;
}

// ------------------------------------------------------------------------
// Called at time specified by Start

void SenderApp::StartApplication( void )
{
	NS_LOG_FUNCTION( this );

	Simulator::Cancel( m_flowStartEvent );
	m_running = true;
	m_flow_counter = 0;
	m_node_id = GetNode() -> GetId();
	
	//if ( m_socket == NULL )
	{
		// Create a TCP socket for the persistent connection

		//m_socket = Socket::CreateSocket( GetNode(), m_tid );

		if ( m_socket -> GetSocketType() != Socket::NS3_SOCK_STREAM && m_socket -> GetSocketType() != Socket::NS3_SOCK_SEQPACKET )
		{
			NS_FATAL_ERROR( "Using BulkSend with an incompatible socket type. "
							"BulkSend requires SOCK_STREAM or SOCK_SEQPACKET. "
							"In other words, use TCP instead of UDP." );
		}

		if ( Inet6SocketAddress::IsMatchingType( m_peer ) )
		{
			if ( m_socket -> Bind6() == -1 )
			{
				NS_FATAL_ERROR( "E: Failed to bind socket" );
			}
		}
		else if ( InetSocketAddress::IsMatchingType( m_peer ) || PacketSocketAddress::IsMatchingType( m_peer ) )
		{
			if ( m_socket -> Bind() == -1 )
			{
				NS_FATAL_ERROR( "E: Failed to bind socket" );
			}
		}

		m_socket -> Connect( m_peer );
		m_socket -> ShutdownRecv();
		m_socket -> SetConnectCallback( MakeCallback( &SenderApp::ConnectionSucceeded, this ), MakeCallback( &SenderApp::ConnectionFailed, this ) );
		m_socket -> SetSendCallback( MakeCallback( &SenderApp::CanSendMore, this ) );
	}

	// Configure tracing mechanisms
	
	std::string tracing_file_name_prefix = "tx_app_" + std::to_string( m_node_id ) + "_" + std::to_string( m_id );
	monitor_flow_throughput( tracing_file_name_prefix + "_throughput.csv" );
	Simulator::ScheduleNow( &ns3::SenderApp::trace_tcp_congestion_window, this, tracing_file_name_prefix + "_tcp_cwnd.csv" );
	Simulator::ScheduleNow( &ns3::SenderApp::trace_tcp_slow_start_threshold, this, tracing_file_name_prefix + "_tcp_ssth.csv" );
	Simulator::ScheduleNow( &ns3::SenderApp::trace_tcp_rtt, this, tracing_file_name_prefix + "_tcp_rtt.csv" );
	Simulator::ScheduleNow( &ns3::SenderApp::trace_tcp_rto, this, tracing_file_name_prefix + "_tcp_rto.csv" );
	
	// Schedule the first flow

	double timeout = m_flowInterArrivalTime -> GetValue();
	m_flowStartEvent = Simulator::Schedule( Seconds( timeout ), &SenderApp::ScheduleNextFlow, this );
}

// ------------------------------------------------------------------------
// Called at time specified by Stop

void SenderApp::StopApplication( void )
{
	NS_LOG_FUNCTION( this );

	Simulator::Cancel( m_flowStartEvent );

	if ( m_socket != NULL )
	{
		m_socket -> Close();
		m_running = false;
		m_connected = false;
		m_transmitting = false;
	}
	else
	{
		NS_LOG_WARN( "W: SenderApp found null socket to close in StopApplication" );
	}
}

// ------------------------------------------------------------------------

void SenderApp::ScheduleNextFlow()
{
	NS_LOG_FUNCTION( this );

	FlowStat current_flow;
	current_flow.flow_start_time = Simulator::Now();
	current_flow.flow_packet_count = ( uint64_t )m_packetCountDistribution.at( ( uint32_t )( m_distributionIndex -> GetInteger() ) );
	current_flow.completed = false;

	NS_LOG_LOGIC( "Next flow size (packets): " << current_flow.flow_packet_count );
	NS_LOG_LOGIC( "Next flow scheduled at time: " << current_flow.flow_start_time );

	if ( m_flow_queue.empty() && m_transmitting == false )
	{
		// Schedule the next transmission now

		m_current_flow = current_flow;
		m_flow_counter++;
		m_flow_max_bytes = m_current_flow.flow_packet_count * m_packet_payload_size;
		m_flow_total_bytes_sent = 0;
		m_flow_packets_sent = 0;
		
		FlowTag flow_tag;
		flow_tag.SetFlowId( m_node_id, m_id, m_flow_counter );
		m_flow_size_map[ flow_tag.GetFlowId() ] = m_flow_max_bytes;
		
		NS_LOG_UNCOND( "--> Starting flow " << flow_tag.GetFlowId() << " at time " << Simulator::Now().GetSeconds() << " s (flow queue empty)" );
		Simulator::ScheduleNow( &SenderApp::SendData, this );
	}
	else
	{
		m_flow_queue.push( current_flow );
	}

	// Compute the time till the next flow

	if ( m_schedule_multiple_subflows )
	{
		double timeout = m_flowInterArrivalTime -> GetValue();
		m_flowStartEvent = Simulator::Schedule( Seconds( timeout ), &SenderApp::ScheduleNextFlow, this );
	}
}

// ------------------------------------------------------------------------

void SenderApp::SendData( void )
{
	NS_LOG_FUNCTION( this );

	FlowTag flow_tag;
	flow_tag.SetFlowId( m_node_id, m_id, m_flow_counter );
	flow_tag.SetFlowSize( m_flow_max_bytes );

	std::string flow_id = flow_tag.GetFlowId();
	int bytes_sent = 0;

	uint8_t *data_buffer = new uint8_t[ m_packet_payload_size ];

	if ( data_buffer == NULL )
	{
		NS_LOG_UNCOND( "--> E: SenderApp::SendData(): Unable to create a packet data buffer" );

		return;
	}

	Ptr<Packet> packet = NULL;

	while ( m_flow_total_bytes_sent < m_flow_max_bytes )
	{
		// Objective of this iteration: send the next packet of the same flow

		m_transmitting = true;
		uint64_t bytes_to_send = m_packet_payload_size;
		
		// Make sure we don't send too many packets

		if ( m_flow_max_bytes > 0 )
		{
			bytes_to_send = std::min( bytes_to_send, m_flow_max_bytes - m_flow_total_bytes_sent );
		}

		// Store a copy of the flow ID in the data segment, so that intermediate nodes can identify the flow in a convenient way
		// Note that the byte tags are not easily accessible in IP functions on intermediate routers

		memset( data_buffer, 0, m_packet_payload_size );
		memcpy( data_buffer, flow_id.c_str(), flow_id.size() );
		
		// Create the packet filled with the content of 'data_buffer' (if possible) and add the flow tag as a byte tag

		if ( bytes_to_send >= flow_id.size() )
		{
			packet = Create<Packet>( data_buffer, bytes_to_send );
		}
		else
		{
			packet = Create<Packet>( bytes_to_send );
		}

		//flow_tag.SetTxStartTime( Simulator::Now().GetSeconds() );
		flow_tag.SetTxStartTime( m_current_flow.flow_start_time.GetSeconds() );
		packet -> AddByteTag( flow_tag );		// Accessed by the ReceiverApp to collect FCT information
		
		if ( packet -> FindFirstMatchingByteTag( flow_tag ) == false )
		{
			NS_LOG_UNCOND( "--> W: Unable to add the flow byte tag (flow ID: " << flow_id << ")" );
		}

		// Send the packet

		bytes_sent = m_socket -> Send( packet );

		if ( bytes_sent > 0 )
		{
			m_flow_packets_sent++;
			m_flow_total_bytes_sent += ( uint64_t )bytes_sent;
			m_tx_trace( packet );

			if ( InetSocketAddress::IsMatchingType( m_peer ) )
			{
				NS_LOG_INFO( "At time " << Simulator::Now().GetSeconds()
				<< "s the sender application sent "
				<<  bytes_sent << " out of " << packet -> GetSize() << " bytes to "
				<< InetSocketAddress::ConvertFrom( m_peer ).GetIpv4()
				<< " port " << InetSocketAddress::ConvertFrom( m_peer ).GetPort() );
			}
			else if ( Inet6SocketAddress::IsMatchingType( m_peer ) )
			{
				NS_LOG_INFO( "At time " << Simulator::Now().GetSeconds()
				<< "s the sender application sent "
				<<  bytes_sent << " out of " << packet -> GetSize() << " bytes to "
				<< Inet6SocketAddress::ConvertFrom( m_peer ).GetIpv6()
				<< " port " << Inet6SocketAddress::ConvertFrom( m_peer ).GetPort() );
			}
		}

		// We exit this loop when bytes_sent < bytes_to_send, as the send side
		// buffer is full. The "CanSendMore" callback will pop when some buffer space has freed up.

		if ( ( uint64_t )bytes_sent != bytes_to_send )
		{
			break;
		}
	}

	delete [] data_buffer;
	data_buffer = NULL;

	// Check if it is time to terminate the flow and schedule a new one
	
	if ( m_flow_total_bytes_sent == m_flow_max_bytes && m_connected && !m_current_flow.completed )
	{
		NS_LOG_UNCOND( "--> Flow " << flow_id << " completed at the sender at time " << Simulator::Now().GetSeconds() << " s" );
		m_current_flow.completed = true;
		
		if ( m_flow_queue.empty() )
		{
			m_transmitting = false;
		}
		else
		{
			m_current_flow = m_flow_queue.front();
			m_flow_counter++;
			m_flow_max_bytes = m_current_flow.flow_packet_count * m_packet_payload_size;
			m_flow_total_bytes_sent = 0;
			m_flow_packets_sent = 0;
			m_flow_queue.pop();
			
			FlowTag flow_tag;
			flow_tag.SetFlowId( m_node_id, m_id, m_flow_counter );
			m_flow_size_map[ flow_tag.GetFlowId() ] = m_flow_max_bytes;
			
			NS_LOG_UNCOND( "--> Starting flow " << flow_tag.GetFlowId() << " at time " << Simulator::Now().GetSeconds() << " s" );
			Simulator::ScheduleNow( &SenderApp::SendData, this );
		}
	}
}

// ------------------------------------------------------------------------
// Trace the throughput

void SenderApp::monitor_flow_throughput( std::string file_name )
{
	// Estimate the throughput until the end of simulation
	
	if ( Simulator::Now().GetSeconds() < m_simulation_time_sec )
	{
		Simulator::Schedule( Seconds( m_throughput_monitoring_interval_sec ), &ns3::SenderApp::monitor_flow_throughput, this, file_name );
	}

	double temp = ( double )( GetTotalBytesSentForCurrentFlow() - m_previous_total_bytes_sent ) * 8.0 / m_throughput_monitoring_interval_sec / 1000000.0;
	m_previous_total_bytes_sent = GetTotalBytesSentForCurrentFlow();

	// Write the result to a file
	
	FILE *output_fd = NULL;
	std::string data_line = std::to_string( Simulator::Now().GetSeconds() ) + " " + std::to_string( temp ) + "\r\n";
	
	if ( ( output_fd = fopen( file_name.c_str(), "a" ) ) != NULL )
	{
		fwrite( data_line.c_str(), 1, data_line.size(), output_fd );
		fclose( output_fd );
		output_fd = NULL;
	}
}

// ------------------------------------------------------------------------
// Trace the TCP congestion window and write the value to a file

void SenderApp::tcp_congestion_window_tracer( uint32_t oldval, uint32_t newval )
{
	if ( m_firstCwnd )
	{
		*m_cWndStream -> GetStream () << "0.0 " << oldval << std::endl;
		m_firstCwnd = false;
	}

	*m_cWndStream -> GetStream () << Simulator::Now ().GetSeconds () << " " << newval << std::endl;
	m_cWndValue = newval;

	if ( !m_firstSshThr )
	{
		*m_ssThreshStream -> GetStream () << Simulator::Now ().GetSeconds () << " " << m_ssThreshValue << std::endl;
	}
}

void SenderApp::trace_tcp_congestion_window( std::string file_name )
{
	AsciiTraceHelper ascii;
	m_cWndStream = ascii.CreateFileStream( file_name.c_str () );
	m_socket -> TraceConnectWithoutContext( "CongestionWindow", MakeCallback( &ns3::SenderApp::tcp_congestion_window_tracer, this ) );		//MakeBoundCallback
}

// ------------------------------------------------------------------------
// Trace the TCP slow start threshold and write the value to a file

void SenderApp::tcp_slow_start_threshold_tracer( uint32_t oldval, uint32_t newval )
{
	if ( m_firstSshThr )
	{
		*m_ssThreshStream -> GetStream () << "0.0 " << oldval << std::endl;
		m_firstSshThr = false;
	}

	*m_ssThreshStream -> GetStream () << Simulator::Now ().GetSeconds () << " " << newval << std::endl;
	m_ssThreshValue = newval;

	if ( !m_firstCwnd )
	{
		*m_cWndStream -> GetStream () << Simulator::Now ().GetSeconds () << " " << m_cWndValue << std::endl;
	}
}

void SenderApp::trace_tcp_slow_start_threshold( std::string file_name )
{
	AsciiTraceHelper ascii;
	m_ssThreshStream = ascii.CreateFileStream( file_name.c_str () );
	m_socket -> TraceConnectWithoutContext( "SlowStartThreshold", MakeCallback( &ns3::SenderApp::tcp_slow_start_threshold_tracer, this ) );
}

// ------------------------------------------------------------------------
// Trace the TCP RTT and write the value to a file

void SenderApp::tcp_rtt_tracer( Time oldval, Time newval )
{
	if ( m_firstRtt )
	{
		*m_rttStream -> GetStream () << "0.0 " << oldval.GetSeconds () << std::endl;
		m_firstRtt = false;
	}

	*m_rttStream -> GetStream () << Simulator::Now ().GetSeconds () << " " << newval.GetSeconds () << std::endl;
}

void SenderApp::trace_tcp_rtt( std::string file_name )
{
	AsciiTraceHelper ascii;
	m_rttStream = ascii.CreateFileStream( file_name.c_str () );
	m_socket -> TraceConnectWithoutContext( "RTT", MakeCallback( &ns3::SenderApp::tcp_rtt_tracer, this ) );
}

// ------------------------------------------------------------------------
// Trace the TCP RTO and write the value to a file

void SenderApp::tcp_rto_tracer( Time oldval, Time newval )
{
	if ( m_firstRto )
	{
		*m_rtoStream -> GetStream () << "0.0 " << oldval.GetSeconds () << std::endl;
		m_firstRto = false;
	}

	*m_rtoStream -> GetStream () << Simulator::Now ().GetSeconds () << " " << newval.GetSeconds () << std::endl;
}

void SenderApp::trace_tcp_rto( std::string file_name )
{
	AsciiTraceHelper ascii;
	m_rtoStream = ascii.CreateFileStream( file_name.c_str () );
	m_socket -> TraceConnectWithoutContext( "RTO", MakeCallback( &ns3::SenderApp::tcp_rto_tracer, this ) );
}

// ------------------------------------------------------------------------

void SenderApp::ConnectionSucceeded( Ptr<Socket> socket )
{
	NS_LOG_FUNCTION( this << socket );
	m_connected = true;
}

// ------------------------------------------------------------------------

void SenderApp::ConnectionFailed( Ptr<Socket> socket )
{
	NS_LOG_FUNCTION( this << socket );
}

// ------------------------------------------------------------------------

void SenderApp::CanSendMore( Ptr<Socket> socket, uint32_t value )
{
	NS_LOG_FUNCTION( this );

	// Only send new packets if:
	//   1) the TCP connection remains active; 
	//   2) the current flow has not finished yet.

	if ( m_connected && !m_current_flow.completed && m_current_flow.flow_packet_count != 0 )
	{
		SendData();
	}
}

// ------------------------------------------------------------------------

}
