// ------------------------------------------------------------------------
// A custom application receiving TCP flows
// Created: October 2018
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// Based on the original PacketSink class
// ------------------------------------------------------------------------

#ifndef __RECEIVER_APPLICATION_H
#define __RECEIVER_APPLICATION_H

#include <list>
#include <map>
#include <string>
#include <stdint.h>

#include "ns3/log.h"
#include "ns3/address.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/traced-callback.h"
#include "ns3/flow-tag.h"

// ------------------------------------------------------------------------
// AK

namespace ns3 {

class ReceiverApp : public Application
{
	public:
		static TypeId GetTypeId( void );
		ReceiverApp();
		virtual ~ReceiverApp();

		void Setup( Address address );
		void SetFCTCollectionStartTime( double seconds );
		
		uint32_t GetTotalRx() const;
		Ptr<Socket> GetListeningSocket( void );
		std::list< Ptr<Socket> > *GetAcceptedSockets( void );
		std::map< std::string, double > *GetFlowStartTimes( void );
		std::map< std::string, double > *GetFlowStopTimes( void );
		std::map< std::string, double > *GetFlowCompletionTimes( void );
		std::map< std::string, uint8_t > *GetFlowMinObservedTTLs( void );

	private:
		virtual void StartApplication( void );    
		virtual void StopApplication( void );     

		void HandleAccept( Ptr<Socket>, const Address& from );
		void HandleRead( Ptr<Socket> socket );
		void HandlePeerClose( Ptr<Socket> );
		void HandlePeerError( Ptr<Socket> );

		Ptr<Socket>  m_socket; 									// Listening socket
		std::list< Ptr<Socket> > m_accepted_sockets;			// Socket list for accepted connections
		Address m_local;										// Local server address to bind to
		uint32_t m_totalRx;										// Total bytes received
		TypeId m_tid;											// Protocol TypeId
		double m_fct_collection_start_time_sec;					// Time at which the FCT values start to be sampled
		std::map< std::string, double > m_flow_start_times;
		std::map< std::string, double > m_flow_stop_times;
		std::map< std::string, double > m_flow_completion_times;
		std::map< std::string, uint64_t > m_flow_received_bytes;
		std::map< std::string, uint8_t > m_flow_min_observed_ttl;
		
		TracedCallback<Ptr<const Packet>, const Address &> m_rx_trace;		// Traced Callback: received packets, source address
};

}

// ------------------------------------------------------------------------

#endif
