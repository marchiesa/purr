// ------------------------------------------------------------------------
// A custom application receiving TCP flows
// Created: October 2018
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// Based on the original PacketSink class
// ------------------------------------------------------------------------

#include "ns3/log.h"
#include "ns3/address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/tcp-socket-factory.h"
//#include "ns3/udp-socket-factory.h"
#include "ns3/flow-tag.h"

#include "receiver-application.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE( "ReceiverApp" );

NS_OBJECT_ENSURE_REGISTERED( ReceiverApp );

// ------------------------------------------------------------------------
// AK

TypeId ReceiverApp::GetTypeId( void )
{
	static TypeId tid = TypeId( "ns3::ReceiverApp" )
		.SetParent<Application>()
		.SetGroupName( "Applications" )
		.AddConstructor<ReceiverApp>()
		.AddAttribute( "Local",
				"The Address on which to Bind the rx socket.",
				AddressValue(),
				MakeAddressAccessor( &ReceiverApp::m_local ),
				MakeAddressChecker() )
		.AddAttribute( "Protocol",
				"The type id of the protocol to use for the rx socket.",
				TypeIdValue( TcpSocketFactory::GetTypeId() ),
				MakeTypeIdAccessor( &ReceiverApp::m_tid ),
				MakeTypeIdChecker() )
		.AddTraceSource( "Rx",
				"A packet has been received",
				MakeTraceSourceAccessor( &ReceiverApp::m_rx_trace ),
				"ns3::Packet::AddressTracedCallback" )
		;

	return tid;
}

// ------------------------------------------------------------------------

ReceiverApp::ReceiverApp()
{
	NS_LOG_FUNCTION( this );

	m_tid = TcpSocketFactory::GetTypeId();
	m_socket = 0;
	m_totalRx = 0;
	m_fct_collection_start_time_sec = 0.0;
}

// ------------------------------------------------------------------------

ReceiverApp::~ReceiverApp()
{
	NS_LOG_FUNCTION( this );
}

// ------------------------------------------------------------------------

void ReceiverApp::Setup( Address address )
{
	NS_LOG_FUNCTION( this );

	m_socket = 0;	
	m_local = address;
}

// ------------------------------------------------------------------------

void ReceiverApp::SetFCTCollectionStartTime( double seconds )
{
	NS_LOG_FUNCTION( this );

	m_fct_collection_start_time_sec = seconds;
}

// ------------------------------------------------------------------------

uint32_t ReceiverApp::GetTotalRx() const
{
	NS_LOG_FUNCTION( this );

	return m_totalRx;
}

// ------------------------------------------------------------------------

Ptr<Socket> ReceiverApp::GetListeningSocket( void )
{
	NS_LOG_FUNCTION( this );

	return m_socket;
}

// ------------------------------------------------------------------------

std::list< Ptr<Socket> > *ReceiverApp::GetAcceptedSockets( void )
{
	NS_LOG_FUNCTION( this );

	return &m_accepted_sockets;
}

// ------------------------------------------------------------------------

std::map< std::string, double > *ReceiverApp::GetFlowStartTimes( void )
{
	NS_LOG_FUNCTION( this );

	return &m_flow_start_times;
}

// ------------------------------------------------------------------------

std::map< std::string, double > *ReceiverApp::GetFlowStopTimes( void )
{
	NS_LOG_FUNCTION( this );

	return &m_flow_stop_times;
}

// ------------------------------------------------------------------------

std::map< std::string, double > *ReceiverApp::GetFlowCompletionTimes( void )
{
	NS_LOG_FUNCTION( this );

	return &m_flow_completion_times;
}

// ------------------------------------------------------------------------

std::map< std::string, uint8_t > *ReceiverApp::GetFlowMinObservedTTLs( void )
{
	NS_LOG_FUNCTION( this );

	return &m_flow_min_observed_ttl;
}

// ------------------------------------------------------------------------
// Called at time specified by Start

void ReceiverApp::StartApplication ()    
{
	NS_LOG_FUNCTION( this );

	if ( m_socket == NULL ) 
	{
		m_socket = Socket::CreateSocket( GetNode(), TcpSocketFactory::GetTypeId() );
		
		if ( m_socket -> Bind( m_local ) == -1 )
		{
			NS_FATAL_ERROR( "Failed to bind socket" );
		}
		
		m_socket -> Listen();
		m_socket -> ShutdownSend();

		/*if ( addressUtils::IsMulticast( m_local ) )
		{
			Ptr<UdpSocket> udp_socket = DynamicCast<UdpSocket>( m_socket );

			if ( udp_socket != NULL )
			{
				// Equivalent to setsockopt (MCAST_JOIN_GROUP)

				udp_socket -> MulticastJoinGroup( 0, m_local );
			}
			else
			{
				NS_FATAL_ERROR( "E: Joining multicast on a non-UDP socket" );
			}
		}*/
	}

	m_socket -> SetRecvCallback( MakeCallback( &ReceiverApp::HandleRead, this ) );
	m_socket -> SetAcceptCallback( MakeNullCallback< bool, Ptr<Socket>, const Address & >(), MakeCallback( &ReceiverApp::HandleAccept, this ) );
	m_socket -> SetCloseCallbacks( MakeCallback( &ReceiverApp::HandlePeerClose, this ), MakeCallback( &ReceiverApp::HandlePeerError, this ) );
}

// ------------------------------------------------------------------------
// Called at time specified by Stop

void ReceiverApp::StopApplication()
{
	NS_LOG_FUNCTION( this );

	// Close all accepted sockets

	while ( !m_accepted_sockets.empty() )
	{
		Ptr<Socket> accepted_socket = m_accepted_sockets.front();
		m_accepted_sockets.pop_front();
		accepted_socket -> Close();
	}

	// Close the listening socket

	if ( m_socket != NULL ) 
	{
		m_socket -> Close();
		m_socket -> SetRecvCallback( MakeNullCallback< void, Ptr<Socket> >() );
	}
}

// ------------------------------------------------------------------------

void ReceiverApp::HandleAccept( Ptr<Socket> s, const Address& from )
{
	NS_LOG_FUNCTION( this << s << from );

	s -> SetRecvCallback( MakeCallback( &ReceiverApp::HandleRead, this ) );
	s -> SetCloseCallbacks( MakeCallback( &ReceiverApp::HandlePeerClose, this ), MakeCallback( &ReceiverApp::HandlePeerError, this ) );
	
	m_accepted_sockets.push_back( s );
}

// ------------------------------------------------------------------------

void ReceiverApp::HandleRead( Ptr<Socket> socket )
{
	NS_LOG_FUNCTION( this << socket );
	
	Ptr<Packet> packet;
	Ipv4Header ip_header;
	Address from;
	uint8_t ip_ttl;
	bool found_tag;
	std::string flow_id;
	double current_sim_time_sec = Simulator::Now().GetSeconds();

	while ( ( packet = socket -> RecvFrom( from ) ) != NULL )
	{
		if ( packet -> GetSize() == 0 )
		{
			// EOF
			break;
		}

		m_totalRx += packet -> GetSize();

		// Get the flow tag and compute the Flow Completion Time (FCT) if needed

		FlowTag flow_tag;
		found_tag = packet -> FindFirstMatchingByteTag( flow_tag );

		if ( found_tag )
		{
			flow_id = flow_tag.GetFlowId();
			
			if ( m_flow_start_times.find( flow_id ) == m_flow_start_times.end() )
			{
				// Only the first received packet of the flow will be considered here

				m_flow_start_times[ flow_id ] = flow_tag.GetTxStartTime();
				m_flow_received_bytes[ flow_id ] = 0;
				NS_LOG_UNCOND( "--> Received the first packet (" << packet -> GetSize() << " B) of flow " << flow_id << " at time " << current_sim_time_sec << " s (scheduled at " << flow_tag.GetTxStartTime() << " s)" );
			}

			m_flow_received_bytes[ flow_id ] += ( uint64_t )( packet -> GetSize() );

			if ( m_flow_received_bytes[ flow_id ] >= flow_tag.GetFlowSize() )
			{
				m_flow_stop_times[ flow_id ] = current_sim_time_sec;

				// FCT is defined as the time difference between the packet's departure at the source node and its arrival at the destination

				if ( current_sim_time_sec >= m_fct_collection_start_time_sec )
				{
					m_flow_completion_times[ flow_id ] = m_flow_stop_times[ flow_id ] - m_flow_start_times[ flow_id ];
				}

				NS_LOG_UNCOND( "--> Received the last packet of flow " << flow_id << " at time " << current_sim_time_sec << " s (scheduled at " << flow_tag.GetTxStartTime() << " s)" );
			}
		}

		// Record the minimum observed TTL for the related flow

		packet -> PeekHeader( ip_header );
		ip_ttl = ip_header.GetTtl();
		
		if ( m_flow_min_observed_ttl.find( flow_id ) == m_flow_min_observed_ttl.end() )
		{
			m_flow_min_observed_ttl[ flow_id ] = ip_ttl;
		}
		else if ( m_flow_min_observed_ttl[ flow_id ] > ip_ttl )
		{
			m_flow_min_observed_ttl[ flow_id ] = ip_ttl;
		}

		if ( InetSocketAddress::IsMatchingType( from ) )
		{
			NS_LOG_INFO( "At time " << current_sim_time_sec
			<< "s packet sink received "
			<<  packet -> GetSize() << " bytes from "
			<< InetSocketAddress::ConvertFrom( from ).GetIpv4()
			<< " port " << InetSocketAddress::ConvertFrom( from ).GetPort()
			<< " total Rx " << m_totalRx << " bytes" );
		}
		else if ( Inet6SocketAddress::IsMatchingType( from ) )
		{
			NS_LOG_INFO( "At time " << current_sim_time_sec
			<< "s packet sink received "
			<<  packet -> GetSize() << " bytes from "
			<< Inet6SocketAddress::ConvertFrom( from ).GetIpv6()
			<< " port " << Inet6SocketAddress::ConvertFrom( from ).GetPort()
			<< " total Rx " << m_totalRx << " bytes" );
		}

		m_rx_trace( packet, from );
	}
 }

// ------------------------------------------------------------------------

void ReceiverApp::HandlePeerClose( Ptr<Socket> socket )
{
	NS_LOG_FUNCTION( this << socket );
}

// ------------------------------------------------------------------------

void ReceiverApp::HandlePeerError( Ptr<Socket> socket )
{
	NS_LOG_FUNCTION( this << socket );
}

// ------------------------------------------------------------------------

}
