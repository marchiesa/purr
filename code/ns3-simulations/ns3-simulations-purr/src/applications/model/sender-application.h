// ------------------------------------------------------------------------
// A custom application sending multiple TCP flows
// Created: October 2018
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// Based on the original BulkSendApplication class
// ------------------------------------------------------------------------

#ifndef __SENDER_APPLICATION_H
#define __SENDER_APPLICATION_H

#include <list>
#include <vector>
#include <queue>
#include <stdint.h>

#include "ns3/address.h"
#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/data-rate.h"
#include "ns3/traced-callback.h"
#include "ns3/flow-tag.h"
#include "ns3/output-stream-wrapper.h"

namespace ns3 {

// ------------------------------------------------------------------------
// AK

struct FlowStat
{
	Time flow_start_time;
	uint64_t flow_packet_count;
	bool completed;

	FlowStat() : 
		flow_start_time( 0 ),
		flow_packet_count( 0 ),
		completed( false )
	{
		// Empty
	}
};

// ------------------------------------------------------------------------

class SenderApp : public Application
{
	public:
		static TypeId GetTypeId( void );
		SenderApp();
		virtual ~SenderApp();
		void Setup( uint32_t application_id, Address dst_address, uint32_t data_segment_size, std::vector<uint32_t> *packet_count_distribution, uint32_t link_capacity_mbps, double link_utilization_factor, uint32_t persistent_tcp_connections_count, double simulation_time_sec, double throughput_monitoring_interval_sec );

		void ScheduleMultipleSubflowsPerConnection( bool value );

		Ptr<Socket> GetSocket( void ) const;
		std::map< std::string, uint64_t > *GetFlowSizeMap();
		uint64_t GetTotalBytesSentForCurrentFlow( void );

		// Additional functions needed for debugging
		/*void monitor_flow_throughput( std::string file_name );
		void tcp_congestion_window_tracer( uint32_t oldval, uint32_t newval );
		void trace_tcp_congestion_window( std::string file_name );
		void tcp_slow_start_threshold_tracer( uint32_t oldval, uint32_t newval );
		void trace_tcp_slow_start_threshold( std::string file_name );
		void tcp_rtt_tracer( Time oldval, Time newval );
		void trace_tcp_rtt( std::string file_name );
		void tcp_rto_tracer( Time oldval, Time newval );
		void trace_tcp_rto( std::string file_name );*/

	private:
		virtual void StartApplication( void );
		virtual void StopApplication( void );
		void ScheduleNextFlow( void );
		void SendData( void );									// Send data until the L4 transmission buffer is full

		// Additional functions needed for debugging
		void monitor_flow_throughput( std::string file_name );
		void tcp_congestion_window_tracer( uint32_t oldval, uint32_t newval );
		void trace_tcp_congestion_window( std::string file_name );
		void tcp_slow_start_threshold_tracer( uint32_t oldval, uint32_t newval );
		void trace_tcp_slow_start_threshold( std::string file_name );
		void tcp_rtt_tracer( Time oldval, Time newval );
		void trace_tcp_rtt( std::string file_name );
		void tcp_rto_tracer( Time oldval, Time newval );
		void trace_tcp_rto( std::string file_name );
		
		// Callback methods
		void ConnectionSucceeded( Ptr<Socket> socket );
		void ConnectionFailed( Ptr<Socket> socket );
		void CanSendMore( Ptr<Socket> socket, uint32_t value );	// Send more data as soon as some has been transmitted (for socket's SetSendCallback)
		
		TypeId m_tid;											// Type of the socket used
		uint32_t m_id;											// Application ID (set via the Setup() call)
		uint32_t m_node_id;
		Ptr<Socket> m_socket;
		Address m_peer;
		bool m_running;
		bool m_connected;
		bool m_transmitting;
		bool m_schedule_multiple_subflows;
		uint32_t m_flow_counter;
		uint32_t m_packet_payload_size;
		uint64_t m_flow_max_bytes;
		uint64_t m_flow_total_bytes_sent;
		uint64_t m_flow_packets_sent;
		uint32_t m_link_capacity_mbps;
		uint32_t m_persistent_tcp_connections_count;
		double m_link_utilization_factor;
		std::vector<uint32_t> m_packetCountDistribution;
		double m_avgPacketCount;
		double m_meanFlowInterArrivalTime;
		EventId m_flowStartEvent;
		FlowStat m_current_flow;
		std::queue< FlowStat > m_flow_queue;
		std::map< std::string, uint64_t > m_flow_size_map;
		Ptr<UniformRandomVariable> m_distributionIndex;
		Ptr<ExponentialRandomVariable> m_flowInterArrivalTime;

		TracedCallback<Ptr<const Packet> > m_tx_trace;			// Traced Callback: transmitted packets

		// Additional elements need for debugging
		bool m_firstCwnd;
		bool m_firstSshThr;
		bool m_firstRtt;
		bool m_firstRto;
		Ptr<OutputStreamWrapper> m_cWndStream;
		Ptr<OutputStreamWrapper> m_ssThreshStream;
		Ptr<OutputStreamWrapper> m_rttStream;
		Ptr<OutputStreamWrapper> m_rtoStream;
		uint32_t m_cWndValue;
		uint32_t m_ssThreshValue;
		uint64_t m_previous_total_bytes_sent;
		double m_simulation_time_sec;
		double m_throughput_monitoring_interval_sec;
};

// ------------------------------------------------------------------------

}

#endif
