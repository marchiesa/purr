/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2006,2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#include "ns3/tag.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include <iostream>
#include "ns3/marco-tag.h"

using namespace ns3;

/*
 * LSB
 * bit 1-2: failed link 1 identifier
 * bit 3-4: failed link 2 identifier
 * bit 5: whether there is a failed link 1
 * bit 6: whether there is a failed link 2
 * bit 7-8: incoming port identifier
 * MSB
 * */

TypeId 
MarcoTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::MyTag")
    .SetParent<Tag> ()
    .AddConstructor<MarcoTag> ()
    .AddAttribute ("SimpleValue",
                   "A simple value",
                   UintegerValue (0),
                   MakeUintegerAccessor (&MarcoTag::GetSimpleValue),
                   MakeUintegerChecker<uint8_t> ())
  ;
  return tid;
}
TypeId 
MarcoTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}
uint32_t 
MarcoTag::GetSerializedSize (void) const
{
  return 1;
}
void 
MarcoTag::Serialize (TagBuffer i) const
{
  i.WriteU8 (m_simpleValue);
}
void 
MarcoTag::Deserialize (TagBuffer i)
{
  m_simpleValue = i.ReadU8 ();
}
void 
MarcoTag::Print (std::ostream &os) const
{
  os << "v=" << (uint32_t)m_simpleValue;
}
void 
MarcoTag::SetSimpleValue (uint8_t value)
{
  m_simpleValue = value;
}
uint8_t 
MarcoTag::GetSimpleValue (void) const
{

  return m_simpleValue;
}
void
MarcoTag::SetFailed (bool value)
{
   m_simpleValue &= 0xEF; // Clear out the 2 LSB part. retain the rest
   if(value)
	   m_simpleValue |= 0x10; // Set the last bit to 1
   else
	   m_simpleValue &= 0xEF; // Set the last bit to 0
   m_simpleValue |= (value);
}
void
MarcoTag::SetIncomingPort (uint8_t value)
{
  //std::cout << " 1. tag simplevalue: " << +m_simpleValue << " value:" << +value <<  std::endl;
   m_simpleValue &= 0xF0; // Clear out the 2-4 LSB part. retain the rest
  //std::cout << " 2. tag simplevalue: " << +m_simpleValue << " value:" << +value <<  std::endl;
   m_simpleValue |= (value);
  //std::cout << " 3. tag simplevalue: " << +m_simpleValue << " value:" << +value <<  std::endl;
}
uint8_t 
MarcoTag::GetIncomingPort (void) const
{
  // Extract only first 6 bits of TOS byte, i.e 0xFC
  //std::cout << " retrieve tag simplevalue: " << +m_simpleValue <<  std::endl;
  return (m_simpleValue & 0x0F);
}
bool
MarcoTag::isFailed (void) const
{
  // Extract only first 6 bits of TOS byte, i.e 0xFC
  return (m_simpleValue & 0x10 ) == 1;
}


