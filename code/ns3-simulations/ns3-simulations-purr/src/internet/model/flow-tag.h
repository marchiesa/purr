// ------------------------------------------------------------------------
// A custom packet tag used to associate packets with the corresponding flows
// Created: October 2018
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// ------------------------------------------------------------------------

#ifndef __FLOW_TAG_H
#define __FLOW_TAG_H

#include "ns3/ipv4-address.h"
#include "ns3/tag.h"

#include <stdint.h>
#include <string>
#include <iostream>

// ------------------------------------------------------------------------

namespace ns3
{
	class FlowTag : public Tag
	{
		public:
			FlowTag();
			
			static TypeId GetTypeId( void );
			virtual TypeId GetInstanceTypeId( void ) const;
			virtual uint32_t GetSerializedSize() const;
			virtual void Serialize( TagBuffer i ) const;
			virtual void Deserialize( TagBuffer i );
			virtual void Print( std::ostream &os ) const;

			void SetFlowId( uint32_t src_node_id, uint32_t socket_ptr, uint32_t sender_flow_index );
			std::string GetFlowId() const;

			void SetFlowSize( uint64_t bytes );
			uint64_t GetFlowSize();

			void SetTxStartTime( double seconds );
			double GetTxStartTime();

		private:
			std::string ipv4_to_string( Ipv4Address address );

			std::string m_flow_id;
			uint64_t m_flow_size_bytes;
			double m_tx_start_time_sec;
	};

}

// ------------------------------------------------------------------------

#endif /* __FLOW_TAG_H */
