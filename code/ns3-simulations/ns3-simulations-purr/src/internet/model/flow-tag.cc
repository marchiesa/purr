// ------------------------------------------------------------------------
// A custom packet tag used to associate packets with the corresponding flows
// Created: October 2018
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// ------------------------------------------------------------------------

#include "ns3/ipv4-address.h"
#include "ns3/log.h"
#include "ns3/tag.h"

#include <stdint.h>
#include <string>
#include <iostream>

#include "flow-tag.h"

// ------------------------------------------------------------------------

NS_LOG_COMPONENT_DEFINE( "FlowTag" );

namespace ns3
{

NS_OBJECT_ENSURE_REGISTERED( FlowTag );

// ------------------------------------------------------------------------

FlowTag::FlowTag() : Tag()
{
	m_flow_size_bytes = 0;
	m_tx_start_time_sec = 0.0;
}

// ------------------------------------------------------------------------

TypeId FlowTag::GetTypeId( void )
{
	static TypeId tid = TypeId( "ns3::FlowTag" )
		.SetParent<Tag>()
		.AddConstructor<FlowTag>();

	return tid;
}

// ------------------------------------------------------------------------

TypeId FlowTag::GetInstanceTypeId( void ) const
{
	return GetTypeId();
}

// ------------------------------------------------------------------------

uint32_t FlowTag::GetSerializedSize( void ) const
{
	// Each string needs an additional \0 character at the end (thus, the required size is increased by one)

	return ( ( m_flow_id.size() + 1 ) + sizeof( m_flow_size_bytes ) + sizeof( m_tx_start_time_sec ) );
}

// ------------------------------------------------------------------------

void FlowTag::Serialize( TagBuffer i ) const
{
	uint32_t flow_id_size = m_flow_id.size();
	uint32_t p;

	for ( p = 0; p < flow_id_size; p++ )
	{
		i.WriteU8( ( uint8_t )m_flow_id[ p ] );
	}

	i.WriteU8( ( uint8_t )'\0' );
	i.WriteU64( m_flow_size_bytes );
	i.WriteDouble( m_tx_start_time_sec );
}

// ------------------------------------------------------------------------

void FlowTag::Deserialize( TagBuffer i )
{
	m_flow_id.clear();
	uint8_t c;

	while ( ( c = i.ReadU8() ) != ( uint8_t )'\0' )
	{
		m_flow_id.push_back( ( char )c );
	}

	m_flow_size_bytes = i.ReadU64();
	m_tx_start_time_sec = i.ReadDouble();

	NS_LOG_FUNCTION( "Loaded a flow tag: " << m_flow_id );
}

// ------------------------------------------------------------------------

void FlowTag::Print( std::ostream &os ) const
{
	os << "FlowTag = [ID: " << m_flow_id << ", size: " << m_flow_size_bytes << " B, started at: " << m_tx_start_time_sec << " s]";
}

// ------------------------------------------------------------------------

void FlowTag::SetFlowId( uint32_t src_node_id, uint32_t socket_ptr, uint32_t sender_flow_index )
{
	m_flow_id = std::to_string( src_node_id ) + std::string( "-" ) + std::to_string( socket_ptr ) + std::string( "-" ) + std::to_string( sender_flow_index );
}

// ------------------------------------------------------------------------

std::string FlowTag::GetFlowId() const
{
	return m_flow_id;
}

// ------------------------------------------------------------------------

void FlowTag::SetFlowSize( uint64_t bytes )
{
	m_flow_size_bytes = bytes;
}

// ------------------------------------------------------------------------

uint64_t FlowTag::GetFlowSize()
{
	return m_flow_size_bytes;
}

// ------------------------------------------------------------------------

void FlowTag::SetTxStartTime( double seconds )
{
	m_tx_start_time_sec = seconds;
}

// ------------------------------------------------------------------------

double FlowTag::GetTxStartTime()
{
	return m_tx_start_time_sec;
}

// ------------------------------------------------------------------------

std::string FlowTag::ipv4_to_string( Ipv4Address address )
{
	std::ostringstream oss;
	address.Print( oss );

	return oss.str();
}

// ------------------------------------------------------------------------

}