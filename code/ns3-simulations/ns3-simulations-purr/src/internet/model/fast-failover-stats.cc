/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
// ------------------------------------------------------------------------
// Implementation of additional mechanisms to collect performance information
// related to the experimental fast-failover mechanisms
// Created: November 2018
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// ------------------------------------------------------------------------

#include <list>
#include <map>
#include <math.h>

#include "ns3/double.h"
#include "ns3/log.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/packet.h"
#include "ns3/tcp-header.h"
#include "ns3/net-device.h"
#include "ns3/node.h"

#include "fast-failover-stats.h"

NS_LOG_COMPONENT_DEFINE( "FastFailoverStats" );

namespace ns3 {

	NS_OBJECT_ENSURE_REGISTERED( FastFailoverStats );

	// ------------------------------------------------------------------------

	TypeId FastFailoverStats::GetTypeId( void )
	{
		static TypeId tid = TypeId( "ns3::FastFailoverStats" )
			.SetParent<Object>()
			.AddConstructor<FastFailoverStats>();

		return tid;
	}

	// ------------------------------------------------------------------------

	FastFailoverStats::FastFailoverStats()
	{
		NS_LOG_FUNCTION( this );

		m_node = NULL;
		m_monitored_output_dev = NULL;
		m_ipv4_l3_protocol = NULL;
	}

	// ------------------------------------------------------------------------

	FastFailoverStats::~FastFailoverStats()
	{
		NS_LOG_FUNCTION( this );
	}

	// ------------------------------------------------------------------------

	void FastFailoverStats::SetNode( Ptr<Node> node )
	{
		NS_LOG_FUNCTION( this );

		m_node = node;
	}

	// ------------------------------------------------------------------------

	Ptr<Node> FastFailoverStats::GetNode()
	{
		NS_LOG_FUNCTION( this );

		return m_node;
	}
	
	// ------------------------------------------------------------------------

	void FastFailoverStats::SetOutputDevice( Ptr<NetDevice> output_dev )
	{
		NS_LOG_FUNCTION( this );

		m_monitored_output_dev = output_dev;
	}

	// ------------------------------------------------------------------------

	Ptr<NetDevice> FastFailoverStats::GetOutputDevice()
	{
		NS_LOG_FUNCTION( this );

		return m_monitored_output_dev;
	}

	// ------------------------------------------------------------------------

	void FastFailoverStats::SetIpv4L3Protocol( Ptr<Ipv4L3Protocol> protocol )
	{
		NS_LOG_FUNCTION( this );

		m_ipv4_l3_protocol = protocol;
	}

	// ------------------------------------------------------------------------

	Ptr<Ipv4L3Protocol> FastFailoverStats::GetIpv4L3Protocol()
	{
		NS_LOG_FUNCTION( this );

		return m_ipv4_l3_protocol;
	}

	// ------------------------------------------------------------------------

	void FastFailoverStats::RegisterForwardedFlow( Ptr<Packet> packet, const Ipv4Header &header, Ptr<NetDevice> output_dev )
	{
		if ( packet == NULL || output_dev == NULL )
		{
			return;
		}

		// Calculate the size of packet headers (IP, TCP)

		uint32_t packet_size = packet -> GetSize();
		/*uint32_t ip_header_size = header.GetSerializedSize();
		
		Header *temp_header = new TcpHeader();
		uint32_t tcp_header_size = temp_header -> GetSerializedSize();
		delete temp_header;
		temp_header = NULL;*/
		
		//uint16_t payload_size = packet_size - ( ip_header_size + tcp_header_size );
		//uint32_t tcp_adu_size = mtu_bytes - 20 - (ip_header + tcp_header);

		// Extract the flow ID from the data segment
		// Note that the byte tags are not easily accessible in IP functions on intermediate routers

		if ( packet_size < 1000 )
		{
			// Ignore control messages (we assume that data flows in our experiments carry packets of size at least 1000 B)

			return;
		}

		uint8_t *data_buffer = new uint8_t[ packet_size ];

		if ( data_buffer == NULL )
		{
			NS_LOG_UNCOND( "--> E: FastFailoverStats::RegisterForwardedFlow(): Unable to create a packet data buffer" );

			return;
		}

		memset( data_buffer, 0, packet_size );
		uint32_t bytes_read = packet -> CopyData( data_buffer, packet_size );

		if ( bytes_read < packet_size )
		{
			NS_LOG_UNCOND( "--> E: FastFailoverStats::RegisterForwardedFlow(): Unable to copy the packet into the data buffer" );

			delete [] data_buffer;
			data_buffer = NULL;
			
			return;
		}

		std::string flow_id;
		uint32_t i = 32;//ip_header_size + tcp_header_size;		// TODO

		while ( i < packet_size && *( data_buffer + i ) != ( uint8_t )'\0' )
		{
			flow_id.push_back( *( data_buffer + i ) );
			i++;
		}

		delete [] data_buffer;
		data_buffer = NULL;
		
		//NS_LOG_UNCOND( "--> DEBUG: Read " << bytes_read << " bytes from the packet. Packet size = " << packet_size << " bytes, IP header size = " << ip_header_size << " bytes, TCP header size = " << tcp_header_size << " bytes. Decoded flow ID = " << flow_id );

		if ( m_monitored_output_dev != NULL )
		{
			// Register all flows forwarded specifically via 'm_monitored_output_dev' network device, regardless of its status

			if ( output_dev == m_monitored_output_dev && m_forwarded_flows.find( flow_id ) == m_forwarded_flows.end() )
			{
				// Forwarding the first packet of the flow
				// Register the flow in the internal map, together with the identifier of the output port

				m_forwarded_flows[ flow_id ] = output_dev -> GetIfIndex();
			}
		}
		else
		{
			// If the preferred output interface is unavailable, register the flow as affected by failure (typically used scenario)

			Ptr<Ipv4L3Protocol> ipv4_l3_protocol = output_dev -> GetNode() -> GetObject<Ipv4L3Protocol>();

			if ( ipv4_l3_protocol == NULL )
			{
				return;
			}

			int32_t ip_interface = ipv4_l3_protocol -> GetInterfaceForDevice( output_dev );

			if ( ip_interface < 0 )
			{
				NS_LOG_UNCOND( "--> IPv4 interface not found for device " << output_dev -> GetIfIndex() << " on node " << output_dev -> GetNode() -> GetId() );

				return;
			}

			if ( ipv4_l3_protocol -> IsUp( ( uint32_t )ip_interface ) == false && m_forwarded_flows.find( flow_id ) == m_forwarded_flows.end() )
			{
				// Forwarding the first packet of the flow
				// Register the flow in the internal map, together with the identifier of the output port

				m_forwarded_flows[ flow_id ] = output_dev -> GetIfIndex();
			}
		}
	}

	// ------------------------------------------------------------------------

	std::map< std::string, uint32_t > *FastFailoverStats::GetForwardedFlows()
	{
		return &m_forwarded_flows;
	}

	// ------------------------------------------------------------------------
}
