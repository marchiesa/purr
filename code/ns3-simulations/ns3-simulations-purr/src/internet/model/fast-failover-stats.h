/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
// ------------------------------------------------------------------------
// Implementation of additional mechanisms to collect performance information
// related to the experimental fast-failover mechanisms
// Created: November 2018
//
// Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
// ------------------------------------------------------------------------

#ifndef __FAST_FAILOVER_STATS_H
#define __FAST_FAILOVER_STATS_H

#include <list>
#include <map>
#include <string>

#include "ns3/ipv4-l3-protocol.h"
#include "ns3/packet.h"
#include "ns3/net-device.h"
#include "ns3/node.h"

namespace ns3 {

	class FastFailoverStats : public Object
	{
		public:
			static TypeId GetTypeId ( void );

			FastFailoverStats();
			virtual ~FastFailoverStats();

			void SetNode( Ptr<Node> node );
			Ptr<Node> GetNode();

			void SetOutputDevice( Ptr<NetDevice> output_dev );
			Ptr<NetDevice> GetOutputDevice();

			void SetIpv4L3Protocol( Ptr<Ipv4L3Protocol> protocol );
			Ptr<Ipv4L3Protocol> GetIpv4L3Protocol();

			void RegisterForwardedFlow( Ptr<Packet> packet, const Ipv4Header &header, Ptr<NetDevice> output_dev );
			std::map< std::string, uint32_t > *GetForwardedFlows();

		protected:
			//

		private:
			Ptr<Node> m_node;
			Ptr<NetDevice> m_monitored_output_dev;
			Ptr<Ipv4L3Protocol> m_ipv4_l3_protocol;
			std::map< std::string, uint32_t > m_forwarded_flows;		// < flow_id, output_dev_id >
			
	};

}

#endif /* __FAST_FAILOVER_STATS_H */
