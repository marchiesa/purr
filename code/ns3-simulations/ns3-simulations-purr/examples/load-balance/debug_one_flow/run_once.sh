#!/bin/bash
# ------------------------------------------------------------------------
# The simulation script created for an evaluation of the selected
# fast-failover mechanisms in a DCTCP-enabled datacenter network
#
# Author: Andrzej Kamisiński (andrzejk@agh.edu.pl)
#
# To run the script:
#   cd /home/(...)/dctcp_fast_failover
#   ./scratch/sigmetrics_dctcp/run_once.sh
#
# Remember to adjust the constants below to match your environment.
# Use it for test purposes only (not suitable for multiple iterations).

# -----------------------------------------------------------------------------
# CONSTANTS

APP_NAME="debug_one_flow"
DATASET_DIR="/home/mchiesa/hermes/examples/load-balance/${APP_NAME}/input_data"
WORK_DIR="examples/load-balance/${APP_NAME}/simout"
WAF_LOG_FILE="${WORK_DIR}/simulation.log"

SIMULATION_RUN=2
DATA_SEGMENT_SIZE_BYTES=1348		# Headers/overhead: 12 + 20 (TCP) + 20 (IPv4) bytes
CORE_LINK_CAPACITY_MBPS=1000
ACCESS_LINK_CAPACITY_MBPS=10000
ACCESS_LINK_UTILIZATION_FACTOR=0.3		# Modify to change the network load
CORE_LINK_DELAY_MS=0.001
ACCESS_LINK_DELAY_MS=0.01
PERSISTENT_TCP_CONNECTIONS_COUNT=1
SCHEDULE_MULTIPLE_SUBFLOWS_PER_CONNECTION=0
FAILURES_COUNT=0
NETWORK_TOPOLOGY="${DATASET_DIR}/topo_general.txt"
#NETWORK_TOPOLOGY="${DATASET_DIR}/topo_recirculation.txt"
FIRST_PACKET_COUNT_DISTRIBUTION="${DATASET_DIR}/cdf_one_large_flow.txt"
#FIRST_PACKET_COUNT_DISTRIBUTION="${DATASET_DIR}/cdf_dctcp_websearch.txt"
SECOND_PACKET_COUNT_DISTRIBUTION="${DATASET_DIR}/cdf_vl2_datamining.txt"

# -----------------------------------------------------------------------------
# BUILD AND RUN THE SIMULATION

#export NS_LOG="TcpDctcp=level_function|prefix_func"
export NS_LOG="RedQueueDisc=level_function|prefix_func:RedQueueDisc=level_debug|prefix_func"

SIMULATION_ARGS="--simRun=${SIMULATION_RUN} --dataSegmentSize=${DATA_SEGMENT_SIZE_BYTES} --coreLinkCapacity=${CORE_LINK_CAPACITY_MBPS} --accessLinkCapacity=${ACCESS_LINK_CAPACITY_MBPS} --accessLinkUtilizationFactor=${ACCESS_LINK_UTILIZATION_FACTOR} --coreLinkDelay=${CORE_LINK_DELAY_MS} --accessLinkDelay=${ACCESS_LINK_DELAY_MS} --persistentTCPConnectionsCount=${PERSISTENT_TCP_CONNECTIONS_COUNT} --scheduleMultipleSubflowsPerConnection=${SCHEDULE_MULTIPLE_SUBFLOWS_PER_CONNECTION} --failuresCount=${FAILURES_COUNT} --networkTopology=${NETWORK_TOPOLOGY} --firstPacketCountDistribution=${FIRST_PACKET_COUNT_DISTRIBUTION} --secondPacketCountDistribution=${SECOND_PACKET_COUNT_DISTRIBUTION}"
./waf --run "${APP_NAME} ${SIMULATION_ARGS}" --cwd="${WORK_DIR}" > ${WAF_LOG_FILE} 2>&1

# DEBUG
#./waf --run ${APP_NAME} --command-template="gdb --args %s ${SIMULATION_ARGS}" --cwd="${WORK_DIR}"

#tput bel
less ${WAF_LOG_FILE}
