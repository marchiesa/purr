#/bin/bash

failure=###FAILURES###
simulation_time=###TIME###
reconvergence=###RECONVERGENCE###
failure_time=###FAILURE_TIME###
frractive=###FRR_ACTIVE###
workload=###WORKLOAD###
runs=###RUNS###

for run in `seq 1 #runs`
do
  for x in 0.1 0.2 0.3 0.4 0.5 0.6 0.7
  do
    #./waf --run "conga-simulation-large --cdfFileName=examples/load-balance/$workload --randomSeed=$run --EndTime=$simulation_time -FlowLaunchEndTime=$simulation_time -runMode=ECMP --load=$x --transportProt=DcTcp --failure=$failure --reconvergence=$reconvergence --failureTime=$failure_time --frrActive=$frractive" &> out-$run-$simulation_time-$x-$failure-$reconvergence-$failure_time-$frractive-$workload.txt &
    echo "./waf --run \"conga-simulation-large --cdfFileName=examples/load-balance/$workload --randomSeed=$run --EndTime=$simulation_time -FlowLaunchEndTime=$simulation_time -runMode=ECMP --load=$x --transportProt=DcTcp --failure=$failure --reconvergence=$reconvergence --failureTime=$failure_time --frrActive=$frractive\""
    sleep 20
  done
done
