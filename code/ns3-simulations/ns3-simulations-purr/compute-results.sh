#/bin/bash

failure=1
run=3
link1=$1
link2=$2
failure_time=$3 

if [ -z "$link1" ]
then
  link1="*:*"
  link2="*:*"
  failure_time=0
fi

echo "avgFCT largeFCT smallFCT numberFlows numberLargeFlows numberSmallFlows rxBytes"
for x in 0.1 0.2 0.3 0.4 0.5 0.6 0.7
#for x in 0.1 
do
    python examples/load-balance/flowmon-parse-results-affected.py 0-1-large-load-4X4-$x-DcTcp-ecmp-simulation-$run-b600-$failure.xml $link1 $link2 $failure_time| grep -A 8 "Number of flows" > temp
    avgFCT=`cat temp | grep "^Avg FCT" | awk '{print $3}'` 
    largeFCT=`cat temp | grep "Large Flow Avg FCT" | awk '{print $5}'`
    smallFCT=`cat temp | grep "Small Flow Avg FCT" | awk '{print $5}'`
    numberFlows=`cat temp | grep "Number of f" | awk '{print $4}'`
    numberLargeFlows=`cat temp | grep "Number of l" | awk '{print $5}'`
    numberSmallFlows=`cat temp | grep "Number of s" | awk '{print $5}'`
    rxBytes=`cat temp | grep "Total RX Bytes" | awk '{print $4}'`
    echo $x " " $avgFCT " " $largeFCT " " $smallFCT " " $numberFlows " " $numberLargeFlows " " $numberSmallFlows " " $rxBytes
    rm temp
done
