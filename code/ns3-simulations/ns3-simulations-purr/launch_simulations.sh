#/bin/bash

failure=0
simulation_time=1
reconvergence=1
failure_time=0.2
frractive=0
workload="VL2_CDF.txt"
runs=1

for run in `seq 1 $runs`
do
  for x in 0.1 0.2 0.3 0.4 0.5 0.6 0.7
  do
    echo "./waf --run \"conga-simulation-large --cdfFileName=examples/load-balance/$workload --randomSeed=$run --EndTime=$simulation_time -FlowLaunchEndTime=$simulation_time -runMode=ECMP --load=$x --transportProt=DcTcp --failure=$failure --reconvergence=$reconvergence --failureTime=$failure_time --frrActive=$frractive\""
    ./waf --run "conga-simulation-large --cdfFileName=examples/load-balance/$workload --randomSeed=$run --EndTime=$simulation_time -FlowLaunchEndTime=$simulation_time -runMode=ECMP --load=$x --transportProt=DcTcp --failure=$failure --reconvergence=$reconvergence --failureTime=$failure_time --frrActive=$frractive" &> out-$run-$simulation_time-$x-$failure-$reconvergence-$failure_time-$frractive-$workload.txt &
    sleep 20
  done
done
