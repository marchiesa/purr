#!/bin/bash

cd ns3-simulations-reconvergence-one-failure

file=$1

failures=0
workload="\"DCTCP_CDF.txt\""

simulation_time=`cat ../$file | grep simulation_time | sed "s/simulation_time=//g"`
reconvergence_time=`cat ../$file | grep reconvergence_time | sed "s/reconvergence_time=//g"`
failure_time=`cat ../$file | grep failure_time | sed "s/failure_time=//g"`
runs=`cat ../$file | grep runs_websearch | sed "s/runs_websearch=//g"`

# create a launch script to run a set of simulations based on the parameters above and the ones read from the 'file' file
cat  launch_simulations.sh_template | sed "s/###FAILURES###/$failures/" | sed "s/###TIME###/$simulation_time/" | sed "s/###WORKLOAD###/$workload/" | sed "s/###RUNS###/$runs/" > launch_simulations.sh

chmod +x launch_simulations.sh
./launch_simulations.sh

cd ..
