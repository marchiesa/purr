#!/bin/bash

cd ns3-simulations-purr

file=$1

failures=1
frr_active=1
workload="\"VL2_CDF.txt\""

simulation_time=`cat ../$file | grep simulation_time | sed "s/simulation_time=//g"`
reconvergence_time=`cat ../$file | grep reconvergence_time | sed "s/reconvergence_time=//g"`
failure_time=`cat ../$file | grep failure_time | sed "s/failure_time=//g"`
runs=`cat ../$file | grep runs_datamining | sed "s/runs_datamining=//g"`

# create a launch script to run a set of simulations based on the parameters above and the ones read from the 'file' file
cat  launch_simulations.sh_template | sed "s/###FAILURES###/$failures/" | sed "s/###TIME###/$simulation_time/" | sed "s/###RECONVERGENCE###/$reconvergence_time/" | sed "s/###FAILURE_TIME###/$failure_time/" | sed "s/###FRR_ACTIVE###/$frr_active/" | sed "s/###WORKLOAD###/$workload/" | sed "s/###RUNS###/$runs/" > launch_simulations.sh

# launch the simulations
chmod +x launch_simulations.sh
./launch_simulations.sh

cd ..
