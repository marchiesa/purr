Contact for repeatability: Roshan Sedar

We present more details on the Depth First Search (DFS) algorithm. The main ingress control block is shown below.
To check whether DFS has been activated, our implementation relies on an additional 
bit in the header that is later extracted into the `local_metadata.pkt_start` metadata field.
In addition, it is assumed that the operational status of each physical output port of the switch 
is always reflected by a binary value stored in the corresponding P4 register.%

```
control ingress {
  if ( local_metadata.pkt_start == 0 ) {
       apply( default_route );
    if ( local_metadata.out_port == 0 ) { 
       apply( start_dfs ); }
  }  else {  
       ctrl_dfs_routing(); }
  if ( local_metadata.is_completed == 1 ) {
        apply( forward_to_parent );
  } else {
    apply( starting_port );
    apply( forward_pkt ); } }
control egress {}
```

Similarly to the `pkt_start` tag, the custom header fields `pkt_cur` and `pkt_par` are also 
extracted into the corresponding local metadata variables.
Packets having the `pkt_start` value set to zero are forwarded based on the default routing scheme.
If the preferred output port is unavailable at the time when the decision is made, the Fast-Reroute 
procedure is initiated and the `local_metadata.pkt_start` variable is set to `1`. 
Note that this value is set only once, by the first switch that encounters a~failure. 
On the other hand, the already-known packets which enter the ingress control block again 
are forwarded according to the implemented FRR mechanism. 
If all possible output ports have already been considered, 
packets are forwarded towards the corresponding parent nodes.

Before the packet is forwarded to one of the downstream nodes, the `Selected-Ports`
match-action table is searched for a matching entry based on the identifier of the output port `local_metadata.out_port` determined by DFS. 
As a~result, the `port_set` variable is set to the value associated with the matching entry.
The forwarding decision will be made based on the value of `port_set`, the status of the 
related physical port, and the `Fwd-Packet` match-action table maintained in TCAM.

```
control ctrl_dfs_routing {
if ( local_metadata.pkt_cur == 0 ) {
apply( curr_eq_zero ); }
apply( curr_neq_zero );
if ( local_metadata.out_port == PORTS + 1 ) {
apply( reached_depth ); }
if ( local_metadata.is_completed == 0 ) {
apply( set_starting_port );
apply( check_outport_status );
if ( local_metadata.out_port == local_metadata.pkt_par ) {
apply( jump_to_next );
if ( local_metadata.out_port == PORTS + 1 ) {
apply( send_to_parent ); } } }
if ( local_metadata.is_completed == 1 ) {
if ( local_metadata.out_port == 0 ) {
apply( out_eq_zero ); } } }
```

Additional details related to the operation of the DFS-based FRR algorithm are shown in the listing above.
For each packet handled by the local switch for the first time, the corresponding `local_metadata.pkt_cur`
variable equals `0`. In this case, the `curr_eq_zero` table is searched in parallel for the `local_metadata.pkt_cur`
key and, in addition,  the `local_metadata.pkt_par` local variable is set to the identifier of the port on which the packet was received.
In the other cases, the `curr_neq_zero` table is used to the value of `local_metadata.pkt_cur` by `1`, 
which becomes the identifier of the next output port to be considered while forwarding. The selected output port 
must be available and it may not lead to the upstream (parent) node. After all the output ports have been "tried" 
(`local_metadata.out_port` exceeds the number of available ports), the `local_metadata.is_completed` variable is set to `1` 
and the packet is sent towards the corresponding upstream node. 



Following the example presented above, other FRR mechanisms relying on 
circular sequences may also be implemented in a~similar way. However, 
more advanced schemes may require that additional changes be introduced 
into the presented control blocks.
