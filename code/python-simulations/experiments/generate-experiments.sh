#!/bin/bash

# experiment-template.jsontemp is a template that can be used to generate the configuration parameters of a simulation. We use sed to create different simulation settings

for algorithm in greedy; do
	for seed in `seq 0 5`; do
		for num_seq in  2 5 10 20 50 100 200 500 1000; do
			for size_seq in 8 16 32 ; do
                                if [ $size_seq -eq 32 ]; then
                                  if [ $num_seq -gt 100 ];then
                                    continue
                                  fi
                                fi
                                if [ $size_seq -eq 16 ]; then
                                  if [ $num_seq -gt 200 ];then
                                    continue
                                  fi
                                fi
				for cost in bit-cost; do
					for generator in random; do
						iterations=1
						cat experiment-template.jsontemp | sed "s/SEED/$seed/" |  sed "s/ALGORITHM/$algorithm/" |  sed "s/NUMBER\_SEQUENCES/$num_seq/" |  sed "s/SIZE\_SEQUENCES/$size_seq/" |  sed "s/COST/$cost/" |  sed "s/ITERATIONS/$iterations/" | sed "s/GENERATOR/$generator/" > $algorithm-$generator-$seed-$num_seq-$size_seq-$cost.json   
done
done
done
done
done
done
for algorithm in hierarchical; do
	for seed in `seq 0 5`; do
		for num_seq in  2 5 10 20 50 100 200 500 1000 2000; do
			for size_seq in 8 16 32 ; do
                                if [ $size_seq -eq 32 ]; then
                                  if [ $num_seq -gt 200 ];then
                                    continue
                                  fi
                                fi
				for cost in bit-cost; do
					for generator in random; do
						iterations=1
						cat experiment-template.jsontemp | sed "s/SEED/$seed/" |  sed "s/ALGORITHM/$algorithm/" |  sed "s/NUMBER\_SEQUENCES/$num_seq/" |  sed "s/SIZE\_SEQUENCES/$size_seq/" |  sed "s/COST/$cost/" |  sed "s/ITERATIONS/$iterations/" | sed "s/GENERATOR/$generator/" > $algorithm-$generator-$seed-$num_seq-$size_seq-$cost.json   
done
done
done
done
done
done
for algorithm in michael-scs; do
	for seed in `seq 0 5`; do
		for num_seq in  2 5 10 20 50 100 200 500 1000 2000 5000 10000 20000 50000 100000 200000; do
			for size_seq in 8 16 32 ; do
				for cost in bit-cost; do
					for generator in random; do
						iterations=1
						cat experiment-template.jsontemp | sed "s/SEED/$seed/" |  sed "s/ALGORITHM/$algorithm/" |  sed "s/NUMBER\_SEQUENCES/$num_seq/" |  sed "s/SIZE\_SEQUENCES/$size_seq/" |  sed "s/COST/$cost/" |  sed "s/ITERATIONS/$iterations/" | sed "s/GENERATOR/$generator/" > $algorithm-$generator-$seed-$num_seq-$size_seq-$cost.json   
done
done
done
done
done
done
#for algorithm in greedy michael-scs hierarchical; do
