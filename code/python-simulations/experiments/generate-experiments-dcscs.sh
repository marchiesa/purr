#!/bin/bash

# note that michael-scs is 'fast-greedy'
for algorithm in greedy michael-scs hierarchical dpscs; do
	for seed in `seq 0 5`; do
		for num_seq in  2 3 4 5 6 7 8; do
			size_seq=7
			for cost in bit-cost; do
				for generator in random; do
					iterations=1
                                        # experiment-template.jsontemp is a template that can be used to generate the configuration parameters of a simulation. We use sed to create different simulation settings
					cat experiment-template.jsontemp | sed "s/SEED/$seed/" |  sed "s/ALGORITHM/$algorithm/" |  sed "s/NUMBER\_SEQUENCES/$num_seq/" |  sed "s/SIZE\_SEQUENCES/$size_seq/" |  sed "s/COST/$cost/" |  sed "s/ITERATIONS/$iterations/" | sed "s/GENERATOR/$generator/" > $algorithm-$generator-$seed-$num_seq-$size_seq-$cost.json   
done
done
done
done
done
