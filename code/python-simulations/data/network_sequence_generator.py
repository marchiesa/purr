import os
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
import sys
sys.path.insert(0, dir_path)
from networks.NetworkRandomSigcomm import Network
from networks.NetworkRandomSigcomm import pretty


### This class generates sequences based on FRR arborescence-based mechanisms. 
### It constructs a random JellyFish topology and instantiate the arc-disjoint
### spanning arborescences. It then applies the log(n) FRR techniques explained in 
###
### Chiesa et al. "The quest for resilient (static) forwarding tables.". In INFOCOM 2016. 
### DOI: 10.1109/INFOCOM.2016.7524552

class NetworkSequenceGenerator():

    def __init__(self, size):
        self.size = size

    def generate_random_sequences(self, number_of_sequences, seed=None):

        sequences=[]
        sequences_dict={}

        ### this command creates a JellyFish network with arborescences
        network = Network(self.size,number_of_sequences, seed)

        ### iterate over all nodes to extract the FRR sequences
        for node in network.nodes:
            if node == 0:
                continue
            circ_sequence = []
            link = network.nodes[node].keys()[0]
            while len(circ_sequence) < 2*self.size:
                # get the ID of the tree currently analysed
                tree_id = network.getOutgoingTreeId(node,link)
                # add the tree to the sequence
                circ_sequence.append(tree_id)
                # get the node at the other side of the link for this tree on this node
                otherEnd=network.getOtherEndLink(node,link)
                # ge the ID of the bounced tree towards the analysed node
                bounced_tree=network.getOutgoingTreeId(otherEnd, link)
                # append the tree ID to the sequence elemements
                if bounced_tree == -1:
                    circ_sequence.append(tree_id)
                else:
                    circ_sequence.append(network.getOutgoingTreeId(otherEnd, link))

                # go to the next tree/arborescence
                tree_id= (tree_id + 1 )% self.size

                # update the 'link' variable based on this new tree
                for link2 in network.nodes[node]:
                    if network.getOutgoingTreeId(node, link2) == tree_id:
                        link = link2
                        break


            #remove duplicate from circular sequences
            circ_sequence = circ_sequence + circ_sequence
            for i in range(0,len(circ_sequence)/2):
                sequence = []
                for j in range(0,len(circ_sequence)/2):
                    if not circ_sequence[i+j] in sequence:
                        sequence.append(circ_sequence[i+j])
                tuple_copy=tuple(sequence)
                if not tuple_copy in sequences_dict:
                    sequences.append(sequence)
                    sequences_dict[tuple_copy]=1

        return sequences

if __name__ == "__main__":
    a=NetworkSequenceGenerator(16)

    print a.generate_random_sequences(2,0)
