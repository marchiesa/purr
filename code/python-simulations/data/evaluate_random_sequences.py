import os
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
import sys
sys.path.insert(0, dir_path)
import json
from pprint import pprint
import numpy as np

from scs.greedyscs import GreedySCS
from scs.hierarchicalscs import HierarchicalSCS
from scs.dpscs import DPSCS
from sequence_generator import SequenceGenerator
from network_sequence_generator import NetworkSequenceGenerator
from circular_sequence_generator import CircularSequenceGenerator
from scs.ScsHeuristics import ScsHeuristic1
from scs.multitablelcs import MultiTableLCS
from scs.dpscs import DPSCS
import time

# a function that computes the cost in TCAM bits of a sequence solution
# according to the following forumla: bit_cost = (num_seq + seq_size) * seq_size
def bit_cost(list,ports):
    cost = 0
    for element in list:
        if type(element[0]) != type([]):
            element=[element]
        for subelement in element:
            cost+= (len(subelement) + ports) * len(subelement)
    return cost

# a function that computes the entry-cost of a sequence solution. Simply counts the number of elements in the sequence. 
def entry_cost(list,ports):
    return len(list[0])


class EvaluateRandomSequences:

    f = None

    def __init__(self, seed=0):
        self.seed=seed

    # this function gets as input the generator of FRR sequences, the number of sequences, the size of the sequences, the algorithm to be evaluated, 
    # the number of iterations (we always use 1 as we use the seed to change the iterations, and the cost to be computed (currently ignored). 
    def evaluate(self, (generator_name,generator), number_of_sequences, size_of_sequences, (algorithm_name,algorithm), number_of_iterations=1,cost_string="bit-cost"):
        cost=bit_cost
        if cost_string=="entry-cost":
            cost=entry_cost
        # create the file that will contain the result of the simulation
        self.f = f = open("results/results-"+str(algorithm_name)+"-"+str(generator_name)+"-"+str(self.seed)+"-"+str(number_of_sequences)+"-"+str(size_of_sequences)+"-"+str(cost_string)+".txt","w")
        final_string = ""
        final_string +="algorithm generator seed number_of_sequences size_of_sequences cost_string scs-lenght time[ms]\n"

        for _ in range(0,number_of_iterations):
            # generate the random sequences according to the provided generator
            sequences = generator.generate_random_sequences(number_of_sequences, self.seed)
            print "len(seq_net): " + str(len(sequences))
            # measure the running time
            start = time.time()
            # run the given algorithm on the produced sequences
            sequence = algorithm.compute(sequences)
            end = time.time()
            print "Length of the " + str(algorithm_name) + " scs: " + str(sequence) + " len: " + str(len(sequence)) + " running time: " + str((end - start) * 1000) + " msec"
            final_string+=str(algorithm_name)+" "+str(generator_name)+" "+str(self.seed)+" "+str(number_of_sequences)+" "+str(size_of_sequences)+" "+str(cost_string)+" "+str(len(sequence))+" "+str((end - start) * 1000)+"\n"
        f.write(final_string)
        f.close()

if __name__ == "__main__":
    print "usage: python data/evaluate_random_sequences.py experiment-setting file"
    file_config=sys.argv[1]

    print "python: " + str(file_config)
    with open(file_config) as f:

        map = json.load(f)

        # load the proper algorithm module
        algo = None
        if map["algorithm"] == "greedy":
            algo = GreedySCS(1)
        elif map["algorithm"] == "hierarchical":
            algo =HierarchicalSCS(2)
        elif map["algorithm"] == "michael-scs":
            algo = ScsHeuristic1()
        elif map["algorithm"] == "dpscs":
            algo = DPSCS()

        # generate the proper generator module
        generator= None
        if map["generator"] == "random":
            generator = SequenceGenerator(map["size-of-sequences"])
        elif map["generator"] == "jellyfish":
            generator = NetworkSequenceGenerator(map["size-of-sequences"])
        elif map["generator"] == "circular":
            generator = CircularSequenceGenerator(map["size-of-sequences"])

        # extract the seed of the simulation
        a = EvaluateRandomSequences(map["seed"])
        # run the evaluation
        a.evaluate((map["generator"],generator),map["number-of-sequences"], map["size-of-sequences"], (map["algorithm"],algo),map["number-of-iterations"], map["cost"])
