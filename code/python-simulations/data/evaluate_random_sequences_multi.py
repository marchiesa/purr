import os
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
import sys
sys.path.insert(0, dir_path)
import json
from pprint import pprint
import numpy as np

from scs.greedyscs import GreedySCS
from scs.hierarchicalscs import HierarchicalSCS
from scs.dpscs import DPSCS
from sequence_generator import SequenceGenerator
from circular_sequence_generator import CircularSequenceGenerator
from network_sequence_generator import NetworkSequenceGenerator
from scs.multiScsHeuristics import MultiScsHeuristic1
from scs.multitablelcs import MultiTableLCS
from scs.dpscs import DPSCS
import time

def bit_cost(list,ports):
    cost = 0
    for element in list:
        #print "element " + str(element)
        if type(element[0]) != type([]):
            element=[element]
        for subelement in element:
            #print "(" + str(len(subelement)) + " + " + str(ports) + ") * " + str( len(subelement))
            cost+= (len(subelement) + ports) * len(subelement)
            #print "cost: " + str((len(subelement) + ports) * len(subelement))
    return cost

def entry_cost(list,ports):
    #print "list: " + str(list) + " len(list): " + str(len(list[0]))
    return len(list[0])


class EvaluateRandomSequences:

    f = None

    def __init__(self, seed=0):
        self.seed=seed

    def evaluate(self, (generator_name,generator), number_of_sequences, size_of_sequences, (algorithm_name,algorithm), number_of_iterations=1,cost_string="bit-cost", num_tables=1):
        cost=bit_cost
        if cost_string=="entry-cost":
            cost=entry_cost
        self.f = f = open("results-multi/results-"+str(algorithm_name)+"-"+str(generator_name)+"-"+str(self.seed)+"-"+str(number_of_sequences)+"-"+str(size_of_sequences)+"-"+str(num_tables)+"-"+str(cost_string)+".txt","w")
        #generator = SequenceGenerator(size_of_sequences)
        #algorithm_2_time = {x:[] for x in algorithms.keys()}
        #algorithm_2_scslength = {x:[] for x in algorithms.keys()}
        final_string = ""
        final_string +="algorithm generator seed number_of_sequences size_of_sequences cost_string tables scs-lenght bit-cost time[ms] \n"

        for _ in range(0,number_of_iterations):
            sequences = generator.generate_random_sequences(number_of_sequences, self.seed)
            #print "len(seq_net): " + str(len(sequences))
            print "seq: " + str(sequences)
            #print "*** sequences: " + str(sequences)
            self.seed+=1
            #for algorithm in algorithms.keys():
            #print "algorithm:" + str(algorithm)
            start = time.time()
            sequence = algorithm.compute(sequences)
            print "seuquences lengths: " + str(sequence)
            bit_cost_value = 0
            entry_cost = 0
            for length in sequence:
                entry_cost += length
                bit_cost_value += length*(length+size_of_sequences)    
            end = time.time()
            #algorithm_2_scslength[algorithm].append(sequence)
            #algorithm_2_time[algorithm].append((end-start))
            print "Length of the " + str(algorithm_name) + " scs: " + str(sequence) + " len: " + str(len(sequence)) + " running time: " + str((end - start) * 1000) + " msec"
            #print str(algorithm) + ": " + str(len(sequence))
            final_string+=str(algorithm_name)+" "+str(generator_name)+" "+str(self.seed)+" "+str(number_of_sequences)+" "+str(size_of_sequences)+" "+str(cost_string)+" "+str(len(sequence))+" "+str(entry_cost)+" "+str(bit_cost_value)+" "+str((end - start) * 1000)+"\n"
        f.write(final_string)
        f.close()

        #print algorithm_2_time
        #print algorithm_2_scslength
        #algorithm_2_time = {k: sum(v) / number_of_iterations for k, v in algorithm_2_time.items()}
        #algorithm_2_scslength = {k: (cost(v,size_of_sequences) / number_of_iterations) for k, v in algorithm_2_scslength.items()}
        #print "-------------------------------------------"
        #print "Final average performance and running time:"
        #print algorithm_2_time
        #print algorithm_2_scslength



def test_single_table():
    algorithms = {
        "greedy" : GreedySCS(1),
        "hierarchical2" : HierarchicalSCS(2),
        #"hierarchical3": HierarchicalSCS(3),
        "michael-scs-1": ScsHeuristic1()
    }

    a=EvaluateRandomSequences(0)
    a.evaluate(100,10,algorithms,100)

def test_multiple_tables():
    algorithms = {
        "multi-LCS" : MultiTableLCS(4),
        "michael-scs-1": ScsHeuristic1()
    }

    a=EvaluateRandomSequences(0)
    a.evaluate(20,6,algorithms,10)

def test_dpscs():
    algorithms = {
        "DP-SCS" : DPSCS(),
        "michael-scs-1": ScsHeuristic1(),
    }

    a=EvaluateRandomSequences(0)
    a.evaluate(10,4,algorithms,1,entry_cost)


if __name__ == "__main__":
    file_config=sys.argv[1]

    print "python: " + str(file_config)
    with open(file_config) as f:

        map = json.load(f)

        algo = None
        if map["algorithm"] == "greedy":
            algo = GreedySCS(1)
        elif map["algorithm"] == "hierarchical":
            algo =HierarchicalSCS(2)
        elif map["algorithm"] == "michael-scs":
            algo = MultiScsHeuristic1(int(map["num-tables"]))
        elif map["algorithm"] == "multilcs":
            algo = MultiTableLCS(int(map["num-tables"]))
        elif map["algorithm"] == "dpscs":
            algo = DPSCS()

        generator= None
        if map["generator"] == "random":
            generator = SequenceGenerator(map["size-of-sequences"])
        elif map["generator"] == "circular":
            generator = CircularSequenceGenerator(map["size-of-sequences"])

        a = EvaluateRandomSequences(map["seed"])
        a.evaluate((map["generator"],generator),map["number-of-sequences"], map["size-of-sequences"], (map["algorithm"],algo),map["number-of-iterations"], map["cost"], int(map["num-tables"]))
