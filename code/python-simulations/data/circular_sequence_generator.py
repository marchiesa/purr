import numpy as np

### Generates circular sequences starting from a random permutation of the ports

class CircularSequenceGenerator():

    def __init__(self, size):
        self.size = size

    def generate_random_sequence(self,seed=None):
        stamp=np.random.permutation(self.size)
        sequences = []

        for x in range(0, self.size):
            sequence = []
            for j in range(x, x + self.size):
                sequence.append(stamp[j % self.size])
            sequences.append(sequence)
        return sequences

    def generate_random_sequences(self, number_of_sequences, seed=None):
        if not seed == None:
            np.random.seed(seed)

        sequences=[]
        for _ in range(number_of_sequences):
            sequences.extend(self.generate_random_sequence())
        return sequences

if __name__ == "__main__":
    a=CircularSequenceGenerator(10)
    print a.generate_random_sequences(10,0)
    print a.generate_random_sequences(10,1)
