import numpy as np

# This class simply generates random sequences
class SequenceGenerator():

    def __init__(self, size):
        self.size = size

    def generate_random_sequence(self,seed=None):
        return np.random.permutation(self.size)

    def generate_random_sequences(self, number_of_sequences, seed=None):
        if not seed == None:
            np.random.seed(seed)

        sequences=[]
        for _ in range(number_of_sequences):
            sequences.append(self.generate_random_sequence())
        return sequences

if __name__ == "__main__":
    a=SequenceGenerator(10)
    print a.generate_random_sequences(10,0)
    print a.generate_random_sequences(10,1)
