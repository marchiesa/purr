#!/bin/bash

# generate all the configuration files for running the experiments. All the *.json files contained in the experiments/ folder will be executed

cd experiments
./generate-experiments.sh
cd ..
# evaluate greedy
./evaluate.sh greedy
# evalute hierarchical
./evaluate.sh hierarchical
# evaluate fastgreed
./evaluate.sh scs
rm experiments/*json

