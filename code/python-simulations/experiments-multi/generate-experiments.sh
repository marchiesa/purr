#!/bin/bash

# experiment-template.jsontemp is a template that can be used to generate the configuration parameters of a simulation. We use sed to create different simulation settings

for algorithm in michael-scs multilcs; do
	for seed in `seq 0 5`; do
		for num_seq in 10 20 50 100 200; do
			for size_seq in  16 ; do
				for cost in bit-cost; do
					for generator in random ; do
                                            for num_tables in 1 2 3; do
						iterations=1
						cat experiment-template.jsontemp | sed "s/SEED/$seed/" |  sed "s/ALGORITHM/$algorithm/" |  sed "s/NUMBER\_SEQUENCES/$num_seq/" |  sed "s/SIZE\_SEQUENCES/$size_seq/" |  sed "s/COST/$cost/" |  sed "s/ITERATIONS/$iterations/" | sed "s/GENERATOR/$generator/" | sed "s/NUM_TABLES/$num_tables/" > $algorithm-$generator-$seed-$num_seq-$size_seq-$num_tables-$cost.json   
done
done
done
done
done
done
done
