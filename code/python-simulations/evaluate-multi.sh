#!/bin/bash
for exp_file in `find experiments-multi -name '*json'  |  grep $1 |  awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);value=$4*$5;$2=value"-"$2;print $0}' | sed "s/--//g" | sort -t$'-' -k2 -n | awk -v FS="-" -v OFS="-" '{gsub(/[0-9]*/,"",$2);gsub("michaelscs","michael-scs");print $0}' | sed "s/--/-/g"`; do
  echo "exp_file: " $exp_file
  results_file=`echo $exp_file | sed "s/json/txt/" | sed "s#experiments-multi/#results-multi/results-#"` 
  echo "results_file: " $results_file
  destination=`echo $results_file | sed "s#results-multi/#results-multi/failed/#"`
  num_seq=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $5}'`
  siz_seq=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $6}'`
  #echo "destination: " $destination
  #echo "num_seq: " $num_seq
  #echo "siz_seq: " $siz_seq

  check=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);gsub("michaelscs","michael-scs");print $0}' | sed "s/--//g" | sed "s#results-multi/failed/##"`
  already_done=`find results-multi/failed/ -name '*' | grep $check | wc -l`
  analyze=1
  if [ "$already_done" -gt "0" ];then
    analyze=0
    #echo "finish"
    touch $destination
    continue
  fi

  check=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"0",$4);gsub(/.*/,"[0-9]*",$5);gsub("michaelscs","michael-scs");print $0}' | sed "s/--//g" | sed "s#results-multi/failed/##"`
  #echo "check: " $check
  for file in `find results-multi/failed/ -name '*' | grep $check`; do
    echo "file: " $file
    num_seq_2=`echo $file | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $5}'`
    siz_seq_2=`echo $file | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $6}'`
    #echo "num_seq_2: " $num_seq_2
    #echo "siz_seq_2: " $siz_seq_2
    
    if [ "$num_seq" -gt "$num_seq_2" ]; then
      analyze=0
      break
    fi
    if [ "$siz_seq" -gt "$siz_seq_2" ]; then
      analyze=0
      break
    fi
  done
  if [ "$analyze" -eq "0" ]; then
    #echo "finish"
    touch $destination
    continue
  fi 
  check=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"0",$4);gsub(/.*/,"[0-9]*",$6);gsub("michaelscs","michael-scs");print $0}' | sed "s/--//g" | sed "s#results-multi/failed/##"`
  for file in `find results-multi/failed/ -name '*' | grep $check`; do
    echo "file: " $file
    num_seq_2=`echo $file | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $5}'`
    siz_seq_2=`echo $file | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $6}'`
    #echo "num_seq_2: " $num_seq_2
    #echo "siz_seq_2: " $siz_seq_2

    if [ "$num_seq" -gt "$num_seq_2" ]; then
      analyze=0
      break
    fi
    if [ "$siz_seq" -gt "$siz_seq_2" ]; then
      analyze=0
      break
    fi
  done 
  if [ "$analyze" -eq "0" ]; then
    #echo "finish"
    touch $destination
    continue
  fi

  #echo "analyze" $analyze
  if [ ! -f $results_file ]; then
    if [ ! -f $destination ]; then
      echo "executed " $results_file
      python data/evaluate_random_sequences_multi.py $exp_file
      size=`cat $results_file | wc -l`
      echo $size
      if [ "$size" -eq "0" ]; then
        rm $results_file
      fi
    fi
  fi 
done
