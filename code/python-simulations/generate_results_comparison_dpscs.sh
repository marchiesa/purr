#!/bin/bash

cd experiments
# generate all the configuration files for running the experiments. All the *.json files contained in the experiments/ folder will be executed
./generate-experiments-dcscs.sh
cd ..
# evaluate DPSCS
./evaluate.sh dpscp
# evaluate GREEDY
./evaluate.sh greedy
# evaluate HIERARCHICAL
./evaluate.sh hierarchical
# evaluate FAST-GREEDY
./evaluate.sh scs
rm experiments/*json


