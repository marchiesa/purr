
### This class is used to implement methods that are in common among all the heuristics and optimal mechanisms


class SCSSolver:
    def __init__(self):
        pass

    # this function eleminate elements from an SCS sequence that are redundant. 
    def simplify(self, frr_sequences, scs):
        removed_elements=[]
        current_scs_prefix = []
        current_scs_subfix = list(scs[1:len(scs)])
        for i in range(len(scs)):
            if not self.check_scs(frr_sequences, current_scs_prefix+current_scs_subfix):
                current_scs_prefix.append(scs[i])
            if len(current_scs_subfix)>0:
                current_scs_subfix.pop(0)
        return current_scs_prefix

    # this function checks whether the computed SCS is correct
    def check_scs(self,sequences,scs):
        for sequence in sequences:
            index = 0
            for i in range(len(sequence)):
                while scs[index] != sequence[i]:
                    index+=1
                    if index == len(scs):
                        return False
        return True

    # this function has not been used in the simulation. It attempts to swap elements in an effort to improve the SCS. 
    def swapping_heuristic(self, frr_sequences, scs):
        swapped = True
        counter = 0
        while swapped:
            swapped = False
            for i in range(1,len(scs)):
                for j in range(0,len(scs)-i):
                    temp = scs[j+i]
                    for x in range(j + i, j, -1):
                        scs[x] = scs[x - 1]
                    scs[j] = temp
                    current_length = len(scs)
                    new_scs = self.simplify(frr_sequences, scs)
                    new_length = len(new_scs)
                    if new_length >= current_length:
                        temp = scs[j]
                        for x in range(j, j + i, 1):
                            scs[x] = scs[x + 1]
                        scs[j+i] = temp
                    else:
                        print " swapping " + str(scs[j + i]) + " with " + str(scs[j])
                        swapped = True
                        break
                if swapped:
                   break
            if swapped:
                scs = self.simplify(frr_sequences, scs)
            counter += 1
            if counter % 1 == 0:
                print "len:" + str(len(scs)) + " scs: " + str(scs)

        return scs





if __name__ == "__main__":
    a=SCSSolver()
    frr_sequences = [[5, 2, 1, 3, 0, 4],[1, 3, 4, 0, 2, 5],[3, 5, 1, 2, 4, 0],[5, 2, 3, 4, 1, 0],[2, 4, 3, 5, 1, 0],[3, 1, 4, 2, 0, 5],[0, 1, 2, 4, 5, 3],[5, 2, 3, 4, 1, 0],[3, 1, 4, 5, 2, 0],[1, 3, 0, 5, 4, 2], [1, 2, 3, 1, 2, 4]]
    scs = [5, 3, 1, 2, 0, 3, 5, 4, 1, 3, 2, 0, 5, 4, 2, 1, 5, 0, 3]
    print scs
    print a.simplify(frr_sequences,scs)
    print a.swapping_heuristic(frr_sequences,scs)

