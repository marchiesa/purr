import time
import random
import sys

### This class computes the number of tables that bring to the best cost using the FAST-GREEDY heuristic
### This heuristic is not shown in the paper


class MultiScsHeuristic1():
    cost_function = lambda _, x, k: (x + k) * x
    num_of_tables=0

    def __init__(self,num_of_tables):
        self.num_of_tables=num_of_tables

    def compute(self, frr_sequences):

        start = time.time()
        num_of_ports = len(frr_sequences[0])
        my_frr_sequences_sets = []
        map(lambda seq: my_frr_sequences_sets.append([seq]), frr_sequences)
        my_frr_costs = []
        map(lambda seq: my_frr_costs.append(self.cost_function(num_of_ports,num_of_ports)), frr_sequences)

        print "self.num_of_tables" + str(self.num_of_tables)
        while len(my_frr_sequences_sets) > self.num_of_tables:
            print len(my_frr_sequences_sets)
            min_scs_len = sys.maxint
            min_i = -1
            min_j = -1
            for i in range(0, len(my_frr_sequences_sets) - 1):
                for j in range(i + 1, len(my_frr_sequences_sets)):
                    tmp = len(self.compute_helper(my_frr_sequences_sets[i] + my_frr_sequences_sets[j])[0])
                    if tmp < min_scs_len:
                        min_scs_len = tmp
                        min_i = i
                        min_j = j

            my_frr_sequences_sets[min_i] = my_frr_sequences_sets[min_i] + my_frr_sequences_sets[min_j]
            my_frr_costs[min_i] = self.cost_function(len(self.compute_helper(my_frr_sequences_sets[min_i])[0]),num_of_ports)

            del my_frr_sequences_sets[min_j]
            del my_frr_costs[min_j]

        improved = True
        while len(my_frr_sequences_sets)>1 and improved:
            improved = False
            print len(my_frr_sequences_sets)
            scs_sequences_length = [len(self.compute_helper(frr_set)[0]) for frr_set in my_frr_sequences_sets]
            num_of_ports = len(my_frr_sequences_sets[0][0])
            total_bits = [(single_table_seq_len + num_of_ports) * single_table_seq_len for single_table_seq_len in
                          scs_sequences_length]
            min_scs_len = sys.maxint
            min_i = -1
            min_j = -1
            for i in range(0, len(my_frr_sequences_sets) - 1):
                for j in range(i + 1, len(my_frr_sequences_sets)):
                    tmp = len(self.compute_helper(my_frr_sequences_sets[i] + my_frr_sequences_sets[j])[0])
                    if tmp < min_scs_len:
                        min_scs_len = tmp
                        min_i = i
                        min_j = j

            tmp = self.cost_function(
                len(self.compute_helper(my_frr_sequences_sets[min_i] + my_frr_sequences_sets[min_j])[0]), num_of_ports)
            if tmp < my_frr_costs[min_i]  + my_frr_costs[min_j]:
                improved = True
                my_frr_sequences_sets[min_i] = my_frr_sequences_sets[min_i] + my_frr_sequences_sets[min_j]
                my_frr_costs[min_i] = self.cost_function(
                    len(self.compute_helper(my_frr_sequences_sets[min_i])[0]), num_of_ports)

                del my_frr_sequences_sets[min_j]
                del my_frr_costs[min_j]

        end = time.time()
        print len(my_frr_sequences_sets)
        scs_sequences = [self.compute_helper(frr_set)[0] for frr_set in my_frr_sequences_sets]
        scs_sequences_length = [len(self.compute_helper(frr_set)[0]) for frr_set in my_frr_sequences_sets]
        print scs_sequences
        total_bits = [(single_table_seq_len + num_of_ports) * single_table_seq_len for single_table_seq_len in
                      scs_sequences_length]
        return scs_sequences_length

    def compute_helper(self, frr_sequences):
        start = time.time()
        my_frr_sequences = []
        map(lambda seq: my_frr_sequences.append(seq), frr_sequences)
        currseq = []
        total_elements = len(my_frr_sequences) * len(my_frr_sequences[0])

        while total_elements > 0:
            max_len = max(map(lambda seq: len(seq), my_frr_sequences))
            first_elem_of_max_len_sequences = dict()
            max_freq = -1
            max_freq_elem = -1

            # find most frequent first elemnt in the longest sequences
            for seq in my_frr_sequences:
                if len(seq) == max_len:
                    prev_freq = 0
                    if seq[0] in first_elem_of_max_len_sequences:
                        prev_freq = first_elem_of_max_len_sequences.get(seq[0])

                    first_elem_of_max_len_sequences.update({seq[0]: prev_freq + 1})
                    if prev_freq + 1 > max_freq:
                        max_freq = prev_freq + 1
                        max_freq_elem = seq[0]

            currseq.append(max_freq_elem)
            # remove this most frequent first element from all the sequences
            for i, seq in enumerate(my_frr_sequences):
                if len(seq) > 0 and seq[0] == max_freq_elem:
                    my_frr_sequences[i] = seq[1:]
                    total_elements -= 1


        end = time.time()

        num_of_ports = len(frr_sequences[0])
        total_bits = (len(currseq) + num_of_ports) * len(currseq)
        return [currseq]


if __name__ == "__main__":
    a = MultiScsHeuristic1(3)
    sequences=[]
    size=8
    for x in range(0,size):
        sequence=[]
        for j in range(x,x+size):
            sequence.append(j%size)
        sequences.append(sequence)
    for x in range(0,size):
        sequence=[]
        for j in range(x,x+size):
            sequence.append(size-(j%size)-1)
        sequences.append(sequence)
    print sequences
    print a.compute(sequences)




