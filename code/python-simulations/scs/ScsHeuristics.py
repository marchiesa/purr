import time

### This class implements FastGreedy

class ScsHeuristic1():
    
    def compute(self, frr_sequences):
        start = time.time()

        # copying the input sequence sto prevent input modification. basically, we don't have to do this.
        my_frr_sequences = []
        map(lambda seq : my_frr_sequences.append(seq), frr_sequences)
        
        currseq = []
        total_elements = len(my_frr_sequences) * len(my_frr_sequences[0])
        
        while total_elements > 0:
            max_len = max(map(lambda seq : len(seq), my_frr_sequences))
            first_elem_of_max_len_sequences = dict()
            max_freq = -1
            max_freq_elem = -1

            # find most frequent first elemnt in the longest sequences
            for seq in my_frr_sequences:
                if len(seq) == max_len:
                    prev_freq = 0
                    if seq[0] in first_elem_of_max_len_sequences:
                        prev_freq = first_elem_of_max_len_sequences.get(seq[0])

                    first_elem_of_max_len_sequences.update({seq[0]: prev_freq + 1})
                    if prev_freq + 1 > max_freq:
                        max_freq = prev_freq + 1
                        max_freq_elem = seq[0]


            currseq.append(max_freq_elem)
            # remove this most frequent first element from all the sequences
            for i, seq in enumerate(my_frr_sequences):
                if len(seq) > 0 and seq[0] == max_freq_elem:
                    my_frr_sequences[i] = seq[1:]
                    total_elements -= 1


            
        end = time.time()
        return currseq


if __name__ == "__main__":
    a = ScsHeuristic1()
    a.compute([[3,2,1,0],[3,2,0,1],[2,1,0,3],[3,0,1,2],[0,1,2,3],[2,3,0,1]])




