import itertools
import numpy as np
from scssolver import SCSSolver
import time

### This class implements DPSCS

class DPSCS(SCSSolver):
    matrix ={}
    def __init__(self):
        pass

    # the computation takes as input the set of sequences
    def compute(self, sequences):

        # we measure the running time
        start = time.time()

        # build a k-dimensional matrix
        self.matrix = np.ndarray(shape=[len(x)+1 for x in sequences], dtype=int, order='C')

        #inizialize single dimensions of matrix
        for x in range(len(sequences)):
            for y in range(len(sequences[x])+1):
                self.matrix[tuple([0]*x+[y]+[0]*(len(sequences)-x-1))]=y
        args = map(lambda x: range(len(x)+1), sequences)
        argslen = map((lambda x: len(x)+1), sequences)
        final=reduce((lambda x, y: x * y), argslen)
        counter =0

        # further initialization to speed up processing
        set_of_elements=[]
        for sequence in sequences:
            for element in sequence:
                if element not in set_of_elements:
                    set_of_elements.append(element)
        element_to_sequence = { x : [1] + [-1] * (len(sequences)) for x in set_of_elements}
        elements_scroll = [1] + [-1] * len(set_of_elements)
        elements_check = { x : -1 for x in set_of_elements}

        indices=np.array([-1]*len(sequences))

        
        # while solving the problem, we report some statistics on the remaining time using starttime and lasttime variables.
        starttime = time.time()
        lasttime = time.time()
        for x in itertools.product(*args):
            counter+=1
            if counter % 100000 ==0:
                endtime = time.time()
                remaining_time = ((endtime-starttime)/counter)*(final-counter)
                remaining_time_2 = ((endtime-lasttime)/100000)*(final-counter)
                print str(counter) + " of " + str(final) + " -> " + str(int((counter*100.0)/final)) + "%, " + str(int(remaining_time/60/60)) + "h " + str(int(remaining_time/60) %60) + "m " + str(int(remaining_time) % 60) + "s, " + str(int(remaining_time_2/60/60)) + "h " + str(int(remaining_time_2/60) %60) + "m " + str(int(remaining_time_2) % 60) + "s "
                lasttime = time.time()

            # find sequences ending with the same element
            found_one=False
            for y in range(len(sequences)):
                if x[y]-1 < 0:
                    continue
                found_one=True
                element = sequences[y][x[y] - 1]
                element_to_sequence[element][element_to_sequence[element][0]]=y
                element_to_sequence[element][0]+=1
                if elements_check[element] != counter:
                    elements_scroll[elements_scroll[0]] = element
                    elements_scroll[0]+=1
                    elements_check[element] = counter
            if not found_one:
                continue

            # compute the minimum among all candidate elements
            min = 99999999
            is_first=True
            counter_element=1
            for element in elements_scroll:
                if is_first:
                    is_first=False
                    continue
                elif counter_element == elements_scroll[0]:
                    break
                counter_element+=1
                counter_2=0
                for value in x:
                    indices[counter_2]=value
                    counter_2+=1
                is_first_2 = True
                counter_element_to_sequence = 1
                for index in element_to_sequence[element]:
                    if is_first_2:
                        is_first_2 = False
                        continue
                    elif counter_element_to_sequence == element_to_sequence[element][0]:
                        break
                    counter_element_to_sequence+=1
                    indices[index]=indices[index]-1
                element_to_sequence[element][0] = 1
                if min > self.matrix[tuple(indices)]+1:
                    min = self.matrix[tuple(indices)]+1
            elements_scroll[0] = 1
            self.matrix[tuple(x)]=min
        x=np.array(map(lambda  x : len(x), sequences))
        scs_sequence=[]

        # reconstruct the optimal sequence performing standard backtracking in dynamic programming
        # TODO: the next part of the code has not been optimized yet
        while np.any(x):
            element_to_sequence = {}
            for y in range(len(sequences)):
                if x[y]-1 <0:
                    continue
                element = sequences[y][x[y]-1]
                if element not in element_to_sequence:
                    element_to_sequence[element]=[]
                element_to_sequence[element].append(y)
            for element in element_to_sequence:
                indices = np.array(x)
                for index in element_to_sequence[element]:
                    indices[index]=indices[index]-1
                if self.matrix[tuple(indices)]+1 == self.matrix[tuple(x)]:
                    optimal_element = element
                    x = indices
                    break
            scs_sequence.insert(0,optimal_element)
        return scs_sequence

    ### The following function is not currently being used in the simulations. It was a previous implementation using a different mechanism.
    def compute_old(self, sequences):
        # print "input sequences: " + str(sequences)
        # mb: is there a way to build the k-dimensional matrix with one command in python?

        start = time.time()
        self.matrix = np.array(self.create_matrix(len(sequences), 0, map(lambda x: len(x) + 1, sequences)))

        print "time: " + str((time.time() - start) * 1000)
        # print self.matrix
        # inizialize single dimensions of matrix
        for x in range(len(sequences)):
            for y in range(len(sequences[x]) + 1):
                self.matrix[tuple([0] * x + [y] + [0] * (len(sequences) - x - 1))] = y
        # print self.matrix
        # print str(map(lambda x: range(len(x)), sequences))
        args = map(lambda x: range(len(x) + 1), sequences)
        counter = 0

        # x=[0,0,0,1] or 'd' in the tex file
        for x in itertools.product(*args):
            counter += 1
            if counter % 100000 == 0:
                print counter
            # find sequences ending with the same element
            element_to_sequence={}
            for y in range(len(sequences)):
                # print " x:" + str(x) + "y:" + str(y)
                if x[y] - 1 < 0:
                    continue
                element = sequences[y][x[y] - 1]
                # print "element: " + str(element)
                if element not in element_to_sequence:
                    element_to_sequence[element] = []
                element_to_sequence[element].append(y)
                # print "element_to_sequence: " + str(element_to_sequence)
            if len(element_to_sequence.keys()) == 0:
                continue
            # compute minimum
            min = 99999999
            for element in element_to_sequence:
                indices = np.array(x)
                for index in element_to_sequence[element]:
                    indices[index] = indices[index] - 1
                    # print "value of " + str(indices) + " is " + str(self.matrix[tuple(indices)])
                if min > self.matrix[tuple(indices)] + 1:
                    min = self.matrix[tuple(indices)] + 1
            # update matrix with minimum
            self.matrix[tuple(x)] = min
            # print "value of " + str(x) + " is " + str(self.matrix[(x)])
        # retrieve solution
        # print "**** retrieve solution ****"
        x = np.array(map(lambda x: len(x), sequences))
        # print "x : " + str(x)
        # print self.matrix
        scs_sequence = []
        '''while np.any(x):
            element_to_sequence = {}
            for y in range(len(sequences)):
                if x[y] - 1 < 0:
                    continue
                # print " x:" + str(x) + "y:" + str(y) + " x[y]:" + str(x[y]) + " x[y]-1:" + str(
                #    x[y] - 1) + " sequences[y]:" + str(sequences[y]) + " " + str(sequences[y][x[y] - 1])
                element = sequences[y][x[y] - 1]
                # print "element: " + str(element)
                if element not in element_to_sequence:
                    element_to_sequence[element] = []
                element_to_sequence[element].append(y)
            # print "element_to_sequence: " + str(element_to_sequence)
            for element in element_to_sequence:
                indices = np.array(x)
                for index in element_to_sequence[element]:
                    indices[index] = indices[index] - 1
                if self.matrix[tuple(indices)] + 1 == self.matrix[tuple(x)]:
                    optimal_element = element
                    x = indices
                    break
            # print "optimal_element: " + str(optimal_element)
            scs_sequence.insert(0, optimal_element)
        print "SCS: " + str(scs_sequence)
        return scs_sequence'''
        final_index = [len(x) for x in sequences]
        return [0] * self.matrix[tuple(final_index)]

    def create_matrix(self, depth,i,sizes):
        matrix=0
        if(i<depth-1):
            matrix=self.create_matrix(depth,i+1,sizes)
        return [matrix]*sizes[i]

if __name__ == "__main__":
    a=DPSCS()
    frr_seq = [[5, 2, 1, 3, 0, 4],
           [1, 3, 4, 0, 2, 5],
           [1, 3, 0, 5, 4, 2]]
    print str(a.compute([[1,2,3],[3,2,1]]))
