
import itertools
import numpy as np
from lcssolver import LCSSolver
import time


### This class implements an LCS solver that runs in polynomial time over sequences without repetitions. 

class LCS(LCSSolver):

    graph = None
    
    def __init__(self):
        pass
    def compute(self, sequences):

        # construct self.graph adjacency matrix
        self.graph = np.array([[1 for x in range(len(sequences[0]))] for y in range(len(sequences[0]))])
        for sequence in sequences:
            for i in range(0,len(sequence)-1):
                for j in range(i+1,len(sequence)):
                    from_node=sequence[j]
                    to_node=sequence[i]
                    self.graph[from_node][to_node]=0
        for i in range(0, len(sequence)):
            self.graph[i][i]=0

        graph_for_topo=np.array(self.graph)
        node_2_distance=np.array([0 for x in range(len(graph_for_topo))])
        node_2_nexthop = np.array([-1 for x in range(len(graph_for_topo))])

        # compute the topological order and longest negative path, which identifes candidate  LCS
        topologic_order=[]
        analyzed_nodes=set()
        minimum = 10000000
        node_minimum = -1
        while len(topologic_order) != len(graph_for_topo):
            node = self.find_node_with_no_outgoing_arcs(graph_for_topo,analyzed_nodes)
            for j in range(len(graph_for_topo)):
                graph_for_topo[j][node]=0
            for j in range(len(self.graph[node])):
                if j in analyzed_nodes and self.graph[node][j]==1:
                    if node_2_distance[node] > node_2_distance[j] -1:
                        node_2_distance[node] = node_2_distance[j] -1
                        node_2_nexthop[node] = j
            if node_2_distance[node] < minimum:
                minimum = node_2_distance[node]
                node_minimum = node
            analyzed_nodes.add(node)
            topologic_order.append(node)

        # find the minimum among all the nodes, which represents the longest LCS
        nexthop=node_minimum
        lcs = []
        while True:
            lcs.append(nexthop)
            nexthop = node_2_nexthop[nexthop]
            if nexthop==-1:
                break

        return lcs


    def find_nodes_with_no_outgoing_arcs(self,graph_for_topo):
        list=[]
        for i in range(len(graph_for_topo)):
            if not np.any(graph_for_topo[i]):
                list.append(i)
        return list

    def find_node_with_no_outgoing_arcs(self,graph_for_topo, analized_nodes):
        for i in range(len(graph_for_topo)):
            if i not in analized_nodes and not np.any(graph_for_topo[i]):
                return i
        return -1


if __name__ == "__main__":
    a=LCS()
    print a.compute([[0, 1, 2, 3, 4, 5],[5, 4, 3, 2, 1, 0], [5, 4, 3, 2, 0, 1]])
