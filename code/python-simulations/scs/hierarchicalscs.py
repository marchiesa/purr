from scssolver import SCSSolver
from dpscs import DPSCS
import math

### This class implements Hierarchical-SCS based on DPSCS

class HierarchicalSCS(SCSSolver):

    def __init__(self, d):
        self.d=d
        self.dp_scs=DPSCS()

    def compute(self, frr_sequences, simplify=True):
        if len(frr_sequences) <= self.d:
            return self.dp_scs.compute(frr_sequences)
        else:
            scs_sequences=[]
            delta=int(math.floor(len(frr_sequences)/self.d))
            # iterate over pairs of increasingly growing sets of sequences. 
            for i in range(self.d):
                sub_sequences = frr_sequences[i*delta:(i+1)*delta]+frr_sequences[self.d*delta+i:self.d*delta+i+1]

                # irecursively iterate over the self.d sets of elements
                scs_sequences.append(self.compute(sub_sequences))
            # compute the optimal SCS using DPSCS
            scs=self.dp_scs.compute(scs_sequences)
            if simplify:
                 #simplify the sequences to remove unused elements.
                scs = self.simplify(frr_sequences, scs)
            return scs


if __name__ == "__main__":
    a=HierarchicalSCS(2)
    a.compute([["b","a"],["a","b"],["a","a"],["b","b"],["a","c"]])
