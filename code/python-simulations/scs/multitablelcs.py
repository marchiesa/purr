from scssolver import SCSSolver
from dpscs import DPSCS
import math
from lcs import LCS
from greedyscs import GreedySCS
from scs.ScsHeuristics import ScsHeuristic1
import time
import numpy as np

 ### This class implements a simple solver for multiple table optimization based on the optimized LCS solver

class MultiTableLCS():

    scssolver=ScsHeuristic1()
    cost_function = lambda _, x, k: (x + k) * x
    ports=None

    # the constructor gets as input the number of tables to be targeted
    def __init__(self, number_of_tables):
        self.number_of_tables=number_of_tables
        self.index_to_lcs={}

    def compute(self, frr_sequences,simplify=True):
        self.ports = int(len(frr_sequences[0]))
        best_i=-1
        best_j=-1
        # create sets of sets of single sequences
        S=[[sequence] for sequence in frr_sequences]
        size_of_S=len(frr_sequences)
        # iterate until the target number of tables have been reached
        for _ in range(size_of_S-self.number_of_tables):
            max_LCS_value=0
            print str(size_of_S)
            for i in range(size_of_S-1):
                for j in range(i+1,size_of_S):
                    lcs = LCS()
                    # compare the LCS value of two sets of sequences
                    LCS_value = lcs.compute(S[i] + S[j])
                    # if the LCS length is good, store the pair of indices
                    if len(LCS_value) > max_LCS_value:
                        best_i = i
                        best_j = j
                        max_LCS_value = len(LCS_value)
            # merge the sets with the highest LCS
            S[best_i].extend(S[best_j])
            del S[best_j]
            size_of_S-=1
        print S

        index_to_scs = [0 for x in range(len(S))]
        for i in range(len(S)):
            index_to_scs[i] = self.scssolver.compute(S[i])

        # compute current cost
        cost=0
        for table in index_to_scs:
            cost+=self.cost_function(len(table),self.ports)

        # check if it is worth mergin to tables to reduce the cost
        merged_something = True
        merged_something = False #merging disabled
        while merged_something:
            merged_something = False
            breaked = False
            best_i = -1
            best_j = -1
            for i in range(len(S)-1):
                for j in range(i+1,len(S)):
                    # we use fast greedy to check the SCS obtainable by merging two tables
                    new_scs = self.scssolver.compute(S[i]+S[j])
                    # if the cost is smaller, we merge the tables
                    if self.cost_function(len(new_scs),self.ports) < self.cost_function(len(index_to_scs[i]),self.ports) + self.cost_function(len(index_to_scs[j]),self.ports):
                        merged_something = True
                        breaked=True
                        best_j = j
                        best_i = i
                        break
                if breaked:
                    break
            if breaked:
                index_to_scs[i] = self.scssolver.compute(S[i] + S[j])
                del index_to_scs[j]
                S[i].extend(S[j])
                del S[j]

        return [len(x) for x in index_to_scs]


if __name__ == "__main__":
    a=MultiTableLCS(2)
    print a.compute([[0,1,2,3,4,5],[0,1,2,3,5,4],[2,1,0,3,4,5],[5,4,3,2,1,0]])
