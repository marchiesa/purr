from scssolver import SCSSolver
from dpscs import DPSCS
import math

### This class implements GREEDY based on the DPSCS solver

class GreedySCS(SCSSolver):

    def __init__(self, d):
        self.d=d
        self.dp_scs=DPSCS()

    def compute(self, frr_sequences,simplify=True):
        scs=[]
        # iterate over batches of 'self.d' sequences
        for i in range(0,int(math.ceil((len(frr_sequences)+0.0)/self.d))):
            if i %10 == 0:
                #print i
                pass
            # compute the SCS of the current scs with the next self.d sequences
            scs=self.dp_scs.compute([scs]+frr_sequences[i*self.d:(i+1)*self.d])

            # compress the scs sequence to remove elements that are not needed
            if simplify:
                scs=self.simplify(frr_sequences[:(i+1)*self.d],scs)

        return scs


if __name__ == "__main__":
    a=GreedySCS(5)
    a.compute([["b","a"],["a","b"],["a","a"],["a","b"],["a","a"],["a","b"],["a","a"]])
