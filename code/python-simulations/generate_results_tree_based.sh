#!/bin/bash

# generate all the configuration files for running the experiments. All the *.json files contained in the experiments/ folder will be executed

cd experiments
./generate-experiments_tree_based.sh
cd ..
# evaluated fast greedy
./evaluate.sh scs
rm experiments/*json

