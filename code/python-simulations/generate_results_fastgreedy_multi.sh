#!/bin/bash

# generate all the configuration files for running the experiments. All the *.json files contained in the experiments/ folder will be executed
cd experiments-multi
./generate-experiments.sh
cd ..
# evaluate LCS solution
./evaluate-multi.sh lcs
#evaluate FastGreedy solution
./evaluate-multi.sh scs
rm experiments-multi/*json

