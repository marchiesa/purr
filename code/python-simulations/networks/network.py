
import random
from sets import Set
import sys

class Network:
    nodes = {}
    connectivity=0
    size=0
    #trees={}

    node_2_link={}

    def __init__(self,connectivity,size):
        self.connectivity=connectivity
        self.size=size
        self.destination_2_trees = {}
        for x in range(0,self.connectivity):
            self.destination_2_trees[x]=[]
        self.links_2_parent = {}
        self.old_generate_network_with_trees(self.connectivity,self.size,0) ## random seed is static

    def __str__(self):
        string =""
        string += str(self.nodes)
        return string


    def generate_network_with_trees(self,connectivity,size,seed=None):
        random.seed(seed)
        link_index=0
        print "size: " + str(size)

        for x in range(1,size):
            if(x==1):
                self.nodes[0] = {}
                self.nodes[1] = {}

                for tree_index in range(0,self.connectivity):
                    self.nodes[0][link_index] = (link_index, 0, 1, -1, tree_index)
                    self.nodes[1][link_index] = (link_index, 1, 0, tree_index, -1)
                    link_index += 1
                    #self.nodes[0][link_index] = (x, x, -1)
                    #link_index += 1

                for destination in range(0,self.connectivity):
                    for link in self.nodes:

                        self.links_2_parent[link[1]]

            else:
                print "here"
                random_links=[]

                for _ in range(0,self.connectivity/2):
                    random_node_index = int(len(self.nodes.keys()) * random.random())
                    print "random_node_index :" + str(random_node_index )
                    selected_random_node = self.nodes[random_node_index]
                    print "selected_random_node:" + str(selected_random_node)
                    random_link_key_index = int(len(selected_random_node.keys()) * random.random())
                    print "random_link_key_index: " + str(random_link_key_index)
                    random_link_index= selected_random_node.keys()[random_link_key_index]
                    print "random_link_index: " + str(random_link_index)
                    selected_random_link = selected_random_node[random_link_index]
                    random_links.append(selected_random_link)
                    print "random_links: " + str(random_links)

                self.nodes[x] = {}
                new_links=[]
                print "new node: " + str(x)
                for (old_link_index,end_point_1,end_point_2,tree_1,tree_2) in random_links:

                    # remove old link-pair
                    print "old-link:" + str((old_link_index,end_point_1,end_point_2,tree_1,tree_2))
                    del self.nodes[end_point_1][old_link_index]
                    del self.nodes[end_point_2][old_link_index]

                    # add first new set of links
                    self.nodes[x][link_index] = (link_index, x, end_point_1, tree_1, tree_2)
                    self.nodes[end_point_1][link_index] = (link_index, end_point_1, x, tree_1, tree_2)
                    print "new link: " + str((link_index, x, end_point_1, tree_2, tree_1))
                    print "new link: " + str((link_index, end_point_1, x, tree_1, tree_2))
                    link_index += 1
                    new_links.append((link_index, x, end_point_1, tree_2, tree_1))

                    #add second new set of links
                    self.nodes[x][link_index] = (link_index, x, end_point_2, tree_1, tree_2)
                    self.nodes[end_point_2][link_index] = (link_index, end_point_2, x, tree_2, tree_1)
                    print "new link: " + str((link_index, x, end_point_2, tree_1, tree_2))
                    print "new link: " + str((link_index, end_point_2, x, tree_2, tree_1))
                    new_links.append((link_index, x, end_point_2, tree_1, tree_2))
                    link_index += 1

                pretty(self.nodes)

                trees = {}
                removable_links_from_trees = []
                missing_trees=Set()
                for i in range(0, connectivity):
                    trees[i] = 0
                    missing_trees.add(i)
                for (link_index,end_point_1,end_point_2,tree_1,tree_2) in self.nodes[x].values():
                    if tree_1 == -1:
                        continue
                    trees[tree_1] += 1
                    if trees[tree_1] > 1:
                        removable_links_from_trees.append((link_index,end_point_1,end_point_2,tree_1,tree_2))
                    if tree_1 in missing_trees:
                        missing_trees.remove(tree_1)
                print "missing trees: " + str(missing_trees)

                for i in missing_trees:
                    (link_index, end_point_1, end_point_2, tree_1, tree_2) =  removable_links_from_trees.pop()
                    print "removable link: " + str((link_index, end_point_1, end_point_2, tree_1, tree_2))
                    new_link = (link_index,end_point_1,end_point_2,i,tree_2)
                    self.nodes[end_point_1][link_index]= new_link



                pretty(self.nodes)



    def old_generate_network_with_trees(self,connectivity,size,seed=None):
        random.seed(seed)
        link_index=0
        print "size: " + str(size)

        for x in range(1,size):
            if(x==1):
                self.nodes[0] = {}
                self.nodes[1] = {}
                for tree_index in range(0,self.connectivity):
                    self.nodes[0][link_index] = (link_index, 0, 1, -1, tree_index)
                    self.nodes[1][link_index] = (link_index, 1, 0, tree_index, -1)
                    link_index += 1
                    #self.nodes[0][link_index] = (x, x, -1)
                    #link_index += 1
            else:
                print "here"
                random_links=[]

                for _ in range(0,self.connectivity/2):
                    random_node_index = int(len(self.nodes.keys()) * random.random())
                    print "random_node_index :" + str(random_node_index )
                    selected_random_node = self.nodes[random_node_index]
                    print "selected_random_node:" + str(selected_random_node)
                    random_link_key_index = int(len(selected_random_node.keys()) * random.random())
                    print "random_link_key_index: " + str(random_link_key_index)
                    random_link_index= selected_random_node.keys()[random_link_key_index]
                    print "random_link_index: " + str(random_link_index)
                    selected_random_link = selected_random_node[random_link_index]
                    random_links.append(selected_random_link)
                    print "random_links: " + str(random_links)

                self.nodes[x] = {}
                new_links=[]
                print "new node: " + str(x)
                for (old_link_index,end_point_1,end_point_2,tree_1,tree_2) in random_links:

                    # remove old link-pair
                    print "old-link:" + str((old_link_index,end_point_1,end_point_2,tree_1,tree_2))
                    del self.nodes[end_point_1][old_link_index]
                    del self.nodes[end_point_2][old_link_index]

                    # add first new set of links
                    self.nodes[x][link_index] = (link_index, x, end_point_1, tree_1, tree_2)
                    self.nodes[end_point_1][link_index] = (link_index, end_point_1, x, tree_1, tree_2)
                    print "new link: " + str((link_index, x, end_point_1, tree_2, tree_1))
                    print "new link: " + str((link_index, end_point_1, x, tree_1, tree_2))
                    link_index += 1
                    new_links.append((link_index, x, end_point_1, tree_2, tree_1))

                    #add second new set of links
                    self.nodes[x][link_index] = (link_index, x, end_point_2, tree_1, tree_2)
                    self.nodes[end_point_2][link_index] = (link_index, end_point_2, x, tree_2, tree_1)
                    print "new link: " + str((link_index, x, end_point_2, tree_1, tree_2))
                    print "new link: " + str((link_index, end_point_2, x, tree_2, tree_1))
                    new_links.append((link_index, x, end_point_2, tree_1, tree_2))
                    link_index += 1

                pretty(self.nodes)

                trees = {}
                removable_links_from_trees = []
                missing_trees=Set()
                for i in range(0, connectivity):
                    trees[i] = 0
                    missing_trees.add(i)
                for (link_index,end_point_1,end_point_2,tree_1,tree_2) in self.nodes[x].values():
                    if tree_1 == -1:
                        continue
                    trees[tree_1] += 1
                    if trees[tree_1] > 1:
                        removable_links_from_trees.append((link_index,end_point_1,end_point_2,tree_1,tree_2))
                    if tree_1 in missing_trees:
                        missing_trees.remove(tree_1)
                print "missing trees: " + str(missing_trees)

                for i in missing_trees:
                    (link_index, end_point_1, end_point_2, tree_1, tree_2) =  removable_links_from_trees.pop()
                    print "removable link: " + str((link_index, end_point_1, end_point_2, tree_1, tree_2))
                    new_link = (link_index,end_point_1,end_point_2,i,tree_2)
                    self.nodes[end_point_1][link_index]= new_link



                pretty(self.nodes)



def pretty(d, indent=0):
    for key, value in d.items():
        print('\t' * indent + str(key))
        if isinstance(value, dict):
            pretty(value, indent+1)
        else:
            print('\t' * (indent+1) + str(value))




def main():
    network = Network(int(sys.argv[1]),int(sys.argv[2]))

if __name__ == "__main__":
    main()
