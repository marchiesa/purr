
import random
from sets import Set
import sys

### This class generates a JellyFish topology with arc-disjoint arborescences.

class Network:
    nodes = {}
    connectivity=0
    size=0

    node_2_link={}
    pinched_links_history = []
    link_2_nodes = {}
    link_2_trees = {} # contains a map node_id -> tree_id. It means the tree outgoing at node_is is tree_id
    link_2_parent = {}
    link_2_children= {}
    pinched_links_history = {}
    seed=0

    def __init__(self,connectivity,size,seed=None):

        self.seed=seed
        if not seed == None:
            random.seed(seed)
        self.connectivity=connectivity
        self.size=size
        self.destination_2_trees = {}
        for x in range(0,self.connectivity):
            self.destination_2_trees[x]=[]
        self.generate_network_with_trees(self.connectivity,self.size) ## random seed is static



    def __str__(self):
        string =""
        string += str(self.nodes)
        return string


    def compute_trees(self):
        self.compute_tree(3)

    def compute_tree(self,root):
        print "create subnetwork"
        subnetwork = Network(self.connectivity,root+1)
        print "hey"
        node_2_destinations={root:[x for x in range(self.connectivity)]}
        for x in range(root,-1,-1):
            for destination in node_2_destinations[x]:
                link_index = self.find_link_with_no_tree(subnetwork,x)
                if link_index == -1:
                    print "*********** ALERT *********** no link found, it should not happen"
                subnetwork.nodes[x][link_index][4]=destination
                other_endpoint = subnetwork.nodes[x][link_index][2]
                subnetwork.nodes[other_endpoint][link_index][3] = destination
                trees = subnetwork.link_2_trees[link_index]
                trees[other_endpoint] = destination
                if other_endpoint not in node_2_destinations:
                    node_2_destinations[other_endpoint] = []
                node_2_destinations[other_endpoint].append(destination)

            # compress the topology
            print subnetwork.pinched_links_history[x]




    def find_link_with_no_tree(self,network,node):
        print "search node: "  +str(node)
        for link_id,[_,_,_,_,tree_id] in network.nodes[node].items():
            print "  link : " + str(link_id) + " tree_id:" + str(tree_id)
            if tree_id == -1:
                return link_id
        return -1



    def generate_network_with_trees(self,connectivity,size):
        link_index=0

        for x in range(1,size):
            if (x % 1000 == 0):
                print "x: " + str(x)
            if(x==1):
                self.nodes[0] = {}
                self.nodes[1] = {}

                for tree_index in range(0,self.connectivity):
                    self.nodes[0][link_index] = [link_index, 0, 1]
                    self.nodes[1][link_index] = [link_index, 0, 1]
                    self.link_2_nodes[link_index] = {0:0, 1:1}
                    self.link_2_trees[link_index] = {0:-1,1:tree_index}
                    link_index += 1


            else:
                random_links=[]

                while len(random_links) < self.connectivity/2:
                    random_node_index = int(len(self.nodes.keys()) * random.random())
                    selected_random_node = self.nodes[random_node_index]
                    random_link_key_index = int(len(selected_random_node.keys()) * random.random())
                    random_link_index= selected_random_node.keys()[random_link_key_index]
                    selected_random_link = selected_random_node[random_link_index]
                    if selected_random_link not in random_links:
                        random_links.append(selected_random_link)

                self.pinched_links_history[x] = random_links
                self.nodes[x] = {}
                new_links=[]
                for [old_link_index,end_point_1,end_point_2] in random_links:

                    # remove old link-pair
                    del self.nodes[end_point_1][old_link_index]
                    del self.nodes[end_point_2][old_link_index]


                    # add first new set of links
                    self.link_2_parent[link_index]=old_link_index
                    self.link_2_nodes[link_index] = {0:end_point_1, 1:x}
                    self.nodes[x][link_index] = [link_index, end_point_1, x]
                    self.nodes[end_point_1][link_index] = [link_index, end_point_1, x]
                    self.link_2_trees[link_index] = {0: self.link_2_trees[old_link_index][0],
                                                     1: self.link_2_trees[old_link_index][1]}
                    link_index += 1
                    new_links.append([link_index, end_point_1, x])

                    #add second new set of links
                    self.link_2_parent[link_index] = old_link_index
                    self.link_2_nodes[link_index] = {0:end_point_2, 1:x}
                    self.nodes[x][link_index] = [link_index, end_point_2, x]
                    self.nodes[end_point_2][link_index] = [link_index, end_point_2, x]
                    self.link_2_trees[link_index] = {0: self.link_2_trees[old_link_index][1],
                                                     1: self.link_2_trees[old_link_index][0]}
                    new_links.append([link_index, end_point_2, x])
                    link_index += 1

                    self.link_2_children[old_link_index] = [link_index-2,link_index-1]



                #remove cycles
                for link in self.nodes[x]:
                    if self.check_loop(x, link):
                        self.setOutgoingTreeId(x, link, -1)

                #remove duplicates
                found_trees=[0]*self.connectivity
                unassigned_links=[]
                for link in self.nodes[x]:
                    link_tree_id = self.getOutgoingTreeId(x,link)
                    if link_tree_id != -1:
                        if found_trees[link_tree_id] == 1:
                            self.setOutgoingTreeId(x,link,-1)
                            unassigned_links.append(link)
                        else:
                            found_trees[link_tree_id]=1
                    else:
                        unassigned_links.append(link)

                #add missing trees
                for tree_index in range(0,self.connectivity):
                    if found_trees[tree_index] == 0:
                        link = unassigned_links.pop()
                        self.setOutgoingTreeId(x,link,tree_index)


    def check_loop(self, node, link):
        visited_nodes=[node]
        to_visit=[self.getOtherEndLink(node,link)]
        tree = self.getOutgoingTreeId(node,link)
        while len(to_visit) >0:
            to_analyze = to_visit.pop()
            if not to_analyze in visited_nodes:
                for link2 in self.nodes[to_analyze]:
                    if self.link_2_trees[link2] == tree:
                        to_visit.append(self.getOtherEndLink(to_analyze,link2))
                visited_nodes.append(to_analyze)
            else:
                return True
        return False

    def setOutgoingTreeId(self,node_id,link_id, tree_id):
        if self.link_2_nodes[link_id][0] == node_id:
            self.link_2_trees[link_id][0]=tree_id
        else:
            self.link_2_trees[link_id][1] = tree_id

    def getOutgoingTreeId(self,node_id,link_id):
        if self.link_2_nodes[link_id][0] == node_id:
            return self.link_2_trees[link_id][0]
        else:
            return self.link_2_trees[link_id][1]

    def getOtherEndLink(self,node_id,link_id):
        if self.link_2_nodes[link_id][0] == node_id:
            return self.link_2_nodes[link_id][1]
        else:
            return self.link_2_nodes[link_id][0]

    def getOutgoinTreeIds(self,node_id):
        ids=[0]*len(self.nodes[node_id])
        i=0
        for link in self.nodes[node_id]:
            ids[i]=self.getOutgoingTreeId(node_id,link)
            i+=1
        return ids

def pretty(d, indent=0):
    for key, value in d.items():
        print('\t' * indent + str(key))
        if isinstance(value, dict):
            pretty(value, indent+1)
        else:
            print('\t' * (indent+1) + str(value))




def main():
    network = Network(int(sys.argv[1]),int(sys.argv[2]),0)
    print network.link_2_trees


    print ""
    for node in network.nodes:
        print str(node) + " : " + str(network.getOutgoinTreeIds(node))
    print ""

    print "*********************"

if __name__ == "__main__":
    main()
