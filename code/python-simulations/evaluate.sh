#!/bin/bash
# extract all the files in the 'experiments' folder
# note that michaelscs is the FASTGREEDY heuritics
# TODO: change 'michaelscs' into 'fastgreedy'
for exp_file in `find experiments -name '*json'  |  grep $1 |  awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);value=$4*$5;$2=value"-"$2;print $0}' | sed "s/--//g" | sort -t$'-' -k2 -n | awk -v FS="-" -v OFS="-" '{gsub(/[0-9]*/,"",$2);gsub("michaelscs","michael-scs");print $0}' | sed "s/--/-/g"`; do
  # exp_file contains the experiment setting
  echo "exp_file: " $exp_file
  results_file=`echo $exp_file | sed "s/json/txt/" | sed "s#experiments/#results/results-#"` 
  # the experiments will be stored in results_file
  echo "results_file: " $results_file
  destination=`echo $results_file | sed "s#results/#results/failed/#"`
  #extract the number of FRR sequences
  num_seq=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $5}'`
  #extract the size of the FRR sequences
  siz_seq=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $6}'`

  ###################################################################################################
  ##### we now check if we already performed this experiment. This allows us to save some time. #####
  check=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);gsub("michaelscs","michael-scs");print $0}' | sed "s/--//g" | sed "s#results/failed/##"`
  already_done=`find results/failed/ -name '*' | grep $check | wc -l`
  analyze=1
  if [ "$already_done" -gt "0" ];then
    analyze=0
    #echo "finish"
    touch $destination
    continue
  fi

  check=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"0",$4);gsub(/.*/,"[0-9]*",$5);gsub("michaelscs","michael-scs");print $0}' | sed "s/--//g" | sed "s#results/failed/##"`
  for file in `find results/failed/ -name '*' | grep $check`; do
    echo "file: " $file
    num_seq_2=`echo $file | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $5}'`
    siz_seq_2=`echo $file | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $6}'`
    
    if [ "$num_seq" -gt "$num_seq_2" ]; then
      analyze=0
      break
    fi
    if [ "$siz_seq" -gt "$siz_seq_2" ]; then
      analyze=0
      break
    fi
  done
  if [ "$analyze" -eq "0" ]; then
    #echo "finish"
    touch $destination
    continue
  fi 
  check=`echo $destination | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"0",$4);gsub(/.*/,"[0-9]*",$6);gsub("michaelscs","michael-scs");print $0}' | sed "s/--//g" | sed "s#results/failed/##"`
  for file in `find results/failed/ -name '*' | grep $check`; do
    echo "file: " $file
    num_seq_2=`echo $file | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $5}'`
    siz_seq_2=`echo $file | awk -v FS="-" -v OFS="-" '{gsub("michael-scs","michaelscs",$0);gsub(/.*/,"[0-9]*",$4);print $6}'`
    #echo "num_seq_2: " $num_seq_2
    #echo "siz_seq_2: " $siz_seq_2

    if [ "$num_seq" -gt "$num_seq_2" ]; then
      analyze=0
      break
    fi
    if [ "$siz_seq" -gt "$siz_seq_2" ]; then
      analyze=0
      break
    fi
  done 
  if [ "$analyze" -eq "0" ]; then
    #echo "finish"
    touch $destination
    continue
  fi
  ######################################## end of the check  ########################################
  ###################################################################################################

  # we are now ready to run the experiment
  if [ ! -f $results_file ]; then
    if [ ! -f $destination ]; then
      echo "executed " $results_file
      echo "python data/evaluate_random_sequences.py $exp_file"
      python data/evaluate_random_sequences.py $exp_file
      size=`cat $results_file | wc -l`
      echo $size
      # if the run produces some results, we store them in the results/ folder
      if [ "$size" -eq "0" ]; then
        rm $results_file
      fi
    fi
  fi 
done
