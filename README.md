# PURR: A Primitive for Reconfigurable Fast Reroute (code and replication instructions)

In the following, `$PURR_HOME` represents the folder where the project has been cloned.

In the paper, the entire code has been executed on `Linux 4.4.0-104-generic #127-Ubuntu SMPx86_64 GNU/Linux` using `Python 2.7.12` and `gcc (Ubuntu 5.4.0-6ubuntu1~16.04.10) 5.4.0 20160609`. We also tested the code on Ubuntu 18.04, which is the version of the VM provided in this repository. All the source code is commented.

Contact for repeatability questions: [Marco Chiesa](https://marchiesa.bitbucket.io).

## Code organization

The code is divided into three main parts: 
 - the code evaluating the FRR encoding (called `python-simulations`) and 
 - the code evaluating PURR against packet recirculation and control-plane reconvergence (called `ns3-simulations`).
 - the Tofino code of the PURR primitive.

We provide all the code and instructions needed to repeat the simulations and to plot the graphs in the paper. However, NS3 simulations are computationally expensive and requires a large degree of parallelization.

For this reason, we also provide both raw results that can be used to only plot the graphs without having to run the code. There are three main folder `code`, `raw-results`, and `graphs`, each of which containing one folder for `python-simulations` and one folder for `ns3-simulations`. The `code` folder contains all the code needed to run the simulations. The `raw-results` folder contains the aggregated results of our simulations as well as scripts to produce these aggregate results from custom simulations. The `graphs` folder contains scripts to plot the graphs of the paper (even without having to run the simulation code). 


## Provided Virtual Machine (VM)

We include also a Virtual Machine that can be used to check that the code runs and the plotting scripts work. Due to the performance slowdown of the VM, replicating NS3 results is practically infeasible. It is however possible to replot the graphs from the raw results provided in the repository. The VM is located in the `VM` folder and runs in VirtualBox 6.0. It already contains the git repository as well as all the dependencies pre-installed. Also, the NS3 simulations are already built in the VM for your convenience. 

Due to storage limitations, the VM can be downloaded from the KTH institutional Box at this [link](https://kth.box.com/s/24nxtf6lbrp2g88r4wmsqczc4sa2cjp5) (last updated 2019-10-15).
 
## FRR encoding replication

### Installing dependencies

We describe how to install missing dependencies on a vanilla Ubuntu 18.04 (download at `https://www.osboxes.org/ubuntu/#ubuntu-19-04-info`):

```
sudo apt update
sudo apt-get --yes install git python python-pip python-numpy 
sudo python -m pip install matplotlib==2.0.2
sudo apt-get --yes install texlive-latex-extra dvipng 
```

### Code organization

The code is organized as follows:

```
code
  ├── python-simulations
        ├── data                    # Sequence generators and evaluation files
        ├── experiments-multi       # Experiment settings files for multiple tables
        ├── experiments             # Experiment settings files 
        ├── networks                # Network model files
        ├── results-multi           # Output results folder for multiple tables
        ├── results                 # Output results folder
        └── scs                     # Heuristic files
```

### Run the code

Move to the main directory:

`cd $PURR_HOME/code/python-simulations`

##### Gap from optimality (Fig 5)

To replicate the results for the comparison among DPSCS, GREEDY, HIERARCHICAL, and FASTGREEDY (Fig.5), run the following command:

`./generate_results_comparison_dpscs.sh`

The expected running time is less than one hour. The results will be stored in the `results/` folder.

To modify the parameters of the experiment, access the `experiments/generate-experiments-dcscs.sh` file.

##### Heuristics large-scale (Fig.6 and Fig.7(a-b))

To replicate the results for the comparison among GREEDY, HIERARCHICAL, and FASTGREEDY (Fig.6 and Fig.7(a-b)), run the following command:

`./generate_results_heuristics_experiment.sh`

The expected running time is roughly one hour. The results will be stored in the `results/` folder.

To modify the parameters of the experiment, access the `experiments/generate-experiments.sh` file.

##### Tree-based vs random sequences (Fig.7 (c))

To replicate the results for the comparison among random sequences and tree-based sequences (Fig.7(c)), run the following command:

`./generate_results_tree_based.sh`

The expected running time is less than one hour. The results will be stored in the `results/` folder.

To modify the parameters of the experiment, access the `experiments/gener
ate-experiments_tree_based.sh` file.

##### Multi-table optimization (no figure)

To replicate the results for multi table compression (no figure), run the following command:

`./generate_results_fastgreedy_multi.sh` 

The expected running time is in the order of hours. The results will be stored in the `results-multi/` folder.

To modify the parameters of the experiment, access the `experiments-multi/generate-experiments` file.

### Access raw data results

We provide a convenient file containing the results for all the above commands (except multitable) in 

`$PURR_HOME/raw-results/python-simulations/results.txt`

### Plot the graphs

Move to the plotting folder:

`cd $PURR_HOME/graphs/python-simulations`

There is a convient script to plot all the results. It takes as input the folder where the results have been stored. 

You can choose whether to plot the graphs from the results computed above or the raw data provided with the repository.

Plotting from the self-computed results:

`./generate-all-plots.sh ../../code/python-simulations/results`

Plotting from the raw results:

`./generate-all-plots.sh ../../raw-results/python-simulations/results.txt`

Inside the `generate-all-plots.sh` file, one can find the individual commands used to produce all the figures with comments.

To produce an aggregated results file for the multi table experiment, run:

`./generate-results-multi.sh ../../code/python-simulations/results-multi`

## NS3 simulations

We note that running this code to reproduce the figures of the paper requires a large amount of parallel resources. We ran simulations for the equivalent of roughly 10000 hours (roughly one year of computing time) on a 2.6Ghz machine. 

### Installing dependencies (not needed in the VM environment)

See above dependencies for FRR encoding.

### Code organization

The code is organized as follows (before building NS3):

```
code/
  ├── ns3-simulations/
        ├── ns3-simulations-purr/                               # Code for simulations using PURR folder
              ├── examples/load-balance/                         
                    ├── conga-simulation-large.cc               # The main file where the topology/flows are created
              ├── src/internet/model/                           
                    ├── ipv4-l3-protocol.cc                     # PURR implementation inside ipv4 forwarding
        ├── diff-files/                                         
              ├── recirculation/                                # "Diff" from PURR for creating packet recirculation code
              ├── reconvergence-one-failure/                    # "Diff" from PURR for creating control plane reconvergence code
              ├── reconvergence-two-failures/                   # "Diff" from PURR for creating control plane reconvergence code
```

The code is organized as follows (after building in NS3):

```
code/
  ├── ns3-simulations/
        ├── ns3-simulations-purr/                               # Code for simulations using PURR folder
              ├── examples/load-balance/                         
                    ├── conga-simulation-large.cc               # The main file where the topology/flows are created
              ├── src/internet/model/                           
                    ├── ipv4-l3-protocol.cc                     # PURR implementation inside ipv4 forwarding
                    ├── marco-tag.cc                            # A tag needed in NS3 to carry metadata
        ├── recirculation/                                # "Diff" from PURR for creating packet recirculation code
              ├── examples/load-balance/                         
                    ├── conga-simulation-large.cc               # The main file where the topology/flows are created
              ├── src/internet/model/                           
                    ├── ipv4-l3-protocol.cc                     # Packet recirulation implementation inside ipv4 forwarding
                    ├── marco-tag.cc                            # A tag needed in NS3 to carry metadata
        ├── reconvergence-one-failure/                    # "Diff" from PURR for creating control plane reconvergence code
        ├── reconvergence-two-failures/                   # "Diff" from PURR for creating control plane reconvergence code
```

### Run the code

##### Compiling NS3 (not needed in the VM environment)

The first step is to compile NS3 (provided in the repo).

Move to the plotting folder:

`cd $PURR_HOME/code/ns3-simulations/`

We first build one copy of the NS3 working directory:

`./build.sh`

We now create three copies of the environment for the comparison methods and build them all (this part of the code could be optimized in the future so as to use a single working directory):

`./create-copies.sh`

We are now ready to run the NS3 simulations.

##### Configuring the simulation parameters

Configure the parameters of the simulation. We provide the original parameters in `large-scale-setting.txt` and a simple/faster setting in `simple-setting-*.txt`. 

Set `file=large-scale-setting.txt` or `file=simple-setting-datamining.txt` or `file=simple-setting-websearch.txt` or a personlized setting before moving one.

##### Data mining workload without failures (Fig. 9(a,b,c,d,e,f), light blue line)

`./generate_datamining_zero_failures.sh $file` 

Run the above command with ``file=simple-setting-datamining.txt` for testing purposes with a simple setting. 

##### Data mining workload with one/two failures, PURR (Fig. 9(a,c,e)/(b,d,f), dark blue line)

`./generate_datamining_one_failure_purr.sh $file`

`./generate_datamining_two_failures_purr.sh $file`

Run the above command with `file=simple-setting-datamining.txt` for testing purposes with a simple setting.

##### Data mining workload with one/two failures, reconvergence (Fig. 9(a,c,e)/(b,d,f), red line)

`./generate_datamining_one_failure_reconvergence.sh $file`

`./generate_datamining_two_failures_reconvergence.sh $file`

Run the above command with `file=simple-setting-datamining.txt` for testing purposes with a simple setting.

##### Data mining workload with one/two failures, recirculation (Fig. 9(a,c,e)/(b,d,f), green line)

`./generate_datamining_one_failure_recirculation.sh $file`

`./generate_datamining_two_failures_recirculation.sh $file`

Run the above command with `file=simple-setting-datamining.txt` for testing purposes with a simple setting.

##### Web search workload with one failure, PURR (Fig. 10(a)/(b), dashed line)

`./generate_websearch_one_failure_purr.sh $file`

`./generate_websearch_two_failures_purr.sh $file`

Run the above command with `file=simple-setting-websearch.txt` for testing purposes with a simple setting.

##### Web search workload with one failure, recirculation (Fig. 10(a)/(b), solid line)

`./generate_websearch_one_failure_recirculation.sh $file`

`./generate_websearch_two_failures_recirculation.sh $file`

Run the above command with `file=simple-setting-websearch.txt` for testing purposes with a simple setting.

### Access raw data results

We provide the aggregated raw results that can be used to plot the graphs. These are located in:

`$PURR_HOME/raw-results/ns3-simulations/results-datamining-raw.txt` for the data-mining workload

`$PURR_HOME/raw-results/ns3-simulations/results-websearch-raw.txt` for the web-search workload

We also stored the raw data at the per-flow granularity for all the simulations. This is based on the `large-scale-setting.txt` file. Due to the large amount of data (>100GB), we do not provide the dataset. Please directly contact the authors for this additional data.

##### Generating the aggregated results after running the simulations

Move to the raw-result folder:

`cd $PURR_HOME/raw-results/ns3-simulations/`

Run the following commands to aggregate the data from the simulation folder (you must have run the NS3 simulations first):

`./generate-results-websearch.sh $simulation_time $failure_time`

`./generate-results-datamining.sh $simulation_time $failure_time`

### Plot the graphs

Move to the plotting folder:

`cd $PURR_HOME/graphs/ns3-simulations/`

To plot all the graphs using the raw data, run:

`./generate-all-plots-raw.sh`

The script copies the data from the `$PURR_HOME/raw-results/ns3-simulations/` folder.

To plot all the graphs using the data computed from running the simulations, run:

`./generate-all-plots.sh`

## Tofino code

We tested the Tofino implementation of PURR on a `Tofino Edgecore Wedges 100 BF-32X` running `BF Studio 9.2.0` on top of `Linux 4.14.151-OpenNetworkLinux`.

### Code organization

The code consists of the following files:

```
code/
  ├── tofino/
        ├── purr_pipeline.p4                              # The P4 pipeline implementation of PURR
        ├── purr_setup.py                                 # A script to generate the entries in the PURR tables
        ├── purr.conf                                     # A configuration file containing the FRR sequences to be implemented
        ├── send.py                                       # A simple script to send a packet from a host (for test purposes)
```

### Preliminaries

On the switch, do not forget to:

 - load the drivers with: `sudo insmod $SDE/build/bf-drivers/kdrv/bf_kpkt/bf_kpkt.ko`

 - set the environment variables: `. ~/tools/set_sde.bash`


### Building and running the code

To build the P4 pipeline, run:

`~/tools/p4_build.sh ~/code/tofino/purr_pipeline.p4`

where we assume all the files have been moved in the root folder of the switch.

To run the P4 program, execute:

`$SDE/run_switchd.sh -p purr_pipeline`

On another terminal, always on the switch, populate the PURR tables with:

`$SDE/run_bfshell.sh -b code/tofino/purr_setup.py`

This will read the `purr.conf` file and generate the PURR entries specified in that file.

The `purr.conf` file consists of four main blocks:

 1. IPV4: a mapping from IPv4 destination prefixes to a FRR ID. Each packet destined to a certain IPv4 prefix will be forwarded according to its FRR sequence identified by the FRR id.
 2. PORTS `<number of ports>`: a single line with the set of port IDs. Check your Tofino port IDs.
 3. PORT SET: a mapping from each FRR id to the sequence of ports to be used for FRR.
 4. PORT SEQUENCE: the encoding of all the PORT SET entries into a single sequence. The sequence can be obtained by running the above Python simulations using any of the heuristics.

### Testing the program

To run a simple test of the program, connect ports 144, 60, 52, and 17 of the Tofino switch to some hosts or modify the `purr.conf` file to use different port numbers. 

Generate a packet from the host connected to port `60` using:

`python send.py` 

This command will generate a packet towards IP `192.168.2.1`, which is mapped to `FRR_id=2`. The sequence of ports to be used for `FRR_id=2` is `60 52 17 144`. The packet will then come back to the same host. `tcpdump` can be used to verify that packet has returned. 

Now, we fail a link by updating the register `port_status_reg` in the P4 pipeline from `1111` to `1101`, which denotes the second port in the `PORTS` list has failed. In our example, this is port `60`. To update the register value, on the `bfshell` run:

```
bfrt_python
bfrt.purr_pipeline.pipe.Ingress.port_status_reg.mod(register_index=0,f1=13)
exit
```

By repeating the above test, i.e., sending a packet, we can observe that the packet is now forwarded to port `52`. More tests can be run by failing more ports and sending packets to different IPv4 destination addresses. 

## Depth-First-Search in P4

See [here](code/p4-dfs/)
