#/bin/bash

echo "command: [directory] [failures] [runs] [workloads] [frractive] [mode] [link1] [link2] [failure_time]"

runs_start=1
runs=4
runs_stop=`echo $runs_start "+" $runs "- 1" | bc -l`
directory=$1
failures=$2
runs=$3
workloads=$4
frractive=$5
mode=$6
link1=$7
link2=$8
link3=$9
link4=${10}
failure_time=${11}
endtime=${12}
#endtime=0.1
obstime=$endtime
reconvergence=$endtime

rm -f temp2

if [ -z "$link1" ]
then
  link1="*:*"
  link2="*:*"
  link3="*:*"
  link4="*:*"
  failure_time=0.5
  #failure_time=0.02
fi

is_positive_int(){ [ "$directory" -ge 0 ] 2>/dev/null && echo YES || echo no; }
for workload in $workloads
do
for failure in $failures
	do
		echo "number of failures:" $failure " workload: " $workload 
		echo "load avgFCT largeFCT smallFCT 99-FCT 99-largeFCT 99-smallFCT 99.9-FCT 99.9-largeFCT 99.9-smallFCT numberFlows numberLargeFlows numberSmallFlows rxGBytes" >> temp2
 for x in 0.1 0.2 0.3 0.4 0.5 0.6 0.7
#for x in 0.1
  do
    sumavgFCT=0
    sumFCT99=0
    sumFCT999=0
    sumlargeFCT=0
    sumlarge99FCT=0
    sumlarge999FCT=0
    sumsmallFCT=0
    sumlargeRxBytes=0
    sumsmall99FCT=0
    sumsmall999FCT=0
    sumnumberFlows=0
    sumnumberLargeFlows=0
    sumnumberSmallFlows=0
    sumrxBytes=0
    counter=0
    counterLarge=0
    counterSmall=0
    for run in `seq $runs`
    #for run in 2 3 4
    do
      xmlFile=$directory/0-1-large-load-4X4-$x-DcTcp-ecmp-simulation-$run-b600-$failure-$endtime-$reconvergence-$failure_time-$frractive-$workload.xml
      #echo $xmlFile
      if [ ! -f "$xmlFile" ]
      then
	continue
      fi
      #echo python flowmon-parse-results-affected.py $directory/0-1-large-load-4X4-$x-DcTcp-ecmp-simulation-$run-b600-$failure-$endtime-$reconvergence-$failure_time-$frractive-$workload.xml $link1 $link2 $link1 $link2 $link1 $link2 $mode $failure_time $obstime
      python flowmon-parse-results-affected.py $directory/0-1-large-load-4X4-$x-DcTcp-ecmp-simulation-$run-b600-$failure-$endtime-$reconvergence-$failure_time-$frractive-$workload.xml $link1 $link2 $link3 $link4 $link1 $link2 $mode $failure_time $obstime | grep -A 15 "Number of flows" > temp
      #anyflow=`cat temp | grep "No flows" | wc -l`
      #if [ "$anyflow" -eq "1" ] ; then
           #continue
      #fi
      counter=$((counter+1))
      avgFCT=`cat temp | grep "^Avg FCT" | awk '{print $3}'` 
      FCT99=`cat temp | grep "^Flow 99-ile FCT" | awk '{print $4}'` 
      FCT999=`cat temp | grep "^Flow 99.9-ile FCT" | awk '{print $4}'` 
      largeFCT=`cat temp | grep "Large Flow Avg FCT" | awk '{print $5}'`
      large99FCT=`cat temp | grep "Large Flow 99-ile FCT" | awk '{print $5}'`
      large999FCT=`cat temp | grep "Large Flow 99.9-ile FCT" | awk '{print $5}'`
      smallFCT=`cat temp | grep "Small Flow Avg FCT" | awk '{print $5}'`
      small99FCT=`cat temp | grep "Small Flow 99-ile FCT" | awk '{print $5}'`
      small999FCT=`cat temp | grep "Small Flow 99.9-ile FCT" | awk '{print $5}'`
      numberFlows=`cat temp | grep "Number of f" | awk '{print $4}'`
      numberLargeFlows=`cat temp | grep "Number of l" | awk '{print $5}'`
      largeRxBytes=`cat temp | grep "Total Large RX Bytes" | awk '{print $5}'`
      numberSmallFlows=`cat temp | grep "Number of s" | awk '{print $5}'`
      rxBytes=`cat temp | grep "Total RX Bytes" | awk '{printf("%.8f", $4/1000000000)}'`
      if [[ "$numberSmallFlows" -eq 0 ]]
      then
        smallFCT=0
        small99FCT=0
        small999FCT=0
      fi
      if [[ "$numberLargeFlows" -eq 0 ]]
      then
        largeFCT=0
        large99FCT=0
        large999FCT=0
        largeRxBytes=0
      fi
      sumavgFCT=`echo $sumavgFCT + $avgFCT | bc -l`
      sumFCT99=`echo $sumFCT99 + $FCT99 | bc -l`
      sumFCT999=`echo $sumFCT999 + $FCT999 | bc -l`
      sumlargeFCT=`echo $sumlargeFCT + $largeFCT | bc -l`
      sumlarge99FCT=`echo $sumlarge99FCT + $large99FCT | bc -l`
      sumlarge999FCT=`echo $sumlarge999FCT + $large999FCT | bc -l`
      sumlargeRxBytes=`echo $sumlargeRxBytes + $largeRxBytes | bc -l`
      sumsmallFCT=`echo $sumsmallFCT + $smallFCT | bc -l`
      sumsmall99FCT=`echo $sumsmall99FCT + $small99FCT | bc -l`
      sumsmall999FCT=`echo $sumsmall999FCT + $small999FCT | bc -l`
      sumnumberFlows=`echo $sumnumberFlows + $numberFlows | bc -l`
      #echo $run " " $sumnumberFlows " " $numberFlows
      sumnumberLargeFlows=`echo $sumnumberLargeFlows + $numberLargeFlows | bc -l`
      sumnumberSmallFlows=`echo $sumnumberSmallFlows + $numberSmallFlows | bc -l`
      sumrxBytes=`echo $sumrxBytes + $rxBytes | bc -l`
      
      #echo $x " " $avgFCT " " $largeFCT " " $smallFCT " " $FCT99 " " $large99FCT " " $small99FCT " " $FCT999 " " $large999FCT " " $small999FCT " " $numberFlows " " $numberLargeFlows " " $numberSmallFlows " " $rxBytes " " $sumrxBytes " " $largeRxBytes " " $sumlargeRxBytes #>> temp2
      rm temp
    done
    if [ "$sumnumberFlows" -eq "0" ]
    then
      continue
    fi
    avgFCT=`echo $sumavgFCT/$counter | bc -l`
    FCT99=`echo $sumFCT99/$counter | bc -l`
    FCT999=`echo $sumFCT999/$counter | bc -l`
    largeFCT=`echo $sumlargeFCT/$counter | bc -l`
    large99FCT=`echo $sumlarge99FCT/$counter | bc -l`
    large999FCT=`echo $sumlarge999FCT/$counter | bc -l`
    largeRxBytes=`echo $sumlargeRxBytes/$counter | bc -l`
    smallFCT=`echo $sumsmallFCT/$counter | bc -l`
    small99FCT=`echo $sumsmall99FCT/$counter | bc -l`
    small999FCT=`echo $sumsmall999FCT/$counter | bc -l`
    numberFlows=`echo $sumnumberFlows/$counter | bc -l`
    numberLargeFlows=`echo $sumnumberLargeFlows/$counter | bc -l`
    numberSmallFlows=`echo $sumnumberSmallFlows/$counter | bc -l`
    rxBytes=`echo $sumrxBytes/$counter | bc -l`
    echo $x " " $avgFCT " " $largeFCT " " $smallFCT " " $FCT99 " " $large99FCT " " $small99FCT " " $FCT999 " " $large999FCT " " $small999FCT " " $numberFlows " " $numberLargeFlows " " $numberSmallFlows " " $rxBytes " " $largeRxBytes >> temp2
    #cat temp2
  done 
  cat temp2 |  awk '{if(NR>1){printf("%.1f ",$1);for(i=2;i<=NF;i++){printf("%.7f  ",$i)};print ""}else{print $0}}' | awk '{print "- " $0}' | rev | column -t | rev
  echo ""
  rm temp2
done
done
