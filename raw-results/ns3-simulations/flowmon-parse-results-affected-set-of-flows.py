from __future__ import division
import sys
import os
try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    from xml.etree import ElementTree

def parse_time_ns(tm):
    if tm.endswith('ns'):
        return long(tm[:-4])
    raise ValueError(tm)

class FiveTuple(object):
    __slots__ = ['sourceAddress', 'destinationAddress', 'protocol', 'sourcePort', 'destinationPort']
    def __init__(self, el):
        if el !=  None:
            self.sourceAddress = el.get('sourceAddress')
            self.destinationAddress = el.get('destinationAddress')
            self.sourcePort = int(el.get('sourcePort'))
            self.destinationPort = int(el.get('destinationPort'))
            self.protocol = int(el.get('protocol'))
    def SetTuple(self, sourceAddress, destinationAddress, protocol, sourcePort, destinationPort):
        self.sourceAddress = sourceAddress
        self.destinationAddress = destinationAddress
        self.sourcePort = sourcePort
        self.destinationPort = destinationPort
        self.protocol = protocol
 
    def __eq__(self, other): 
        if not isinstance(other, FiveTuple):
            # don't attempt to compare against unrelated types
            return NotImplemented
        return self.sourceAddress == other.sourceAddress and self.destinationAddress == other.destinationAddress and  self.sourcePort == other.sourcePort and self.destinationPort == other.destinationPort and self.protocol == other.protocol
    
    def __str__(self):
        return "(" + str(self.sourceAddress) + " - " + str(self.destinationAddress) + " -  " + str(self.sourcePort) + " -  " + str(self.destinationPort) + " - " + str(self.protocol) + ")"

    def __hash__(self):
        return self.sourceAddress.__hash__() + self.destinationAddress.__hash__() + self.sourcePort.__hash__() + self.destinationPort.__hash__() + self.protocol.__hash__()

class Histogram(object):
    __slots__ = 'bins', 'nbins', 'number_of_flows'
    def __init__(self, el=None):
        self.bins = []
        if el is not None:
            #self.nbins = int(el.get('nBins'))
            for bin in el.findall('bin'):
                self.bins.append( (float(bin.get("start")), float(bin.get("width")), int(bin.get("count"))) )

class Flow(object):
    __slots__ = ['flowId', 'delayMean', 'packetLossRatio', 'rxBitrate', 'txBitrate',
            'fiveTuple', 'packetSizeMean', 'probe_stats_unsorted',
                 'hopCount', 'flowInterruptionsHistogram', 'rx_duration',
                 'fct', 'txBytes', 'txPackets', 'rxPackets', 'rxBytes', 'lostPackets', 'nodes', 'paths', 'firstTxPacket', 'lastRxPacket' ]
    def __init__(self, flow_el):
        self.flowId = int(flow_el.get('flowId'))
        rxPackets = long(flow_el.get('rxPackets'))
        txPackets = long(flow_el.get('txPackets'))
        tx_duration = float(long(flow_el.get('timeLastTxPacket')[:-4]) - long(flow_el.get('timeFirstTxPacket')[:-4]))*1e-9
        rx_duration = float(long(flow_el.get('timeLastRxPacket')[:-4]) - long(flow_el.get('timeFirstRxPacket')[:-4]))*1e-9
        fct = float(long(flow_el.get('timeLastRxPacket')[:-4]) - long(flow_el.get('timeFirstTxPacket')[:-4]))*1e-9
        txBytes = long(flow_el.get('txBytes'))
        rxBytes = long(flow_el.get('rxBytes'))
        self.txBytes = txBytes
        self.txPackets = txPackets
        self.rxBytes = rxBytes
        self.rxPackets = rxPackets
        self.rx_duration = rx_duration
        self.firstTxPacket = float(long(flow_el.get('timeFirstTxPacket')[:-4]))*1e-9
        self.lastRxPacket = float(long(flow_el.get('timeLastRxPacket')[:-4]))*1e-9
        if fct > 0:
            self.fct = fct
        else:
            self.fct = None
        self.probe_stats_unsorted = []
        if rxPackets:
            self.hopCount = float(flow_el.get('timesForwarded')) / rxPackets + 1
        else:
            self.hopCount = -1000
        if rxPackets:
            self.delayMean = float(flow_el.get('delaySum')[:-4]) / rxPackets * 1e-9
            self.packetSizeMean = float(flow_el.get('rxBytes')) / rxPackets
        else:
            self.delayMean = None
            self.packetSizeMean = None
        if rx_duration > 0:
            self.rxBitrate = long(flow_el.get('rxBytes'))*8 / rx_duration
        else:
            self.rxBitrate = None
        if tx_duration > 0:
            self.txBitrate = long(flow_el.get('txBytes'))*8 / tx_duration
        else:
            self.txBitrate = None
        lost = float(flow_el.get('lostPackets'))
        self.lostPackets = lost
        #print "rxBytes: %s; txPackets: %s; rxPackets: %s; lostPackets: %s" % (flow_el.get('rxBytes'), txPackets, rxPackets, lost)
        if rxPackets == 0:
            self.packetLossRatio = None
        else:
            self.packetLossRatio = (lost / (rxPackets + lost))

        interrupt_hist_elem = flow_el.find("flowInterruptionsHistogram")
        if interrupt_hist_elem is None:
            self.flowInterruptionsHistogram = None
        else:
            self.flowInterruptionsHistogram = Histogram(interrupt_hist_elem)

        list_nodes = flow_el.findall("node")
	self.nodes = [0] * len(list_nodes)
	for node_el in list_nodes:
            self.nodes[int(node_el.get('time'))] = int(node_el.get('id')) 

        list_paths = flow_el.findall("path")
	self.paths = [0] * len(list_paths)
        for path_el in list_paths:
            self.paths[int(path_el.get('time'))] = int(path_el.get('port'))


class ProbeFlowStats(object):
    __slots__ = ['probeId', 'packets', 'bytes', 'delayFromFirstProbe']

class Simulation(object):
    def __init__(self, simulation_el):
        self.flows = []
        FlowClassifier_el, = simulation_el.findall("Ipv4FlowClassifier")
        self.flow_map = {}
        for flow_el in simulation_el.findall("FlowStats/Flow"):
            flow = Flow(flow_el)
            self.flow_map[flow.flowId] = flow
            self.flows.append(flow)
        for flow_cls in FlowClassifier_el.findall("Flow"):
            flowId = int(flow_cls.get('flowId'))
            self.flow_map[flowId].fiveTuple = FiveTuple(flow_cls)

        for probe_elem in simulation_el.findall("FlowProbes/FlowProbe"):
            probeId = int(probe_elem.get('index'))
            for stats in probe_elem.findall("FlowStats"):
                flowId = int(stats.get('flowId'))
                s = ProbeFlowStats()
                s.packets = int(stats.get('packets'))
                s.bytes = long(stats.get('bytes'))
                s.probeId = probeId
                if s.packets > 0:
                    s.delayFromFirstProbe =  parse_time_ns(stats.get('delayFromFirstProbeSum')) / float(s.packets)
                else:
                    s.delayFromFirstProbe = 0
                self.flow_map[flowId].probe_stats_unsorted.append(s)

def main(argv):
    file_obj = open(argv[1])
    (f_node_1,f_interface_1) = argv[2].split(":")
    (f_node_2,f_interface_2) = argv[3].split(":")
    (f_node_3,f_interface_3) = argv[4].split(":")
    (f_node_4,f_interface_4) = argv[5].split(":")
    (f_node_5,f_interface_5) = argv[6].split(":")
    (f_node_6,f_interface_6) = argv[7].split(":")
    mode=argv[8] #available modes = all, select- filter
    start_time = float(argv[9])
    end_obs_time = 0
    if argv[10] == "1e-05":
        end_obs_time = 0.00001
    elif argv[10] == "1e-07":
        end_obs_time = 0.0000001
    else:
    	end_obs_time = float(argv[10])

    print "Reading XML file ",

    sys.stdout.flush()
    level = 0
    sim_list = []
    for event, elem in ElementTree.iterparse(file_obj, events=("start", "end")):
        if event == "start":
            level += 1
        if event == "end":
            level -= 1
            if level == 0 and elem.tag == 'FlowMonitor':
                sim = Simulation(elem)
                sim_list.append(sim)
                elem.clear() # won't need this any more
                sys.stdout.write(".")
                sys.stdout.flush()
    print " done."

    total_fct = 0
    flow_count = 0
    large_flow_total_fct = 0
    large_flow_count = 0
    large_flow_total_rxBytes = 0 
    small_flow_total_fct = 0
    small_flow_count = 0

    total_lost_packets = 0
    total_packets = 0
    total_rx_packets = 0
    total_rx_bytes = 0
    FCTs = []
    smallFCTs = []
    largeFCTs = []

    max_small_flow_id = 0
    max_small_flow_fct = 0

    fiveTupleMap={}

    for sim in sim_list:
        for flow in sim.flows:
            #print "flow-id: " + str(flow.flowId) + " header: " + str(sim.flow_map[flow.flowId].fiveTuple)
	    to_continue = False
            flowTuple = sim.flow_map[flow.flowId].fiveTuple
            reversedTuple = FiveTuple(None)
            reversedTuple.SetTuple(flowTuple.destinationAddress,flowTuple.sourceAddress,flowTuple.protocol,flowTuple.destinationPort,flowTuple.sourcePort) 
            if flow.fct == None or flow.txBitrate == None or flow.rxBitrate == None:
                #print "hello1 " + str(reversedTuple)
                if reversedTuple in fiveTupleMap:
                    #print "removing " +str(reversedTuple)
                    del fiveTupleMap[reversedTuple]
                to_continue = True
                continue
            if flow.txBytes == 52 * flow.txPackets + 4:
                to_continue = True
                #print "hello2 " + str(reversedTuple.__hash__()) + " flowmap:" + str(fiveTupleMap.keys()[0]) 
                if reversedTuple in fiveTupleMap:
                    #print "hoy"
                    originalFlow = fiveTupleMap[reversedTuple]
                    total_fct += originalFlow.fct
                    FCTs.append(originalFlow.fct)
	            total_packets += originalFlow.txPackets 
                    total_rx_packets += originalFlow.rxPackets
                    total_rx_bytes += originalFlow.rxBytes
	            total_lost_packets += originalFlow.lostPackets
                    if originalFlow.txBytes > 10000000:
                        large_flow_count += 1
                        large_flow_total_fct += originalFlow.fct
                        large_flow_total_rxBytes += originalFlow.rxBytes
                        largeFCTs.append(originalFlow.fct)
                    if originalFlow.txBytes < 100000:
                        small_flow_count += 1
                        small_flow_total_fct += originalFlow.fct
                        smallFCTs.append(originalFlow.fct)
                        if originalFlow.fct > max_small_flow_fct:
                            max_small_flow_id = originalFlow.flowId
                            max_small_flow_fct = originalFlow.fct
                    t = originalFlow.fiveTuple
                    proto = {6: 'TCP', 17: 'UDP'} [t.protocol]
                    print "FlowID: %i (%s %s/%s --> %s/%i)" % \
                        (originalFlow.flowId, proto, t.sourceAddress, t.sourcePort, t.destinationAddress, t.destinationPort)
                    print "\tTX bitrate: %.2f kbit/s" % (originalFlow.txBitrate*1e-3,)
                    print "\tRX bitrate: %.2f kbit/s" % (originalFlow.rxBitrate*1e-3,)
                    print "\tMean Delay: %.2f ms" % (originalFlow.delayMean*1e3,)
                    #print "\tPacket Loss Ratio: %.2f %%" % (flow.packetLossRatio*100)
                    print "\tFlow size: %i bytes, %i packets" % (originalFlow.txBytes, originalFlow.txPackets)
                    print "\tRx %i bytes, %i packets" % (originalFlow.rxBytes, originalFlow.rxPackets)
                    print "\tDevice Lost %i packets" % (originalFlow.lostPackets)
                    print "\tReal Lost %i packets" % (originalFlow.txPackets - originalFlow.rxPackets)
                    print "\tFCT: %.4f" % (originalFlow.fct)

            to_continue_2 = False
            if mode != "all":
                if mode == "select":
                    modifier = False
                else:
                    modifier = True
                to_continue_2 = True != modifier
                dstAddress = sim.flow_map[flow.flowId].fiveTuple.destinationAddress
                dstAddress = dstAddress[:dstAddress.rfind(".")]
                #print str(dstAddress) + " 10.1.4" 
                if dstAddress == "10.1.4":
                    to_continue_2 = False != modifier
            if flow.rxPackets != flow.txPackets:
                #print "hey1"
                #to_continue = True
                pass
            if flow.firstTxPacket > end_obs_time:
                #print "hey2"
                to_continue = True
            #if flow.lastRxPacket >0.2:
                #to_continue = True
            if flow.lastRxPacket < start_time:
                #print "hey3"
                to_continue = True
            to_continue = to_continue or to_continue_2
	    if to_continue:
	        continue
            fiveTupleMap[sim.flow_map[flow.flowId].fiveTuple]=flow
            flow_count += 1
    FCTs=sorted(FCTs)
    largeFCTs=sorted(largeFCTs)
    smallFCTs=sorted(smallFCTs)

    print "Number of flows: " + str (flow_count)
    print "Number of large flows: " + str (large_flow_count)
    print "Number of small flows: " + str (small_flow_count)
    if flow_count == 0:
	print "No  flows"
    else:
        print "Avg FCT: %.4f" % (total_fct / flow_count)
        print "Flow 99-ile FCT: %.4f" % (FCTs[int((len(FCTs)*99)/100)])
        print "Flow 99.9-ile FCT: %.4f" % (FCTs[int((len(FCTs)*999)/1000)])
    if large_flow_count == 0:
	print "No large flows"
    else:
        print "Large Flow Avg FCT: %.4f" % (large_flow_total_fct / large_flow_count)
        print "Large Flow 99-ile FCT: %.4f" % (largeFCTs[int((len(largeFCTs)*99)/100)])
        print "Large Flow 99.9-ile FCT: %.4f" % (largeFCTs[int((len(largeFCTs)*999)/1000)])
        print "Total Large RX Bytes: %.4f" % (large_flow_total_rxBytes)

    if small_flow_count == 0:
   	print "No small flows"
    else:
	print "Small Flow Avg FCT: %.4f" % (small_flow_total_fct / small_flow_count)
        print "Small Flow 99-ile FCT: %.4f" % (smallFCTs[int((len(smallFCTs)*99)/100)])
        print "Small Flow 99.9-ile FCT: %.4f" % (smallFCTs[int((len(smallFCTs)*999)/1000)])

    print "Total TX Packets: %i" % total_packets
    print "Total RX Packets: %i" % total_rx_packets
    print "Total RX Bytes: %i" % total_rx_bytes
    print "Total Lost Packets: %i" % total_lost_packets
    print "Max Small flow Id: %i" % max_small_flow_id

if __name__ == '__main__':
    main(sys.argv)
