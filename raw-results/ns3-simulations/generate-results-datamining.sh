#!/bin/bash

# the following scripts generates an aggregate of the results from the output of the simulations. 

simulation_time=$1
failure_time=$2
runs=$3

workload="datamining"
failure="0"
technique="controlplane"
mode="all"
./compute-results-no-failures-cp.sh ../../code/ns3-simulations/ns3-simulations-purr/  "0" "1 $runs" "VL2_CDF.txt" 0 all *:* *:* $simulation_time $simulation_time | awk -v WORKLOAD="$workload" -v FAILURE="$failure" -v TECHNIQUE="$technique" -v MODE="$mode" '{if(NF>1){if(NR>3){$1="";print WORKLOAD " " FAILURE " " TECHNIQUE " " MODE " " $0}}}' > results-datamining.temp

workload="datamining"
failure="1"
technique="controlplane"
mode="all"
./compute-results-cp-one-failure.sh  ../../code/ns3-simulations/ns3-simulations-reconvergence-one-failure "0" "1 $runs" "VL2_CDF.txt" all *:* *:* *:* *:* $simulation_time $simulation_time | awk -v WORKLOAD="$workload" -v FAILURE="$failure" -v TECHNIQUE="$technique" -v MODE="$mode" '{if(NF>1){if(NR>3){$1="";print WORKLOAD " " FAILURE " " TECHNIQUE " " MODE " " $0}}}' >> results-datamining.temp

workload="datamining"
failure="1"
technique="circular"
mode="select-1"
./compute-results-two-failures.sh ../../code/ns3-simulations/ns3-simulations-purr/  "1" "1 $runs" VL2_CDF.txt 1 select 3:4 *:* 3:4 *:* $failure_time $simulation_time | awk -v WORKLOAD="$workload" -v FAILURE="$failure" -v TECHNIQUE="$technique" -v MODE="$mode" '{if(NF>1){if(NR>3){$1="";print WORKLOAD " " FAILURE " " TECHNIQUE " " MODE " " $0}}}' >> results-datamining.temp

workload="datamining"
failure="1"
technique="recirculation"
mode="select-1"
./compute-results-recirculation.sh ../../code/ns3-simulations/ns3-simulations-recirculation "1" "1 $runs" "VL2_CDF.txt" 1 select 3:7 3:8 3:7 3:8 $failure_time $simulation_time | awk -v WORKLOAD="$workload" -v FAILURE="$failure" -v TECHNIQUE="$technique" -v MODE="$mode" '{if(NF>1){if(NR>3){$1="";print WORKLOAD " " FAILURE " " TECHNIQUE " " MODE " " $0}}}' >> results-datamining.temp

workload="datamining"
failure="2"
technique="controlplane"
mode="all"
./compute-results-cp-one-failure.sh  ../../code/ns3-simulations/ns3-simulations-reconvergence-two-failures "0" "1 $runs" "VL2_CDF.txt" all *:* *:* *:* *:* $simulation_time $simulation_time | awk -v WORKLOAD="$workload" -v FAILURE="$failure" -v TECHNIQUE="$technique" -v MODE="$mode" '{if(NF>1){if(NR>3){$1="";print WORKLOAD " " FAILURE " " TECHNIQUE " " MODE " " $0}}}' >> results-datamining.temp

workload="datamining"
failure="2"
technique="recirculation"
mode="select-1"
./compute-results-recirculation.sh ../../code/ns3-simulations/ns3-simulations-recirculation "2" "1 $runs" "VL2_CDF.txt" 1 select 3:7 3:8 3:7 3:8 $failure_time $simulation_time | awk -v WORKLOAD="$workload" -v FAILURE="$failure" -v TECHNIQUE="$technique" -v MODE="$mode" '{if(NF>1){if(NR>3){$1="";print WORKLOAD " " FAILURE " " TECHNIQUE " " MODE " " $0}}}' >> results-datamining.temp

workload="datamining"
failure="2"
technique="circular"
mode="select-1"
./compute-results-two-failures.sh ../../code/ns3-simulations/ns3-simulations-purr/ "2" "1 $runs" "VL2_CDF.txt" 1 select 3:4 *:* 3:4 *:* $failure_time $simulation_time | awk -v WORKLOAD="$workload" -v FAILURE="$failure" -v TECHNIQUE="$technique" -v MODE="$mode" '{if(NF>1){if(NR>3){$1="";print WORKLOAD " " FAILURE " " TECHNIQUE " " MODE " " $0}}}' >> results-datamining.temp

